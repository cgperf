#ifndef CGPERF_HASH_C
#define CGPERF_HASH_C
#include "c_fixing.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/hash.h"
/*------------------------------------------------------------------------------------------------*/
/*
 * Some useful hash function.
 * It's not a particularly good hash function (<< 5 would be better than << 4), but people believe
 * in it because it comes from Dragon book.
 */
static u32 hashpjw(u8 *x, u32 len) /* From Dragon book, p436 */
{
	u32 h;
	u32 g;

	h = 0;
	loop  {
		if (len <= 0)
			break;
		h = (h << 4) + *x++;
		g = h & 0xf0000000;
		if (g != 0)
			h = (h ^ (g >> 24)) ^ g;
		len--;
	}
	return h;
}
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/hash.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
