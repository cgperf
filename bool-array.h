#ifndef CGPERF_BOOL_ARRAY_H
#define CGPERF_BOOL_ARRAY_H
#include "c_fixing.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/bool-array.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ constants and types */
struct Bool_Array {
/*{{{ private */
	/* size of array */
	u32 size;
	/*
	 * Current iteration number. Always nonzero. Starts out as 1, and is
	 * incremented each time clear() is called.
	 */
	u32 iteration_number;
	/*
	 * for each index, we store in storage_array[index] the
	 * iteration_number at the time set_bit(index) was last called
	 */
	u32 *storage_array;
/*}}} private -- END */
};
/*}}} constants and types -- END */
/*{{{ public static methods */
static struct Bool_Array *ba_new(u32 size);
static void ba_del(struct Bool_Array *t);
static void ba_clear(struct Bool_Array *t);
static bool ba_set_bit(struct Bool_Array *t, u32 index);
/*}}} public static methods -- END */
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/bool-array.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
