#ifndef CGPERF_SEARCH_C
#define CGPERF_SEARCH_C
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "c_fixing.h"
#include "globals.h"
#include "search.h"
#include "keyword.h"
#include "keyword_list.h"
#include "options.h"
#include "positions.h"
#include "hash-table.h"
#include "bool-array.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/globals.h"
#include "namespace/search.h"
#include "namespace/keyword.h"
#include "namespace/keyword_list.h"
#include "namespace/options.h"
#include "namespace/positions.h"
#include "namespace/hash-table.h"
#include "namespace/bool-array.h"
#include "namespace/search.c"
/*------------------------------------------------------------------------------------------------*/
/*{{{ THEORY */
/* The general form of the hash function is

      hash (keyword) = sum (asso_values[keyword[i] + alpha_inc[i]] : i in Pos)
                       + len (keyword)

   where Pos is a set of byte positions,
   each alpha_inc[i] is a nonnegative integer,
   each asso_values[c] is a nonnegative integer,
   len (keyword) is the keyword's length if _hash_includes_len, or 0 otherwise.

   Theorem 1: If all keywords are different, there is a set Pos such that
   all tuples (keyword[i] : i in Pos) are different.

   Theorem 2: If all tuples (keyword[i] : i in Pos) are different, there
   are nonnegative integers alpha_inc[i] such that all multisets
   {keyword[i] + alpha_inc[i] : i in Pos} are different.

   Define selchars[keyword] := {keyword[i] + alpha_inc[i] : i in Pos}.

   Theorem 3: If all multisets selchars[keyword] are different, there are
   nonnegative integers asso_values[c] such that all hash values
   sum (asso_values[c] : c in selchars[keyword]) are different.

   Based on these three facts, we find the hash function in three steps:

   Step 1 (Finding good byte positions):
   Find a set Pos, as small as possible, such that all tuples
   (keyword[i] : i in Pos) are different.

   Step 2 (Finding good alpha increments):
   Find nonnegative integers alpha_inc[i], as many of them as possible being
   zero, and the others being as small as possible, such that all multisets
   {keyword[i] + alpha_inc[i] : i in Pos} are different.

   Step 3 (Finding good asso_values):
   Find asso_values[c] such that all hash (keyword) are different.

   In other words, each step finds a projection that is injective on the
   given finite set:
     proj1 : String --> Map (Pos --> N)
     proj2 : Map (Pos --> N) --> Map (Pos --> N) / S(Pos)
     proj3 : Map (Pos --> N) / S(Pos) --> N
   where
     N denotes the set of nonnegative integers,
     Map (A --> B) := Hom_Set (A, B) is the set of maps from A to B, and
     S(Pos) is the symmetric group over Pos.

   This was the theory for !_hash_includes_len; if _hash_includes_len, slight
   modifications apply:
     proj1 : String --> Map (Pos --> N) x N
     proj2 : Map (Pos --> N) x N --> Map (Pos --> N) / S(Pos) x N
     proj3 : Map (Pos --> N) / S(Pos) x N --> N

   For a case-insensitive hash function, the general form is

      hash (keyword) =
        sum (asso_values[alpha_unify[keyword[i] + alpha_inc[i]]] : i in Pos)
        + len (keyword)

   where alpha_unify[c] is chosen so that an upper/lower case change in
   keyword[i] doesn't change  alpha_unify[keyword[i] + alpha_inc[i]].
 *//*}}} THEORY -- END */
/*{{{ finding asso_values[] that fit 
   The idea is to choose the _asso_values[] one by one, in a way that
   a choice that has been made never needs to be undone later.  This
   means that we split the work into several steps.  Each step chooses
   one or more _asso_values[c].  The result of choosing one or more
   _asso_values[c] is that the partitioning of the keyword set gets
   broader.
   Look at this partitioning:  After every step, the _asso_values[] of a
   certain set C of characters are undetermined.  (At the beginning, C
   is the set of characters c with _occurrences[c] > 0.  At the end, C
   is empty.)  To each keyword K, we associate the multiset of _selchars
   for which the _asso_values[] are undetermined:
                    K  -->  K->_selchars intersect C.
   Consider two keywords equivalent if their value under this mapping is
   the same.  This introduces an equivalence relation on the set of
   keywords.  The equivalence classes partition the keyword set.  (At the
   beginning, the partition is the finest possible: each K is an equivalence
   class by itself, because all K have a different _selchars.  At the end,
   all K have been merged into a single equivalence class.)
   The partition before a step is always a refinement of the partition
   after the step.
   We choose the steps in such a way that the partition really becomes
   broader at each step.  (A step that only chooses an _asso_values[c]
   without changing the partition is better merged with the previous step,
   to avoid useless backtracking.) }}}*/
/*------------------------------------------------------------------------------------------------*/
/*{{{ local */
/*{{{ types */
struct EquivalenceClass
{
	/* the keywords in this equivalence class */
	struct Keyword_List *keywords;
	struct Keyword_List  *keywords_last;
	/* the number of keywords in this equivalence class */
	u32 cardinality;
	/*
	 * the undetermined selected characters for the keywords in this equivalence class, as a
	 * canonically reordered multiset
	 */
	u32 *undetermined_chars;
	u32 undetermined_chars_length;

	struct EquivalenceClass *next;
};

struct Step
{
	/* the characters whose values are being determined in this step */
	u32 changing_count;
	u32 *changing;
	/*
	 * Exclusive upper bound for the _asso_values[c] of this step. A power
	 * of 2.
	 */
	u32 asso_value_max;
	/* the characters whose values will be determined after this step */
	bool *undetermined;
	/* the keyword set partition after this step */
	struct EquivalenceClass *partition;
	/* the expected number of iterations in this step */
	f64 expected_lower;
	f64 expected_upper;

	struct Step *next;
};
/*}}} types -- END */
/*{{{ code */
/*{{{ equals */
static bool equals(u32 *ptr1, u32 *ptr2, u32 len)
{
	loop {
		if (len == 0)
			break;
		if (*ptr1 != *ptr2)
			return false;
		++ptr1;
		++ptr2;
		len--;
	}
	return true;
}/*}}}*/
static void delete_partition(struct EquivalenceClass *partition)
{
	loop {
		struct EquivalenceClass *equclass;

		if (partition == 0)
			break;
		equclass = partition;
		partition = equclass->next;
		delete_list(equclass->keywords);
		free(equclass);
	}
}
static bool less_by_hash_value(struct Keyword *kw1, struct Keyword *kw2)
{
	return kw1->hash_value < kw2->hash_value;
}
/*}}} code -- END */
/*}}} local -- END */
/*------------------------------------------------------------------------------------------------*/
/*{{{ schr_new */
static struct Search *schr_new(struct Keyword_List *list)
{
	struct Search *t;

	t = calloc(1, sizeof(*t));
	t->head = list;
	t->key_positions = pos_new();
	return t;
}/*}}}*/
/*{{{ schr_del */
static void schr_del(struct Search *t)
{
	ba_del(t->collision_detector);
	if (OPTS(DEBUG)) {
		u32 i;
		s32 field_width;
		struct Keyword_List *ptr;

		fprintf(stderr, "\ndumping occurrence and associated values tables\n");
		i = 0;
		loop {
			if (i >= t->alpha_size)
				break;
			if (t->occurrences[i])
				fprintf (stderr, "asso_values[%c] = %6d, occurrences[%c] = %6d\n", i, t->asso_values[i], i, t->occurrences[i]);
			++i;
		}
		fprintf(stderr, "end table dumping\n");
		fprintf(stderr, "\nDumping key list information:\ntotal non-static linked keywords = %d\ntotal keywords = %d\ntotal duplicates = %d\nmaximum key length = %d\n", t->list_len, t->total_keys, t->total_duplicates, t->max_key_len);
		field_width = t->max_selchars_length;
		fprintf(stderr, "\nList contents are:\n(hash value, key length, index, %*s, keyword):\n", field_width, "selchars");
		ptr = t->head;
		loop {
			s32 j;

			if (ptr == 0)
				break;
			fprintf(stderr, "%11d,%11d,%6d, ", ptr->kw->hash_value, ptr->kw->allchars_length, ptr->kw->final_index);
			if (field_width > ptr->kw->selchars_length)
				fprintf(stderr, "%*s", field_width - ptr->kw->selchars_length, "");
			j = 0;
			loop {
				if (j >= ptr->kw->selchars_length)
					break;
				putc(ptr->kw->selchars[j], stderr);
				++j;
			}
			fprintf(stderr, ", %.*s\n", ptr->kw->allchars_length, ptr->kw->allchars);
			ptr = ptr->next;
		}
		fprintf(stderr, "End dumping list.\n\n");
	}
	pos_del(t->key_positions);
	free(t->asso_values);
	free(t->occurrences);
	free(t->alpha_unify);
	free(t->alpha_inc);
	free(t);
}/*}}}*/
/*{{{ schr_optimize */
static void schr_optimize(struct Search *t)
{
	struct Keyword_List *curr_ptr;
	s32 max_hash_value;
	u32 c;

	/* preparations */
	schr_prepare(t);

	/* Step 1: Finding good byte positions. */
	schr_find_positions(t);

	/* Step 2: Finding good alpha increments. */
	schr_find_alpha_inc(t);

	/* Step 3: Finding good asso_values. */
	schr_find_good_asso_values(t);
	/* Make one final check, just to make sure nothing weird happened.... */
	ba_clear(t->collision_detector);
	curr_ptr = t->head;
	loop {
		struct Keyword *curr;
		u32 hashcode;

		if (curr_ptr == 0)
			break;
		curr = curr_ptr->kw;
		hashcode = schr_compute_hash(t, curr);
		if (ba_set_bit(t->collision_detector, hashcode)) {
			/*
			 * This shouldn't happen. proj1, proj2, proj3 must have been computed to be
			 * injective on the given keyword set.
			 */
			fprintf(stderr, "\nInternal error, unexpected duplicate hash code\n");
			if (OPTS(POSITIONS))
				fprintf(stderr, "try options -m or -r, or use new key positions.\n\n");
			else
				fprintf(stderr, "try options -m or -r.\n\n");
			exit(1);
		}
		curr_ptr = curr_ptr->next;
	}
	/* sorts the keyword list by hash value */
	schr_sort(t);
	/*
	 * Set unused asso_values[c] to max_hash_value + 1.  This is not absolutely necessary, but
	 * speeds up the lookup function in many cases of lookup failure: no string comparison is
	 * needed once the hash value of a string is larger than the hash value of any keyword.
	 */
	{
		struct Keyword_List *tmp;

		tmp = t->head;
		loop {
			if (tmp->next == 0)
				break;
			tmp = tmp->next;
		}
		max_hash_value = tmp->kw->hash_value;
	}
	c = 0;
	loop {
		if (c >= t->alpha_size)
			break;
		if (t->occurrences[c] == 0)
			t->asso_values[c] = max_hash_value + 1;
		++c;
	}
	/* propagate unified asso_values */
	if (t->alpha_unify) {
		u32 c;

		c = 0;
		loop {
			if (c >= t->alpha_size)
				break;
			if (t->alpha_unify[c] != c)
				t->asso_values[c] = t->asso_values[t->alpha_unify[c]];
			++c;
		}
	}
}/*}}}*/
/*{{{ schr_prepare */
static void schr_prepare(struct Search *t)
{
	struct Keyword_List *tmp;

	t->total_keys = 0;
	tmp = t->head;
	loop {
		if (tmp == 0)
			break;
		++(t->total_keys);
		tmp = tmp->next;
	}
	/* compute the minimum and maximum keyword length */
	t->max_key_len = S32_MIN;
	t->min_key_len = S32_MAX;
	tmp = t->head;
	loop {
		struct Keyword *kw;

		if (tmp == 0)
			break;
		kw = tmp->kw;
		if (t->max_key_len < kw->allchars_length)
			t->max_key_len = kw->allchars_length;
		if (t->min_key_len > kw->allchars_length)
			t->min_key_len = kw->allchars_length;
		tmp = tmp->next;
	}
	/*
	 * exit program if an empty string is used as keyword, since the comparison expressions
	 * don't work correctly for looking up an empty string
	 */
	if (t->min_key_len == 0) {
		fprintf (stderr, "Empty input keyword is not allowed.\nTo recognize an empty input keyword, your code should check for\nlen == 0 before calling the gperf generated lookup function.\n");
		exit(1);
	}
	/* exit program if the characters in the keywords are not in the required range */
	if (OPTS(SEVENBIT)) {
		tmp = t->head;
		loop {
			struct Keyword *kw;
			u8 *k;
			s32 i;

			if (tmp == 0)
				break;
			kw = tmp->kw;
			k = kw->allchars;
			i = kw->allchars_length;
			loop {
				if (i <= 0)
					break;
				if (!(*k < 128)) {
					fprintf(stderr, "Option --seven-bit has been specified,\nbut keyword \"%.*s\" contains non-ASCII characters.\nTry removing option --seven-bit.\n", kw->allchars_length, kw->allchars);
					exit(1);
				}
				i--;
				++k;
			}
			tmp = tmp->next;
		}
	}
	/* determine whether the hash function shall include the length */
	t->hash_includes_len = !(OPTS(NOLENGTH) || (t->min_key_len == t->max_key_len));
}/*}}}*/
/*{{{ schr_find_positions */
/* find good key positions */
static void schr_find_positions(struct Search *t)
{
	u32 *alpha_unify;
	s32 imax;
	struct Positions *mandatory;
	struct Positions *current;
	u32 current_duplicates_count;
	/* if the user gave the key positions, we use them */
	if (OPTS(POSITIONS)) {
		pos_cpy(t->key_positions, options->key_positions);
		return;
	}
	/* compute preliminary alpha_unify table */
	alpha_unify = schr_compute_alpha_unify(t);

	/* 1. find positions that must occur in order to distinguish duplicates */
	mandatory = pos_new();
	if (!OPTS(DUP)) {
		struct Keyword_List *l1;

		l1 = t->head;
		loop {
			struct Keyword *kw1;
			struct Keyword_List *l2;

			if (l1 == 0 || l1->next == 0)
				break;
			kw1 = l1->kw;
			l2 = l1->next;
			loop {
				struct Keyword *kw2;

				if (l2 == 0)
					break;
				kw2 = l2->kw;
				/*
				 * if keyword1 and keyword2 have the same length and differ
				 * in just one position, and it is not the last character,
				 * this position is mandatory
				 */
				if (kw1->allchars_length == kw2->allchars_length) {
					s32 n;
					s32 i;

					n = kw1->allchars_length;
					i = 0;
					loop {
						u32 c1;
						u32 c2;

						if (i >= (n - 1))
							break;
						c1 = kw1->allchars[i];
						c2 = kw2->allchars[i];
						if (OPTS(UPPERLOWER)) {
							if (c1 >= 'A' && c1 <= 'Z')
								c1 += 'a' - 'A';
							if (c2 >= 'A' && c2 <= 'Z')
								c2 += 'a' - 'A';
						}
						if (c1 != c2)
							break;
						++i;
					}
					if (i < (n - 1)) {
						s32 j;

						j = i + 1;
						loop {
							u32 c1;
							u32 c2;

							if (j >= n)
								break;
							c1 = kw1->allchars[j];
							c2 = kw2->allchars[j];
							if (OPTS(UPPERLOWER)) {
								if (c1 >= 'A' && c1 <= 'Z')
									c1 += 'a' - 'A';
								if (c2 >= 'A' && c2 <= 'Z')
									c2 += 'a' - 'A';
							}
							if (c1 != c2)
								break;
							++j;
						}
						if (j >= n) {
							/* position i is mandatory */
							if (!pos_contains(mandatory, i))
								pos_add(mandatory, i);
						}
					}
				}
				l2 = l2->next;
			}
			l1 = l1->next;
		}
	}
  	/* 2. add positions, as long as this decreases the duplicates count */
	imax = (t->max_key_len - 1 < (s32)POS_MAX_KEY_POS - 1 ?  t->max_key_len - 1
									: (s32)POS_MAX_KEY_POS - 1);
	current = pos_new();
	pos_cpy(current, mandatory);
	current_duplicates_count = schr_count_duplicates_tuple_do(t, current, alpha_unify);
	loop {
		struct Positions *best;
		u32 best_duplicates_count;
		s32 i;

		best = pos_new();
		best_duplicates_count = U32_MAX;
		i = imax;
		loop {
			if (i < -1)
				break;
			if (!pos_contains(current, i)) {
				struct Positions *tryal;
				u32 try_duplicates_count;

				tryal = pos_new();
				pos_cpy(tryal, current);
				pos_add(tryal, i);
				try_duplicates_count = schr_count_duplicates_tuple_do(t, tryal,
										alpha_unify);
				/*
				 * We prefer 'try' to 'best' if it produces less duplicates, or if
				 * it produces the same number of duplicates but with a more
				 * efficient hash function.
				 */
				if (try_duplicates_count < best_duplicates_count
						|| (try_duplicates_count == best_duplicates_count
											&& i >=0)) {
					pos_cpy(best, tryal);
					best_duplicates_count = try_duplicates_count;
				}
				pos_del(tryal);
			}
			i--;
		}
		/* stop adding positions when it gives no improvement */
		if (best_duplicates_count >= current_duplicates_count)
			break;
		pos_cpy(current, best);
		pos_del(best);
		current_duplicates_count = best_duplicates_count;
	}
	/* 3. remove positions, as long as this doesn't increase the duplicates count */
	loop {
		struct Positions *best;
		u32 best_duplicates_count;
		s32 i;

		best = pos_new();
		best_duplicates_count = U32_MAX;
		i = imax;
		loop {
			if (i < -1)
				break;
			if (pos_contains(current, i) && !pos_contains(mandatory, i)) {
				struct Positions *tryal;
				u32 try_duplicates_count;

				tryal = pos_new();
				pos_cpy(tryal, current);
				pos_remove(tryal, i);
				try_duplicates_count = schr_count_duplicates_tuple_do(t, tryal,
										alpha_unify);
				/*
				 * We prefer 'try' to 'best' if it produces less duplicates, or if
				 * it produces the same number of duplicates but with a more
				 * efficient hash function.
				 */
				if (try_duplicates_count < best_duplicates_count
						|| (try_duplicates_count == best_duplicates_count
										&& i == -1)) {
					pos_cpy(best, tryal);
					best_duplicates_count = try_duplicates_count;
				}
			}
			i--;
		}
		/* stop removing positions when it gives no improvement */
		if (best_duplicates_count > current_duplicates_count)
			break;
		pos_cpy(current, best);
		pos_del(best);
		current_duplicates_count = best_duplicates_count;
	}
	/* 4. replace two positions by one, as long as this doesn't increase the duplicates count */
	loop {
		struct Positions *best;
		u32 best_duplicates_count;
		s32 i1;

		best = pos_new();
		best_duplicates_count = U32_MAX;
		/*
		 * Loop over all pairs { i1, i2 } of currently selected positions. W.l.o.g. we can
		 * assume i1 > i2.
		 */
		i1 = imax;
		loop {
			if (i1 < -1)
				break;
			if (pos_contains(current, i1) && !pos_contains(mandatory, i1)) {
				s32 i2;

				i2 = i1 - 1;
				loop {
					if (i2 < -1)
						break;
					if (pos_contains(current, i2) && !pos_contains(mandatory, i2)) {
						s32 i3;

						i3 = imax;
						loop {
							if (i3 < -1)
								break;
							if (!pos_contains(current, i3)) {
								struct Positions *tryal;
								u32 try_duplicates_count;

								tryal = pos_new();
								pos_cpy(tryal, current);
								pos_remove(tryal, i1);
								pos_remove(tryal, i2);
								pos_add(tryal, i3);
								try_duplicates_count = schr_count_duplicates_tuple_do(t, tryal, alpha_unify);
								/*
								 * We prefer 'try' to 'best' if it produces less
								 * duplicates, or if it produces the same number
								 * of duplicates but with a more efficient hash
								 * function.
								 */
								if (try_duplicates_count < best_duplicates_count
									|| (try_duplicates_count == best_duplicates_count
										&& (i1 == -1 || i2 == -1) && i3 >= 0)) {
									pos_cpy(best, tryal);
									best_duplicates_count = try_duplicates_count;
								}
								pos_del(tryal);
							}
							i3--;
						}
					}
					i2--;
				}
			}
			i1--;
		}
		/* stop removing positions when it gives no improvement */
		if (best_duplicates_count > current_duplicates_count)
			break;
		pos_cpy(current, best);
		current_duplicates_count = best_duplicates_count;
		pos_del(best);
	}
	/* That's it. Hope it's good enough. */
	pos_cpy(t->key_positions, current);
	if (OPTS(DEBUG)) {
		struct PositionReverseIterator *iter;
		bool seen_lastchar;
		bool first;
		s32 i;
		/* Print the result. */
		fprintf(stderr, "\nComputed positions: ");
		iter = pos_reviterator(t->key_positions);
		seen_lastchar = false;
		first = true;
		loop {
			i = posrevit_next(iter);
			if (i == POSREVIT_EOS)
				break;
			if (!first)
				fprintf(stderr, ", ");
			if (i == POS_LASTCHAR)
				seen_lastchar = true;
			else {
				fprintf(stderr, "%d", i + 1);
				first = false;
			}
		}
		if (seen_lastchar) {
			if (!first)
				fprintf(stderr, ", ");
			fprintf(stderr, "$");
		}
		fprintf(stderr, "\n");
		posrevit_del(iter);
	}
	pos_del(current);
	pos_del(mandatory);
	free(alpha_unify);
}/*}}}*/
/*{{{ schr_compute_alpha_size */
/* computes the upper bound on the indices passed to asso_values[], assuming no alpha_increments */
static u32 schr_compute_alpha_size(void)
{
	return (OPTS(SEVENBIT) ? 128 : 256);
}/*}}}*/
/*{{{ schr_compute_alpha_unify */
static u32 *schr_compute_alpha_unify(struct Search *t)
{
	if (OPTS(UPPERLOWER)) {
		/* uppercase to lowercase mapping */
		u32 alpha_size;
		u32 *alpha_unify;
		u32 c;

		alpha_size = schr_compute_alpha_size();
		alpha_unify = calloc(alpha_size, sizeof(*alpha_unify));
		c = 0;
		loop {
			if (c >= alpha_size)
				break;
			alpha_unify[c] = c;
			++c;
		}
		c = 'A';
		loop {
			if (c > 'Z')
				break;
			alpha_unify[c] = c + ('a' - 'A');
			++c;
		}
		return alpha_unify;
	} else
		/* identity mapping */
		return 0;
}/*}}}*/
/*{{{ schr_find_alpha_inc */
/* find good alpha_inc[] */
static void schr_find_alpha_inc(struct Search *t)
{
	/*
	 * The goal is to choose _alpha_inc[] such that it doesn't introduce artificial duplicates.
	 * In other words, the goal is: # proj2 (proj1 (K)) = # proj1 (K).
	 */
	u32 duplicates_goal;
	u32 *current;
	s32 i;
	u32 current_duplicates_count;

	duplicates_goal = schr_count_duplicates_tuple(t);
	current = calloc(t->max_key_len, sizeof(*current));
	i = 0;
	loop {
		if (i >= t->max_key_len)
			break;
		current[i] = 0;
		++i;
	}
	current_duplicates_count = schr_count_duplicates_multiset(t, current);
	if (current_duplicates_count > duplicates_goal) {
		/* look which _alpha_inc[i] we are free to increment */
		u32 nindices;
		u32 *indices;
		u32 *best;
		u32 *tryal;
		{
			struct PositionIterator *iter;

			nindices = 0;
			iter = pos_iterator(t->key_positions, t->max_key_len);
			loop {
				s32 key_pos;

				key_pos = positer_next(iter);
				if (key_pos == POSITER_EOS)
					break;
				if (key_pos != POS_LASTCHAR)
					++nindices;
			}
			positer_del(iter);
		}
		indices = calloc(nindices, sizeof(*indices));
		{
			u32 j;
			struct PositionIterator *iter;

			j = 0;
			iter = pos_iterator(t->key_positions, t->max_key_len);
			loop {
				s32 key_pos;

				key_pos = positer_next(iter);
				if (key_pos == POSITER_EOS)
					break;
				if (key_pos != POS_LASTCHAR) {
					indices[j] = key_pos;
					++j;
				}
			}
			if (!(j == nindices))
				abort();
			positer_del(iter);
		}
		/*
		 * Perform several rounds of searching for a good alpha increment. Each round
		 * reduces the number of artificial collisions by adding an increment in a single
		 * key position.
		 */
		best = calloc(t->max_key_len, sizeof(*best));
		tryal = calloc(t->max_key_len, sizeof(*best));
		loop {
			u32 inc;
			/* An increment of 1 is not always enough. Try higher increments also. */
			inc = 1;
			loop {
				u32 best_duplicates_count;
				u32 j;

				best_duplicates_count = U32_MAX;
				j = 0;
				loop {
					u32 try_duplicates_count;

					if (j >= nindices)
						break;
					memcpy(tryal, current, t->max_key_len * sizeof(u32));
					tryal[indices[j]] += inc;
					try_duplicates_count = schr_count_duplicates_multiset(t,
											tryal);
                  			/* we prefer 'try' to 'best' if it produces less duplicates */
					if (try_duplicates_count < best_duplicates_count) {
						memcpy(best, tryal, t->max_key_len * sizeof(u32));
						best_duplicates_count = try_duplicates_count;
					}
					++j;
				}
				/* stop this round when we got an improvement */
				if (best_duplicates_count < current_duplicates_count) {
					memcpy(current, best, t->max_key_len * sizeof(u32));
					current_duplicates_count = best_duplicates_count;
					break;
				}
				++inc;
			}
			if (current_duplicates_count <= duplicates_goal)
				break;
		}
		free(tryal);
		free(best);

		if (OPTS(DEBUG)) {
			bool first;
			u32 j;
			/* print the result */
          		fprintf(stderr, "\nComputed alpha increments: ");
			first = true;
			j = nindices;
			loop {
				if (j <= 0)
					break;
				j--;
				if (current[indices[j]] != 0) {
					if (!first)
						fprintf(stderr, ", ");
                			fprintf(stderr, "%u:+%u", indices[j] + 1, current[indices[j]]);
					first = false;
				}
			}
       			fprintf(stderr, "\n");
		}
		free(indices);
	}
	t->alpha_inc = current;
	t->alpha_size = schr_compute_alpha_size_with_inc(t, t->alpha_inc);
	t->alpha_unify = schr_compute_alpha_unify_with_inc(t, t->key_positions, t->alpha_inc);
}/*}}}*/
/*{{{ schr_count_duplicates_tuple_do */
/*
 * Count the duplicate keywords that occur with a given set of positions. In other words, it
 * returns the difference # K - # proj1 (K) where K is the multiset of given keywords.
 */
static u32 schr_count_duplicates_tuple_do(struct Search *t, struct Positions *positions,
										u32 *alpha_unify)
{
	u32 count;
	/*
	 * Run through the keyword list and count the duplicates incrementally.  The result does not
	 * depend on the order of the keyword list, thanks to the formula above.
	 */
	schr_init_selchars_tuple(t, positions, alpha_unify);
	count = 0;
	{
		struct Hash_Table *representatives;
		struct Keyword_List *tmp;

		representatives = ht_new(t->total_keys, !t->hash_includes_len);
		tmp = t->head;	
		loop {
			struct Keyword *kw;

			if (tmp == 0)
				break;
			kw = tmp->kw;
			if (ht_insert(representatives, kw))
				++count;
			tmp = tmp->next;
		}
		ht_del(representatives);
	}
	schr_delete_selchars(t);
	return count;
}/*}}}*/
/*{{{ schr_init_selchars_tuple */
static void schr_init_selchars_tuple(struct Search *t, struct Positions *positions,
										u32 *alpha_unify)
{
	struct Keyword_List *tmp;

	tmp = t->head;
	loop {
		if (tmp == 0)
			break;
		kw_init_selchars_tuple(tmp->kw, positions, alpha_unify);
		tmp = tmp->next;
	}
}/*}}}*/
/*{{{ schr_delete_selchars */
static void schr_delete_selchars(struct Search *t)
{
	struct Keyword_List *tmp;

	tmp = t->head;
	loop {
		if (tmp == 0)
			break;
		kw_delete_selchars(tmp->kw);
		tmp = tmp->next;
	}
}/*}}}*/
/*{{{ schr_count_duplicates_tuple */
/*
 * Count the duplicate keywords that occur with the found set of positions.  In other words, it
 * returns the difference # K - # proj1 (K) where K is the multiset of given keywords.
 */
static u32 schr_count_duplicates_tuple(struct Search *t)
{
	u32 *alpha_unify;
	u32 count;

	alpha_unify = schr_compute_alpha_unify(t);
	count = schr_count_duplicates_tuple_do(t, t->key_positions, alpha_unify);
	free(alpha_unify);
	return count;
}/*}}}*/
/*{{{ schr_count_duplicates_multiset */
/*
 * Count the duplicate keywords that occur with the given set of positions and a given alpha_inc[]
 * array.
 * In other words, it returns the difference
 * # K - # proj2 (proj1 (K))
 * where K is the multiset of given keywords.
 */
static u32 schr_count_duplicates_multiset(struct Search *t, u32 *alpha_inc)
{
	/*
	 * Run through the keyword list and count the duplicates incrementally. The result does not
	 * depend on the order of the keyword list, thanks to the formula above.
	 */
	u32 *alpha_unify;
	u32 count;

	alpha_unify = schr_compute_alpha_unify_with_inc(t, t->key_positions, alpha_inc);
	schr_init_selchars_multiset(t, t->key_positions, alpha_unify, alpha_inc);
	count = 0;
	{
		struct Hash_Table *representatives;
		struct Keyword_List *tmp;

		representatives = ht_new(t->total_keys, !t->hash_includes_len);
		tmp = t->head;
		loop {	
			struct Keyword *kw;

			if (tmp == 0)
				break;
			kw = tmp->kw;
			if (ht_insert(representatives, kw))
				++count;
			tmp = tmp->next;
		}
		ht_del(representatives);
	}
	schr_delete_selchars(t);
	free(alpha_unify);
	return count;
}/*}}}*/
/*{{{ schr_compute_alpha_unify_with_inc */
/* computes the unification rules between different asso_values[c] */
static u32 *schr_compute_alpha_unify_with_inc(struct Search *t, struct Positions *positions,
										u32 *alpha_inc)
{
	if (OPTS(UPPERLOWER)) {
		/*
		 * Without alpha increments, we would simply unify
		 * 'A' -> 'a', ..., 'Z' -> 'z'.
		 * But when a keyword contains at position i a character c,
		 * we have the constraint
		 * asso_values[tolower(c) + alpha_inc[i]] ==
		 * asso_values[toupper(c) + alpha_inc[i]].
		 * This introduces a unification
		 * toupper(c) + alpha_inc[i] -> tolower(c) + alpha_inc[i].
		 * Note that this unification can extend outside the range of
		 * ASCII letters! But still every unified character pair is at
		 * a distance of 'a'-'A' = 32, or (after chained unification)
		 * at a multiple of 32.  So in the end the alpha_unify vector
		 * has the form    c -> c + 32 * f(c)   where f(c) is a
		 * nonnegative integer.
		 */
		u32 alpha_size;
		u32 *alpha_unify;
		u32 c;
		struct Keyword_List *tmp;

		alpha_size = schr_compute_alpha_size_with_inc(t, alpha_inc);
		alpha_unify = calloc(alpha_size, sizeof(*alpha_unify));
		c = 0;
		loop {
			if (c >= alpha_size)
				break;
			alpha_unify[c] = c;
			++c;
		}
		tmp = t->head;
		loop {
			struct Keyword *kw;
			struct PositionIterator *iter;
			s32 i;

			if (tmp == 0)
				break;
			kw = tmp->kw;
			iter = pos_iterator(positions, kw->allchars_length);
			loop {
				u32 c;

				i = positer_next(iter);
				if (i == POSITER_EOS)
					break;
				if (i == POS_LASTCHAR)
					c = (u8)(kw->allchars[kw->allchars_length - 1]);
				else if (i < kw->allchars_length)
					c = (u8)(kw->allchars[i]);
				else
					abort();
				if (c >= 'A' && c <= 'Z')
					c += 'a' - 'A';
				if (c >= 'a' && c <= 'z') {
					u32 d;
					u32 b;
					s32 a;

					if (i != POS_LASTCHAR)
						c += alpha_inc[i];
					/* unify c with c - ('a'-'A') */
					d = alpha_unify[c];
					b = c - ('a' - 'A');
					a = b;
					loop {
						if (a < 0 || alpha_unify[a] != b)
							break;
						alpha_unify[a] = d;
						a -= ('a' - 'A');
					}
				}
			}
			positer_del(iter);
			tmp = tmp->next;
		}
		return alpha_unify;
	} else
    		/* identity mapping */
		return 0;
}/*}}}*/
/*{{{ schr_compute_alpha_size_with_inc */
/* computes the upper bound on the indices passed to asso_values[] */
static u32 schr_compute_alpha_size_with_inc(struct Search *t, u32 *alpha_inc)
{
	u32 max_alpha_inc;
	s32 i;

	max_alpha_inc = 0;
	i = 0;
	loop {
		if (i >= t->max_key_len)
			break;
		if (max_alpha_inc < alpha_inc[i])
			max_alpha_inc = alpha_inc[i];
		++i;
	}
	return (OPTS(SEVENBIT) ? 128 : 256) + max_alpha_inc;
}/*}}}*/
/*{{{ schr_init_selchars_multiset */
static void schr_init_selchars_multiset(struct Search *t, struct Positions *positions,
								u32 *alpha_unify, u32 *alpha_inc)
{
	struct Keyword_List *tmp;

	tmp = t->head;
	loop {
		if (tmp == 0)
			break;
		kw_init_selchars_multiset(tmp->kw, positions, alpha_unify, alpha_inc);
		tmp = tmp->next;
	}
}/*}}}*/
/*{{{ schr_find_good_asso_values */
/* finds good _asso_values[] */
static void schr_find_good_asso_values(struct Search *t)
{
	s32 asso_iterations;
	struct Keyword_List *saved_head;
	s32 best_initial_asso_value;
	s32 best_jump;
	s32 *best_asso_values;
	s32 best_collisions;
	s32 best_max_hash_value;

	schr_prepare_asso_values(t);

	/* search for good _asso_values[] */
	asso_iterations = options->asso_iterations;
	if (asso_iterations == 0) {
		schr_find_asso_values(t);
		return;
	}
	/*
	 * Try different pairs of _initial_asso_value and _jump, in the
	 * following order:
	 * (0, 1)
	 * (1, 1)
	 * (2, 1) (0, 3)
	 * (3, 1) (1, 3)
	 * (4, 1) (2, 3) (0, 5)
	 * (5, 1) (3, 3) (1, 5)
	 *.....
	 */
	saved_head = t->head;
	best_initial_asso_value = 0;
	best_jump = 1;
	best_asso_values = calloc(t->alpha_size, sizeof(*best_asso_values));
	best_collisions = S32_MAX;
	best_max_hash_value = S32_MAX;

	t->initial_asso_value = 0;
	t->jump = 1;
	loop {
		s32 collisions;
		s32 max_hash_value;
		struct Keyword_List *ptr;
		/* restore the keyword list in its original order */
		t->head = copy_list(saved_head);
		/* find good _asso_values[] */
		schr_find_asso_values(t);
		/* test whether it is the best solution so far */
		collisions = 0;
		max_hash_value = S32_MIN;
		ba_clear(t->collision_detector);
		ptr = t->head;
		loop {
			struct Keyword *kw;
			s32 hashcode;

			if (ptr == 0)
				break;
			kw = ptr->kw;
			hashcode = schr_compute_hash(t, kw);
			if (max_hash_value < hashcode)
				max_hash_value = hashcode;
			if (ba_set_bit(t->collision_detector, hashcode))
				++collisions;
			ptr = ptr->next;
		}
		if (collisions < best_collisions || (collisions == best_collisions
							 && max_hash_value < best_max_hash_value)) {
			memcpy(best_asso_values, t->asso_values, t->alpha_size
								* sizeof(*(t->asso_values)));
			best_collisions = collisions;
			best_max_hash_value = max_hash_value;
		}
		/* delete the copied keyword list */
		delete_list(t->head);

		asso_iterations--;
		if (asso_iterations == 0)
			break;
		/* prepare for next iteration */
		if (t->initial_asso_value >= 2) {
			t->initial_asso_value -= 2;
			t->jump += 2;
		} else {
			t->initial_asso_value += t->jump;
			t->jump = 1;
		}
	}
	t->head = saved_head;
	/* install the best found asso_values */
	t->initial_asso_value = best_initial_asso_value;
	t->jump = best_jump;
	memcpy(t->asso_values, best_asso_values, t->alpha_size * sizeof(*(t->asso_values)));
	free(best_asso_values);
}/*}}}*/
/*{{{ schr_find_asso_values */
static void schr_find_asso_values(struct Search *t)
{
	struct Step *steps;
	struct Step *step;
	u32 stepno;

	/* determine the steps, starting with the last one */
	{
		bool *undetermined;
		bool *determined;
		s32 c;

		steps = 0;

		undetermined = calloc(t->alpha_size, sizeof(*undetermined));
		c = 0;
		loop {
			if (c >= t->alpha_size)
				break;
			undetermined[c] = false;
			++c;
		}
		determined = calloc(t->alpha_size, sizeof(*determined));
		c = 0;
		loop {
			if (c >= t->alpha_size)
				break;
			determined[c] = true;
			++c;
		}
		loop {
			/* compute the partition that needs to be refined */
			struct EquivalenceClass *partition;
			u32 chosen_c;
			u32 chosen_possible_collisions;
			struct Step *step;
			u32 c;
			u32 *changing;
			u32 changing_count;

			partition = schr_compute_partition(t, undetermined);
			/*
			 * Determine the main character to be chosen in this step. Choosing such a
			 * character c has the effect of splitting every equivalence class
			 * (according the the frequency of occurrence of c). We choose the c with
			 * the minimum number of possible collisions, so that characters which lead
			 * to a large number of collisions get handled early during the search.
			 */
			{
				u32 best_c;
				u32 best_possible_collisions;
				u32 c;

				best_c = 0;
				best_possible_collisions = U32_MAX;
				c = 0;
				loop {
					if (c >= t->alpha_size)
						break;
					if (t->occurrences[c] > 0 && determined[c]) {
						u32 possible_collisions;

						possible_collisions =
								schr_count_possible_collisions(t,
										partition, c);
						if (possible_collisions
								< best_possible_collisions) {
							best_c = c;
							best_possible_collisions =
										possible_collisions;
						}
					}
					++c;
				}
				if (best_possible_collisions == U32_MAX) {
              				/*
					 * All c with occurrences[c] > 0 are undetermined. We are
					 * are the starting situation and don't need any more step.
					 */
					delete_partition(partition);
					break;
				}
				chosen_c = best_c;
				chosen_possible_collisions = best_possible_collisions;
			}
        		/* we need one more step */
			step = calloc(1, sizeof(*step));

			step->undetermined = calloc(t->alpha_size,
								sizeof(*(step->undetermined)));
			memcpy(step->undetermined, undetermined, t->alpha_size
								* sizeof(*(step->undetermined)));
			step->partition = partition;
			/* now determine how the equivalence classes will be before this step */
			undetermined[chosen_c] = true;
			partition = schr_compute_partition(t, undetermined);
			/*
			 * Now determine which other characters should be determined in this step,
			 * because they will not change the equivalence classes at this point.  It
			 * is the set of all c which, for all equivalence classes, have the same
			 * frequency of occurrence in every keyword of the equivalence class.
			 */
			c = 0;
			loop {
				if (c >= t->alpha_size)
					break;
				if (t->occurrences[c] > 0 && determined[c]
						&& schr_unchanged_partition(t, partition, c)) {
					undetermined[c] = true;
					determined[c] = false;
				}
				++c;
			}
			/* main_c must be one of these */
			if (determined[chosen_c])
				abort();
			/* now the set of changing characters of this step */
			changing_count = 0;
			c = 0;
			loop {
				if (c >= t->alpha_size)
					break;
				if (undetermined[c] && !step->undetermined[c])
					++changing_count;
				++c;
			}
			changing = calloc(changing_count, sizeof(*changing));
			changing_count = 0;
			c = 0;
			loop {
				if (c >= t->alpha_size)
					break;
				if (undetermined[c] && !step->undetermined[c]) {
					changing[changing_count] = c;
					++changing_count;
				}
				++c;
			}
			step->changing = changing;
			step->changing_count = changing_count;
			step->asso_value_max = t->asso_value_max;
			step->expected_lower = exp((f64)(chosen_possible_collisions)
								/ (f64)(t->max_hash_value));
			step->expected_upper = exp((f64)(chosen_possible_collisions)
								/ (f64)(t->asso_value_max));
			delete_partition(partition);
			step->next = steps;
			steps = step;
		}
		free(determined);
		free(undetermined);
	}
	if (OPTS(DEBUG)) {
		u32 stepno;
		struct Step *step;

		stepno = 0;
		step = steps;
		loop {
			u32 i;
			struct EquivalenceClass *cls;

			if (step == 0)
				break;
			++stepno;
          		fprintf(stderr, "Step %u chooses _asso_values[", stepno);
			i = 0;
			loop {
				if (i >= step->changing_count)
					break;
				if (i > 0)
					fprintf(stderr, ",");
				fprintf(stderr, "'%c'", step->changing[i]);
				++i;
			}
          		fprintf(stderr, "], expected number of iterations between %g and %g.\n", step->expected_lower, step->expected_upper);
			fprintf(stderr, "Keyword equivalence classes:\n");
			cls = step->partition;
			loop {
				struct Keyword_List *tmp;

				if (cls == 0)
					break;
				fprintf (stderr, "\n");
				tmp = cls->keywords;
				loop {
					struct Keyword *kw;

					if (tmp == 0)
						break;
					kw = tmp->kw;
					fprintf(stderr, "  %.*s\n", kw->allchars_length, kw->allchars);
					tmp = tmp->next;
				}
				cls = cls->next;
			}
			fprintf(stderr, "\n");
			step = step->next;
		}
	}
	/*
	 * Initialize _asso_values[]. (The value given here matters only for those c which occur in
	 * all keywords with equal multiplicity.)
	 */
	memset(t->asso_values, 0, t->alpha_size * sizeof(*t->asso_values));
	stepno = 0;
	step = steps;
	loop {
		u32 k;
		u32 i;
		u32 iterations;
		u32 *iter;
		u32 ii;

		if (step == 0)
			break;
		++stepno;
		/* initialize the asso_values[] */
		k = step->changing_count;
		i = 0;
		loop {
			u32 c;

			if (i >= k)
				break;
			c = step->changing[i];
			t->asso_values[c] = (t->initial_asso_value < 0 ?  rand()
					: t->initial_asso_value) & (step->asso_value_max - 1);
			++i;
		}
		iterations = 0;
		iter = calloc(k, sizeof(*iter));
		ii = (t->jump != 0 ? k - 1 : 0);
		loop {
			bool has_collision;
			struct EquivalenceClass *cls;
			/*
			 * test whether these asso_values[] lead to collisions among the equivalence
			 * classes that should be collision-free
			 */
			has_collision = false;
			cls = step->partition;
			loop {
				struct Keyword_List *ptr;

				if (cls == 0)
					break;
				/* Iteration Number array is a win, O(1) initialization time! */
				ba_clear(t->collision_detector);
				ptr = cls->keywords;				
				loop {
					struct Keyword *kw;
					s32 hashcode;

					if (ptr == 0)
						break;
					kw = ptr->kw;
					/*
					 * compute the new hash code for the keyword, leaving apart
					 * the yet undetermined asso_values[].
					 */
					{
						s32 sum;
						u32 *p;
						s32 i;

						sum = t->hash_includes_len ? kw->allchars_length
												: 0;
						p = kw->selchars;
						i = kw->selchars_length;
						loop {
							if (i <= 0)
								break;
							if (!step->undetermined[*p])
								sum += t->asso_values[*p];
							++p;
							i--;
						}
						hashcode = sum;
					}
					/*
					 * See whether it collides with another keyword's hash code,
					 * from the same equivalence class
					 */
					if (ba_set_bit(t->collision_detector, hashcode)) {
						has_collision = true;
						break;
					}
					ptr = ptr->next;
				}
				/*
				 * don't need to continue looking at the other equivalence classes
				 * if we already have found a collision
				 */
				if (has_collision)
					break;	
				cls = cls->next;
			}
			++iterations;
			if (!has_collision)
				break;
			/* try other asso_values[] */
			if (t->jump != 0) {
				/*
				 * The way we try various values for
				 *   asso_values[step->_changing[0],...step->_changing[k-1]]
				 * is like this:
				 * for (bound = 0,1,...)
				 *   for (ii = 0,...,k-1)
				 *     iter[ii] := bound
				 *     iter[0..ii-1] := values <= bound
				 *     iter[ii+1..k-1] := values < bound
				 * and
				 *   asso_values[step->_changing[i]] =
				 *     _initial_asso_value + iter[i] * _jump.
				 * This makes it more likely to find small asso_values[].
				 */
				u32 bound;
				s32 i;

				bound = iter[ii];
				i = 0;
				loop {
					u32 c;

					if (i >= ii)
						break;
					c = step->changing[i];
					++(iter[i]);
					t->asso_values[c] = (t->asso_values[c] + t->jump)
								& (step->asso_value_max - 1);
					if (iter[i] <= bound)
						goto found_next;
					t->asso_values[c] = (t->asso_values[c] - iter[i] * t->jump)
								& (step->asso_value_max - 1);
					iter[i] = 0;
					++i;
				}
				i = ii + 1;
				loop {
					u32 c;

					if (i >= k)
						break;
					c = step->changing[i];
					++(iter[i]);
					t->asso_values[c] = (t->asso_values[c] + t->jump)
								& (step->asso_value_max - 1);
					if (iter[i] < bound)
						goto found_next;
					t->asso_values[c] = (t->asso_values[c] - iter[i] * t->jump)
								& (step->asso_value_max - 1);
					iter[i] = 0;
					++i;
				}
				/* switch from one ii to the next */
				{
					u32 c;

					c = step->changing[ii];
					t->asso_values[c] = (t->asso_values[c] - bound * t->jump)
								& (step->asso_value_max - 1);
					iter[ii] = 0;
				}
				/* here all iter[i] == 0 */
				++ii;
				if (ii == k) {
					ii = 0;
					++bound;
					if (bound == step->asso_value_max) {
						/*
						 * Out of search space!  We can either backtrack, or
						 * increase the available search space of this step.
						 * It seems simpler to choose the latter solution.
						 */
						step->asso_value_max = 2 * step->asso_value_max;
						if (step->asso_value_max > t->asso_value_max) {
							t->asso_value_max = step->asso_value_max;
							/* reinitialize _max_hash_value */
							t->max_hash_value = (t->hash_includes_len ? t->max_key_len : 0)
									+ (t->asso_value_max - 1) * t->max_selchars_length;
							/* reinitialize _collision_detector */
							free(t->collision_detector);
							t->collision_detector = ba_new(
									t->max_hash_value + 1);
						}
					}
				}
				{
					u32 c;

					c = step->changing[ii];
					iter[ii] = bound;
					t->asso_values[c] = (t->asso_values[c] + bound * t->jump)
								& (step->asso_value_max - 1);
				}
				found_next: ;
			} else {
				/* random */
				u32 c;

				c = step->changing[ii];
				t->asso_values[c] = (t->asso_values[c] + rand())
								& (step->asso_value_max - 1);
				/* next time, change the next c */
				++ii;
				if (ii == k)
					ii = 0;
			}
		}
		free(iter);	
		if (OPTS(DEBUG)) {
			u32 i;

          		fprintf(stderr, "Step %u chose _asso_values[", stepno);
			i = 0;
			loop {
				if (i >= step->changing_count)
					break;
				if (i > 0)
					fprintf(stderr, ",");
				fprintf(stderr, "'%c'", step->changing[i]);
				++i;
			}			
			fprintf (stderr, "] in %u iterations.\n", iterations);
		}
		step = step->next;
	}
	/* free allocated memory */
	loop {
		struct Step *step;

		if (steps == 0)
			break;
		step = steps;
		steps = step->next;
		free(step->changing);	
		free(step->undetermined);
		delete_partition(step->partition);
		free(step);
	}
}/*}}}*/
/*{{{ chr_count_possible_collisions */
/*
 * compute the possible number of collisions when asso_values[c] is chosen, leading to the given
 * partition
 */
static u32 schr_count_possible_collisions(struct Search *t,
							struct EquivalenceClass *partition, u32 c)
{
	/*
	 * Every equivalence class p is split according to the frequency of occurrence of c, leading
	 * to equivalence classes p1, p2, ...
	 * This leads to   (|p|^2 - |p1|^2 - |p2|^2 - ...)/2  possible collisions.
	 * Return the sum of this expression over all equivalence classes.
	 */
	u32 sum;
	u32 m;
	u32 *split_cardinalities;
	struct EquivalenceClass *cls;

	sum = 0;
	m = t->max_selchars_length;
	split_cardinalities = calloc(m + 1, sizeof(*split_cardinalities));
	cls = partition;
	loop {
		u32 i;
		struct Keyword_List *tmp;

		if (cls == 0)
			break;
		memset(split_cardinalities, 0, (m + 1) * sizeof(*split_cardinalities));
		tmp = cls->keywords;
		loop {
			struct Keyword *kw;
			u32 count;
			s32 i;

			if (tmp == 0)
				break;
			kw = tmp->kw;
			count = 0;
			i = 0;
			loop {
				if (i >= kw->selchars_length)
					break;
				if (kw->selchars[i] == c)
					++count;
				++i;
			}
			++(split_cardinalities[count]);
			tmp = tmp->next;
		}
		sum += cls->cardinality * cls->cardinality;
		i = 0;
		loop {
			if (i > m)
				break;
			sum -= split_cardinalities[i] * split_cardinalities[i];
			++i;
		}
		cls = cls->next;
	}
	free(split_cardinalities);
	return sum;
}
/*}}}*/
/*{{{ schr_compute_partition */
static struct EquivalenceClass *schr_compute_partition(struct Search *t, bool *undetermined)
{
	struct EquivalenceClass *partition;
	struct EquivalenceClass *partition_last;
	struct Keyword_List *tmp;
	struct EquivalenceClass *cls;

	partition = 0;
	partition_last = 0;
	tmp = t->head;
	loop {
		struct Keyword *kw;
		u32 *undetermined_chars;
		u32 undetermined_chars_length;
		s32 i;
		struct EquivalenceClass *equclass;
		struct Keyword_List *cons;

		if (tmp == 0)
			break;
		kw = tmp->kw;
		undetermined_chars = calloc(kw->selchars_length, sizeof(*undetermined_chars));
		undetermined_chars_length = 0;
		i = 0;
		loop {
			if (i >= kw->selchars_length)
				break;
			if (undetermined[kw->selchars[i]] != 0) {
				undetermined_chars[undetermined_chars_length] = kw->selchars[i];
				++undetermined_chars_length;
			}
			++i;
		}
		/* look up the equivalence class to which this keyword belongs */
		equclass = partition;
		loop {
			if (equclass == 0)
				break;
			if (equclass->undetermined_chars_length == undetermined_chars_length
					&& equals(equclass->undetermined_chars, undetermined_chars,
									undetermined_chars_length))
				break;
			equclass = equclass->next;
		}
		if (equclass == 0) {
			equclass = calloc(1, sizeof(*equclass));
			equclass->undetermined_chars = undetermined_chars;
			equclass->undetermined_chars_length = undetermined_chars_length;
			if (partition != 0)
				partition_last->next = equclass;
			else
				partition = equclass;
			partition_last = equclass;
		} else
			free(undetermined_chars);
		/* add the keyword to the equivalence class */
		cons = kwl_new(kw);
		if (equclass->keywords != 0) 
			equclass->keywords_last->next = cons;
		else
			equclass->keywords = cons;
		equclass->keywords_last = cons;
		++(equclass->cardinality);
		tmp = tmp->next;
	}
	/* Free some of the allocated memory. The caller doesn't need it. */
	cls = partition;
	loop {
		if (cls == 0)
			break;
		free(cls->undetermined_chars);
		cls = cls->next;
	}
	return partition;
}/*}}}*/
/*{{{ schr_prepare_asso_values */
/* initializes the asso_values[] related parameters */
static void schr_prepare_asso_values(struct Search *t)
{
	struct Keyword_List *tmp;
	struct PositionIterator *iter;
	s32 non_linked_length;
	u32 asso_value_max;

	/* initialize each keyword's _selchars array */
	schr_init_selchars_multiset(t, t->key_positions, t->alpha_unify, t->alpha_inc);
	/* compute the maximum _selchars_length over all keywords */
	iter = pos_iterator(t->key_positions, t->max_key_len);
	t->max_selchars_length = positer_remaining(iter);
	positer_del(iter);
	/*
	 * Check for duplicates, i.e. keywords with the same _selchars array (and - if
	 * _hash_includes_len - also the same length).  We deal with these by building an
	 * equivalence class, so that only 1 keyword is representative of the entire collection.
	 * Only this representative remains in the keyword list; the others are accessible through
	 * the _duplicate_link chain, starting at the representative.  This *greatly* simplifies
	 * processing during later stages of the program.  Set _total_duplicates and _list_len =
	 * _total_keys - _total_duplicates.
	 */
	{
		struct Hash_Table *representatives;
		struct Keyword_List *prev;

		t->list_len = t->total_keys;
		t->total_duplicates = 0;

		representatives = ht_new(t->list_len, !t->hash_includes_len);
		prev = 0; /* list node before temp */
		tmp = t->head;
		loop {
			struct Keyword *kw;
			struct Keyword *other_kw;
			struct Keyword_List *garbage;

			if (tmp == 0)
				break;
			kw = tmp->kw;
			other_kw = ht_insert(representatives, kw);
			garbage = 0;
			if (other_kw != 0) {
				++(t->total_duplicates);
				(t->list_len)--;
            			/* remove keyword from the main list */
				prev->next = tmp->next;
				garbage = tmp;
				/* and insert it on other_keyword's duplicate list */
				kw->duplicate_link = other_kw->duplicate_link;
				other_kw->duplicate_link = kw;
				/* complain if user hasn't enabled the duplicate option */
				if (!OPTS(DUP) || OPTS(DEBUG)) {
					s32 j;

					fprintf(stderr, "Key link: \"%.*s\" = \"%.*s\", with key set \"", kw->allchars_length, kw->allchars, other_kw->allchars_length, other_kw->allchars);
					j = 0;
					loop {
						if (j >= kw->selchars_length)
							break;
						putc(kw->selchars[j], stderr);
						++j;
					}
                			fprintf(stderr, "\".\n");
				}
			} else {
				kw->duplicate_link = 0;
				prev = tmp;
			}
			tmp = tmp->next;
			if (garbage != 0)
				kwl_del(garbage);
		}
		if (OPTS(DEBUG))
			ht_dump(representatives);
		ht_del(representatives);
	}
	/*
	 * Exit program if duplicates exists and option[DUP] not set, since we don't want to
	 * continue in this case. (We don't want to turn on option[DUP] implicitly, because the
	 * generated code is usually much slower.
	 */
	if (t->total_duplicates != 0) {
		if (OPTS(DUP))
			fprintf(stderr, "%d input keys have identical hash values, examine output carefully...\n", t->total_duplicates);
		else {
			fprintf(stderr, "%d input keys have identical hash values,\n", t->total_duplicates);
			if (OPTS(POSITIONS))
				fprintf(stderr, "try different key positions or use option -D.\n");
			else
				fprintf(stderr, "use option -D.\n");
			exit(1);
		}
	}
	/* compute the occurrences of each character in the alphabet */
	t->occurrences = calloc(t->alpha_size, sizeof(*(t->occurrences)));
	tmp = t->head;
	loop {
		struct Keyword *kw;
		u32 *ptr;
		s32 count;

		if (tmp == 0)
			break;
		kw = tmp->kw;
		ptr = kw->selchars;
		count = kw->selchars_length;
		loop {
			if (count <= 0)
				break;
			++(t->occurrences[*ptr]);
			++ptr;
			count--;
		}
		tmp = tmp->next;
	}
	/* memory allocation */
	t->asso_values = calloc(t->alpha_size, sizeof(*(t->asso_values)));
	non_linked_length = t->list_len;
	asso_value_max = (u32)(non_linked_length * options->size_multiple);
	/*
	 * Round up to the next power of two. This makes it easy to ensure an _asso_value[c] is >= 0
	 * and < asso_value_max. Also, the jump value being odd, it guarantees that
	 * Search::try_asso_value() will iterate through different values for _asso_value[c].
	 */
	if (asso_value_max == 0)
		asso_value_max = 1;
	asso_value_max |= asso_value_max >> 1;
	asso_value_max |= asso_value_max >> 2;
	asso_value_max |= asso_value_max >> 4;
	asso_value_max |= asso_value_max >> 8;
	asso_value_max |= asso_value_max >> 16;
	++asso_value_max;
	t->asso_value_max = asso_value_max;
	/*
	 * given the bound for _asso_values[c], we have a bound for the possible hash values, as
	 * computed in compute_hash()
	 */
	t->max_hash_value = (t->hash_includes_len ? t->max_key_len : 0) + (t->asso_value_max - 1)
									* t->max_selchars_length;
	/* allocate a sparse bit vector for detection of collisions of hash values */
	t->collision_detector = ba_new(t->max_hash_value + 1);
	if (OPTS(DEBUG)) {
		s32 field_width;
		s32 i;

      		fprintf (stderr, "total non-linked keys = %d\nmaximum associated value is %d\nmaximum size of generated hash table is %d\n", non_linked_length, asso_value_max, t->max_hash_value);
		field_width = 0;
		{
			struct Keyword_List *tmp;

			tmp = t->head;
			loop {
				struct Keyword *kw;

				if (tmp == 0)
					break;
				kw = tmp->kw;
				if (field_width < kw->selchars_length)
					field_width = kw->selchars_length;
				tmp = tmp->next;
			}
		}
		fprintf (stderr, "\ndumping the keyword list without duplicates\n");
		fprintf (stderr, "keyword #, %*s, keyword\n", field_width, "keysig");
		i = 0;
		tmp = t->head;
		loop {
			struct Keyword *kw;
			s32 j;

			if (tmp == 0)
				break;
			kw = tmp->kw;
			++i;
  			fprintf(stderr, "%9d, ", i);
			if (field_width > kw->selchars_length)
				fprintf(stderr, "%*s", field_width - kw->selchars_length, "");
			j = 0;
			loop {
				if (j >= kw->selchars_length)
					break;
				putc(kw->selchars[j], stderr);
				++j;
			}
			fprintf(stderr, ", %.*s\n", kw->allchars_length, kw->allchars);
			tmp = tmp->next;
		}
      		fprintf(stderr, "\nend of keyword list\n\n");
	}
	if (OPTS(RANDOM) || options->jump == 0)
		srand((long)time(0));
	t->initial_asso_value = (OPTS(RANDOM) ?  -1 : options->initial_asso_value);
	t->jump = options->jump;
}/*}}}*/
/*{{{ schr_unchanged_partition */
/* Test whether adding c to the undetermined characters changes the given partition. */
static bool schr_unchanged_partition(struct Search *t, struct EquivalenceClass *partition, u32 c)
{
	struct EquivalenceClass *cls;

	cls = partition;
	loop {
		u32 first_count;
		struct Keyword_List *tmp;

		if (cls == 0)
			break;
		first_count = U32_MAX;
		tmp = cls->keywords;
		loop {
			struct Keyword *kw;
			u32 count;
			s32 i;

			if (tmp == 0)
				break;
			kw = tmp->kw;
			count = 0;
			i = 0;
			loop {
				if (i >= kw->selchars_length)
					break;
				if (kw->selchars[i] == c)
					++count;
				++i;
			}
			if (tmp == cls->keywords)
				first_count = count;
			else if (count != first_count)
				/* c would split this equivalence class */
				return false;
			tmp = tmp->next;
		}
		cls = cls->next;
	}
	return true;
}
/*}}}*/
/*{{{ schr_compute_hash */
/*
 * computes a keyword's hash value, relative to the current _asso_values[], and stores it in
 * keyword->_hash_value
 */
static s32 schr_compute_hash(struct Search *t, struct Keyword *kw)
{
	s32 sum;
	u32 *p;
	s32 i;

	sum = t->hash_includes_len ? kw->allchars_length : 0;
	p = kw->selchars;
	i = kw->selchars_length;
	loop {
		if (i <= 0)
			break;
		sum += t->asso_values[*p];
		++p;
		i--;
	}
	kw->hash_value = sum;
	return sum;
}/*}}}*/
/*{{{ schr_sort */
static void schr_sort(struct Search *t)
{
	t->head = mergesort_list(t->head, less_by_hash_value);
}
/*}}}*/
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/globals.h"
#include "namespace/search.h"
#include "namespace/keyword.h"
#include "namespace/keyword_list.h"
#include "namespace/options.h"
#include "namespace/positions.h"
#include "namespace/hash-table.h"
#include "namespace/bool-array.h"
#include "namespace/search.c"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
