#ifndef GPERF_OPTIONS_C
#define GPERF_OPTIONS_C
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <ctype.h>
#include <string.h>

#include "globals.h"
#include "options.h"
#include "version.h"
#include "positions.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/globals.h"
#include "namespace/options.h"
#include "namespace/positions.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ defaults */
/* default struct initializer suffix */
static u8 *DEFAULT_INITIALIZER_SUFFIX = "";
/* default name for the key component */
static u8 *DEFAULT_SLOT_NAME = "name";
/* default delimiters that separate keywords from their attributes */
static u8 *DEFAULT_DELIMITERS = ",";
/* default name for generated hash function */
static u8 *DEFAULT_HASH_NAME = "hash";
/* default name for generated lookup function */
static u8 *DEFAULT_FUNCTION_NAME = "in_word_set";
/* default name for the generated class */
static u8 *DEFAULT_CLASS_NAME = "Perfect_Hash";
/* default name for string pool */
static u8 *DEFAULT_STRINGPOOL_NAME = "stringpool";
/* default prefix for constants */
static u8 *DEFAULT_CONSTANTS_PREFIX = "";
/* default name for generated hash table array */
static u8 *DEFAULT_WORDLIST_NAME = "wordlist";
/* default name for generated length table array */
static u8 *DEFAULT_LENGTHTABLE_NAME = "lengthtable";
/*}}} default -- END */
/*{{{ opts_new */
static struct Options *opts_new(void)
{
	struct Options *t;

	t = calloc(1, sizeof(*t));
	t->option_word = OPTS_ANSIC;
	t->jump = OPTS_DEFAULT_JUMP_VALUE;
	t->total_switches = 1;
	t->size_multiple = 1;
	t->function_name = DEFAULT_FUNCTION_NAME;
	t->slot_name = DEFAULT_SLOT_NAME;
	t->initializer_suffix = DEFAULT_INITIALIZER_SUFFIX;
	t->class_name = DEFAULT_CLASS_NAME;
	t->hash_name = DEFAULT_HASH_NAME;
	t->wordlist_name = DEFAULT_WORDLIST_NAME;
	t->lengthtable_name = DEFAULT_LENGTHTABLE_NAME;
	t->stringpool_name = DEFAULT_STRINGPOOL_NAME;
	t->constants_prefix = DEFAULT_CONSTANTS_PREFIX;
	t->delimiters = DEFAULT_DELIMITERS;
	t->key_positions = pos_new();
	return t;
}/*}}}*/
/*{{{ opts_del */
static void opts_del(struct Options *t)
{
	if (OPTS(DEBUG)) {
		struct PositionIterator *iter;
		s32 pos;

		fprintf(stderr, "\ndumping Options:"
			"\nTYPE is........: %s"
			"\nUPPERLOWER is..: %s"
			"\nKRC is.........: %s"
			"\nC is...........: %s"
			"\nANSIC is.......: %s"
			"\nCPLUSPLUS is...: %s"
			"\nSEVENBIT is....: %s"
			"\nLENTABLE is....: %s"
			"\nCOMP is........: %s"
			"\nCONST is.......: %s"
			"\nENUM is........: %s"
			"\nINCLUDE is.....: %s"
			"\nGLOBAL is......: %s"
			"\nNULLSTRINGS is.: %s"
			"\nSHAREDLIB is...: %s"
			"\nSWITCH is......: %s"
			"\nNOTYPE is......: %s"
			"\nDUP is.........: %s"
			"\nNOLENGTH is....: %s"
			"\nRANDOM is......: %s"
			"\nDEBUG is.......: %s"
			"\nlookup function name = %s"
			"\nhash function name = %s"
			"\nword list name = %s"
			"\nlength table name = %s"
			"\nstring pool name = %s"
			"\nslot name = %s"
			"\ninitializer suffix = %s"
			"\nasso_values iterations = %d"
			"\njump value = %d"
			"\nhash table size multiplier = %g"
			"\ninitial associated value = %d"
			"\ndelimiters = %s"
			"\nnumber of switch statements = %d\n",
			OPTS(TYPE) ? "enabled" : "disabled",
			OPTS(UPPERLOWER) ? "enabled" : "disabled",
			OPTS(KRC) ? "enabled" : "disabled",
			OPTS(C) ? "enabled" : "disabled",
			OPTS(ANSIC) ? "enabled" : "disabled",
			OPTS(CPLUSPLUS) ? "enabled" : "disabled",
			OPTS(SEVENBIT) ? "enabled" : "disabled",
			OPTS(LENTABLE) ? "enabled" : "disabled",
			OPTS(COMP) ? "enabled" : "disabled",
			OPTS(CONST) ? "enabled" : "disabled",
			OPTS(ENUM) ? "enabled" : "disabled",
			OPTS(INCLUDE) ? "enabled" : "disabled",
			OPTS(GLOBAL) ? "enabled" : "disabled",
			OPTS(NULLSTRINGS) ? "enabled" : "disabled",
			OPTS(SHAREDLIB) ? "enabled" : "disabled",
			OPTS(SWITCH) ? "enabled" : "disabled",
			OPTS(NOTYPE) ? "enabled" : "disabled",
			OPTS(DUP) ? "enabled" : "disabled",
			OPTS(NOLENGTH) ? "enabled" : "disabled",
			OPTS(RANDOM) ? "enabled" : "disabled",
			OPTS(DEBUG) ? "enabled" : "disabled",
			t->function_name, t->hash_name, t->wordlist_name, t->lengthtable_name,
			t->stringpool_name, t->slot_name, t->initializer_suffix,
			t->asso_iterations, t->jump, t->size_multiple, t->initial_asso_value,
			t->delimiters, t->total_switches);
			if (t->key_positions->useall)
				fprintf(stderr, "all characters are used in the hash function\n");
			else {
				fprintf(stderr, "maximum keysig size = %d\nkey positions are: \n", t->key_positions->size);
				iter = pos_iterator_all(t->key_positions);
				loop {
					pos = positer_next(iter);
					if (pos == POSITER_EOS)
						break;
					if (pos == POS_LASTCHAR)
						fprintf(stderr, "$\n");
					else
						fprintf(stderr, "%d\n", pos + 1);
					
				}
			}
			fprintf (stderr, "finished dumping Options\n");
	}
	pos_del(t->key_positions);
	free(t);
}/*}}}*/
/*{{{ opts_long_options
Parses the command line Options and sets appropriate flags in option_word. */
static const struct option opts_long_options[] =
{
  { "output-file", required_argument, NULL, CHAR_MAX + 1 },
  { "ignore-case", no_argument, NULL, CHAR_MAX + 2 },
  { "delimiters", required_argument, NULL, 'e' },
  { "struct-type", no_argument, NULL, 't' },
  { "language", required_argument, NULL, 'L' },
  { "slot-name", required_argument, NULL, 'K' },
  { "initializer-suffix", required_argument, NULL, 'F' },
  { "hash-fn-name", required_argument, NULL, 'H' }, /* backward compatibility */
  { "hash-function-name", required_argument, NULL, 'H' },
  { "lookup-fn-name", required_argument, NULL, 'N' }, /* backward compatibility */
  { "lookup-function-name", required_argument, NULL, 'N' },
  { "class-name", required_argument, NULL, 'Z' },
  { "seven-bit", no_argument, NULL, '7' },
  { "compare-strncmp", no_argument, NULL, 'c' },
  { "readonly-tables", no_argument, NULL, 'C' },
  { "enum", no_argument, NULL, 'E' },
  { "includes", no_argument, NULL, 'I' },
  { "global-table", no_argument, NULL, 'G' },
  { "constants-prefix", required_argument, NULL, CHAR_MAX + 5 },
  { "word-array-name", required_argument, NULL, 'W' },
  { "length-table-name", required_argument, NULL, CHAR_MAX + 4 },
  { "switch", required_argument, NULL, 'S' },
  { "omit-struct-type", no_argument, NULL, 'T' },
  { "key-positions", required_argument, NULL, 'k' },
  { "compare-strlen", no_argument, NULL, 'l' }, /* backward compatibility */
  { "compare-lengths", no_argument, NULL, 'l' },
  { "duplicates", no_argument, NULL, 'D' },
  { "fast", required_argument, NULL, 'f' },
  { "initial-asso", required_argument, NULL, 'i' },
  { "jump", required_argument, NULL, 'j' },
  { "multiple-iterations", required_argument, NULL, 'm' },
  { "no-strlen", no_argument, NULL, 'n' },
  { "occurrence-sort", no_argument, NULL, 'o' },
  { "optimized-collision-resolution", no_argument, NULL, 'O' },
  { "pic", no_argument, NULL, 'P' },
  { "string-pool-name", required_argument, NULL, 'Q' },
  { "null-strings", no_argument, NULL, CHAR_MAX + 3 },
  { "random", no_argument, NULL, 'r' },
  { "size-multiple", required_argument, NULL, 's' },
  { "help", no_argument, NULL, 'h' },
  { "version", no_argument, NULL, 'v' },
  { "debug", no_argument, NULL, 'd' },
  { NULL, no_argument, NULL, 0 }
};/*}}}*/
/*{{{ opts_parse_options */
static void opts_parse_options(struct Options *t, u32 argc, u8 **argv)
{
	opts_program_name = (u8*)argv[0];
	t->argument_count  = argc;
	t->argument_vector = argv;

	loop {
		int option_char;

		option_char = getopt_long(t->argument_count, t->argument_vector,
			"acCdDe:Ef:F:gGhH:i:Ij:k:K:lL:m:nN:oOpPQ:rs:S:tTvW:Z:7", opts_long_options,
											NULL);
		if (option_char == -1)
			break;
		switch (option_char) {
		case 'a': /* generated code uses the ANSI prototype format */
			break; /* This is now the default */
		case 'c': /* generate strncmp rather than strcmp */
			t->option_word |= OPTS_COMP;
			break;
		case 'C': /* make the generated tables readonly (const) */
			t->option_word |= OPTS_CONST;
			break;
		case 'd': /* enable debugging option */
			t->option_word |= OPTS_DEBUG;
			fprintf(stderr, "Starting program %s, version %s, with debugging on.\n", opts_program_name, cgperf_version_string);
			break;
		case 'D': /* enable duplicate option */
			t->option_word |= OPTS_DUP;
			break;
		case 'e': /* specify keyword/attribute separator */
			t->delimiters = /*getopt*/(u8*)optarg;
			break;
		case 'E':
			t->option_word |= OPTS_ENUM;
			break;
		case 'f': /* generate the hash table "fast" */
			break; /* Not needed any more */
		case 'F':
			t->initializer_suffix = /*getopt*/(u8*)optarg;
			break;
		case 'g': /* use the 'inline' keyword for generated sub-routines, ifdef __GNUC__ */
			break; /* This is now the default */
		case 'G': /* make the keyword table a global variable */
			t->option_word |= OPTS_GLOBAL;
			break;
		case 'h': /* displays a list of helpful Options to the user */
			opts_long_usage(stdout);
			exit(0);
		case 'H':  /* sets the name for the hash function */
			t->hash_name = /*getopt*/(u8*)optarg;
			break;
		case 'i': /* sets the initial value for the associated values array */
			t->initial_asso_value = atoi(/*getopt*/optarg);
			if (t->initial_asso_value < 0)
				fprintf(stderr, "Initial value %d should be non-zero, ignoring and continuing.\n", t->initial_asso_value);
			if (OPTS(RANDOM))
				fprintf(stderr, "warning, -r option superceeds -i, ignoring -i option and continuing\n");
			break;
		case 'I': /* enable #include statements */
			t->option_word |= OPTS_INCLUDE;
			break;
		case 'j': /* sets the jump value, must be odd for later algorithms */
			t->jump = atoi (/*getopt*/optarg);
			if (t->jump < 0) {
				fprintf(stderr, "Jump value %d must be a positive number.\n", t->jump);
				opts_short_usage(stderr);
				exit(1);
			} else if ((t->jump != 0) && ((t->jump % 2) == 0))
				fprintf (stderr, "Jump value %d should be odd, adding 1 and continuing...\n", t->jump++);
			break;
		case 'k': { /* sets key positions used for hash function */
			t->option_word |= OPTS_POSITIONS;
			s32 BAD_VALUE = -3;
			s32 EOS = POSITER_EOS;
			s32 value;
			struct PositionStringParser *sparser;

			sparser = posstrp_new(/*getopt*/(u8*)optarg, 1,
				POS_MAX_KEY_POS, POS_LASTCHAR, BAD_VALUE, EOS);

			if (/*getopt*/optarg[0] == '*') /* use all the characters for hashing!!!! */
				pos_set_useall(t->key_positions, true);
			else {
				s32 *key_positions;
				s32 *key_pos;
                		u32 total_keysig_size;

				pos_set_useall(t->key_positions, false);
				key_positions = t->key_positions->positions;

				key_pos = key_positions;
				loop {
					value = posstrp_nextPosition(sparser);
					if (value == EOS)
						break;
					if (value == BAD_VALUE) {
						fprintf(stderr, "Invalid position value or range, use 1,2,3-%d,'$' or '*'.\n", POS_MAX_KEY_POS);
						opts_short_usage(stderr);
						exit(1);
					}
					if ((key_pos - key_positions) == POS_MAX_SIZE) {
						/*
						 * More than Positions_max_size key positions.
						 * Since all key positions are in the range
						 * 0..Positions_max_key_pos-1 or == Positions_lastchar,
						 * there must be duplicates.
						 */
						fprintf(stderr, "Duplicate key positions selected\n");
						opts_short_usage(stderr);
						exit(1);
					}
					if (value != POS_LASTCHAR)
						/* We use 0-based indices in the class Positions */
						value = value - 1;
					*key_pos = value;
					++key_pos;
				}
				total_keysig_size = key_pos - key_positions;
				if (total_keysig_size == 0) {
					fprintf(stderr, "No key positions selected.\n");
					opts_short_usage(stderr);
                   			exit(1);
				}
				t->key_positions->size = total_keysig_size;
				/*
				 * Sorts the key positions *IN REVERSE ORDER!!*
				 * This makes further routines more efficient.
				 * Especially when generating code.
				 */
				if (!pos_sort(t->key_positions)) {
					fprintf(stderr, "Duplicate key positions selected\n");
					opts_short_usage(stderr);
					exit(1);
				}
			}
			break;}
		case 'K':/* make this the keyname for the keyword component field */
			t->slot_name = /*getopt*/optarg;
			break;
        	case 'l':/* create length table to avoid extra string compares */
			t->option_word |= OPTS_LENTABLE;
			break;
        	case 'L':/* deal with different generated languages */
			t->language = 0;
			opts_set_language(t,/*getopt*/optarg);
			break;
		case 'm':/* multiple iterations for finding good asso_values */
			t->asso_iterations = atoi(/*getopt*/optarg);
			if (t->asso_iterations < 0) {
				fprintf(stderr, "asso_iterations value must not be negative, assuming 0\n");
				 t->asso_iterations = 0;
			}
			break;
		case 'n':/* don't include the length when computing hash function */
			t->option_word |= OPTS_NOLENGTH;
			break;
		case 'N':/* make generated lookup function name be optarg */
			t->function_name = /*getopt*/optarg;
			break;
		case 'o':/* order input by frequency of key set occurrence */
			break; /* not needed any more */
		case 'O':/* optimized choice during collision resolution */
			break; /* not needed any more */
		case 'p':/* generated lookup function a pointer instead of int */
			break; /* this is now the default */
		case 'P':/* optimize for position-independent code */
			t->option_word |= OPTS_SHAREDLIB;
			break;
		case 'Q':/* sets the name for the string pool */
			t->stringpool_name = /*getopt*/optarg;
			break;
		case 'r':/* utilize randomness to initialize the associated values table */
			t->option_word |= OPTS_RANDOM;
			if (t->initial_asso_value != 0)
				fprintf(stderr, "warning, -r option supersedes -i, disabling -i option and continuing\n");
			break;
		case 's':{/* range of associated values, determines size of final table */
			f32 numerator;
			f32 denominator;
			bool invalid;
			u8 *endptr;

			denominator = 1;
			invalid = false;
			numerator = strtod(/*getopt*/optarg, &endptr);
			if (endptr == /*getopt*/(u8*)optarg)
				invalid = true;
			else if (*endptr != '\0') {
				if (*endptr == '/') {
					u8 *denomptr;

					denomptr = endptr + 1;
					denominator = strtod(denomptr, &endptr);
					if (endptr == denomptr || *endptr != '\0')
						invalid = true;
				} else
					invalid = true;
			}
			if (invalid) {
				fprintf(stderr, "Invalid value for option -s.\n");
				opts_short_usage(stderr);
				exit (1);
			}
			t->size_multiple = numerator / denominator;
			/* backward compatibility: -3 means 1/3 */
			if (t->size_multiple < 0)
				t->size_multiple = 1 / (-t->size_multiple);
			/* catch stupid users and port to C the c++ from stupid coders */
			if (t->size_multiple == 0)
				t->size_multiple = 1;
			/* warnings */
			if (t->size_multiple > 50)
				fprintf(stderr, "Size multiple %g is excessive, did you really mean this?! (try '%s --help' for help)\n", t->size_multiple, opts_program_name);
			else if (t->size_multiple < 0.01f)
				fprintf(stderr, "Size multiple %g is extremely small, did you really mean this?! (try '%s --help' for help)\n", t->size_multiple, opts_program_name);
			break;}
		case 'S':/* generate switch statement output, rather than lookup table */
			t->option_word |= OPTS_SWITCH;
			t->total_switches = atoi(/*getopt*/optarg);
			if (t->total_switches <= 0) {
				fprintf(stderr, "number of switches %s must be a positive number\n", /*getopt*/optarg);
				opts_short_usage (stderr);
				exit(1);
			}
			break;
		case 't':/* enable the TYPE mode, allowing arbitrary user structures */
			t->option_word |= OPTS_TYPE;
			break;
		case 'T':/* don't print structure definition */
			t->option_word |= OPTS_NOTYPE;
			break;
		case 'v':/* print out the version and quit */
			fprintf(stdout, "GNU gperf %s\n", cgperf_version_string);
			fprintf(stdout, "Copyright (C) %s Free Software Foundation, Inc.\n\
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n\
",
"1989-2018");
			fprintf(stdout, "Written by %s and %s. C89 with benign bits of C99/C11 port by Sylvain BERTRAND\n", "Douglas C. Schmidt", "Bruno Haible");
			exit(0);
		case 'W':/* sets the name for the hash table array */
			t->wordlist_name = /*getopt*/optarg;
			break;
		case 'Z':/* set the class name */
			t->class_name = /*getopt*/optarg;
			break;
		case '7':/* assume 7-bit characters */
			t->option_word |= OPTS_SEVENBIT;
			break;
		case CHAR_MAX + 1:/* set the output file name */
			t->output_file_name = /*getopt*/optarg;
			break;
		case CHAR_MAX + 2:/* case insignificant */
			t->option_word |= OPTS_UPPERLOWER;
			break;
		case CHAR_MAX + 3:/* use NULL instead of "" */
			t->option_word |= OPTS_NULLSTRINGS;
			break;
		case CHAR_MAX + 4:/* sets the name for the length table array */
			t->lengthtable_name = /*getopt*/optarg;
			break;
		case CHAR_MAX + 5:/* sets the prefix for the constants */
			t->constants_prefix = /*getopt*/optarg;
			break;
		default:
			opts_short_usage(stderr);
			exit(1);
		}
	}
	if (/*getopt*/optind < argc)
		t->input_file_name = argv[/*getopt*/optind++];

	if (/*getopt*/optind < argc) {
		fprintf(stderr, "Extra trailing arguments to %s.\n", opts_program_name);
		opts_short_usage(stderr);
		exit(1);
	}
}/*}}}*/
/*{{{ opts_short_usage */
static void opts_short_usage(FILE * stream)
{
	fprintf(stream, "Try '%s --help' for more information.\n", opts_program_name);
}/*}}}*/
/*{{{ opts_long_usage */
static void opts_long_usage(FILE * stream)
{
	fprintf(stream,
           "GNU 'gperf' generates perfect hash functions.\n");
	fprintf(stream, "\n");
	fprintf(stream,
           "Usage: %s [OPTION]... [INPUT-FILE]\n",
           opts_program_name);
	fprintf(stream, "\n");
	fprintf(stream,
           "If a long option shows an argument as mandatory, then it is mandatory\n"
           "for the equivalent short option also.\n");
	fprintf(stream, "\n");
	fprintf(stream,
           "Output file location:\n");
	fprintf(stream,
           "      --output-file=FILE Write output to specified file.\n");
	fprintf(stream,
           "The results are written to standard output if no output file is specified\n"
           "or if it is -.\n");
	fprintf(stream, "\n");
	fprintf(stream,
           "Input file interpretation:\n");
	fprintf(stream,
           "  -e, --delimiters=DELIMITER-LIST\n"
           "                         Allow user to provide a string containing delimiters\n"
           "                         used to separate keywords from their attributes.\n"
           "                         Default is \",\".\n");
	fprintf(stream,
           "  -t, --struct-type      Allows the user to include a structured type\n"
           "                         declaration for generated code. Any text before %%%%\n"
           "                         is considered part of the type declaration. Key\n"
           "                         words and additional fields may follow this, one\n"
           "                         group of fields per line.\n");
	fprintf(stream,
           "      --ignore-case      Consider upper and lower case ASCII characters as\n"
           "                         equivalent. Note that locale dependent case mappings\n"
           "                         are ignored.\n");
	fprintf(stream, "\n");
	fprintf(stream,
           "Language for the output code:\n");
	fprintf(stream,
           "  -L, --language=LANGUAGE-NAME\n"
           "                         Generates code in the specified language. Languages\n"
           "                         handled are currently C++, ANSI-C, C, and KR-C. The\n"
           "                         default is ANSI-C.\n");
	fprintf(stream, "\n");
	fprintf(stream,
           "Details in the output code:\n");
	fprintf(stream,
           "  -K, --slot-name=NAME   Select name of the keyword component in the keyword\n"
           "                         structure.\n");
	fprintf(stream,
           "  -F, --initializer-suffix=INITIALIZERS\n"
           "                         Initializers for additional components in the keyword\n"
           "                         structure.\n");
	fprintf(stream,
           "  -H, --hash-function-name=NAME\n"
           "                         Specify name of generated hash function. Default is\n"
           "                         'hash'.\n");
	fprintf(stream,
           "  -N, --lookup-function-name=NAME\n"
           "                         Specify name of generated lookup function. Default\n"
           "                         name is 'in_word_set'.\n");
	fprintf(stream,
           "  -Z, --class-name=NAME  Specify name of generated C++ class. Default name is\n"
           "                         'Perfect_Hash'.\n");
	fprintf(stream,
           "  -7, --seven-bit        Assume 7-bit characters.\n");
	fprintf(stream,
           "  -l, --compare-lengths  Compare key lengths before trying a string\n"
           "                         comparison. This is necessary if the keywords\n"
           "                         contain NUL bytes. It also helps cut down on the\n"
           "                         number of string comparisons made during the lookup.\n");
	fprintf(stream,
           "  -c, --compare-strncmp  Generate comparison code using strncmp rather than\n"
           "                         strcmp.\n");
	fprintf(stream,
           "  -C, --readonly-tables  Make the contents of generated lookup tables\n"
           "                         constant, i.e., readonly.\n");
	fprintf(stream,
           "  -E, --enum             Define constant values using an enum local to the\n"
           "                         lookup function rather than with defines.\n");
	fprintf(stream,
           "  -I, --includes         Include the necessary system include file <string.h>\n"
           "                         at the beginning of the code.\n");
	fprintf(stream,
           "  -G, --global-table     Generate the static table of keywords as a static\n"
           "                         global variable, rather than hiding it inside of the\n"
           "                         lookup function (which is the default behavior).\n");
	fprintf(stream,
           "  -P, --pic              Optimize the generated table for inclusion in shared\n"
           "                         libraries.  This reduces the startup time of programs\n"
           "                         using a shared library containing the generated code.\n");
	fprintf(stream,
           "  -Q, --string-pool-name=NAME\n"
           "                         Specify name of string pool generated by option --pic.\n"
           "                         Default name is 'stringpool'.\n");
	fprintf(stream,
           "      --null-strings     Use NULL strings instead of empty strings for empty\n"
           "                         keyword table entries.\n");
	fprintf(stream,
           "      --constants-prefix=PREFIX\n"
           "                         Specify prefix for the constants like TOTAL_KEYWORDS.\n");
	fprintf(stream,
           "  -W, --word-array-name=NAME\n"
           "                         Specify name of word list array. Default name is\n"
           "                         'wordlist'.\n");
	fprintf(stream,
           "      --length-table-name=NAME\n"
           "                         Specify name of length table array. Default name is\n"
           "                         'lengthtable'.\n");
	fprintf(stream,
           "  -S, --switch=COUNT     Causes the generated C code to use a switch\n"
           "                         statement scheme, rather than an array lookup table.\n"
           "                         This can lead to a reduction in both time and space\n"
           "                         requirements for some keyfiles. The COUNT argument\n"
           "                         determines how many switch statements are generated.\n"
           "                         A value of 1 generates 1 switch containing all the\n"
           "                         elements, a value of 2 generates 2 tables with 1/2\n"
           "                         the elements in each table, etc. If COUNT is very\n"
           "                         large, say 1000000, the generated C code does a\n"
           "                         binary search.\n");
	fprintf(stream,
           "  -T, --omit-struct-type\n"
           "                         Prevents the transfer of the type declaration to the\n"
           "                         output file. Use this option if the type is already\n"
           "                         defined elsewhere.\n");
	fprintf(stream, "\n");
	fprintf(stream,
           "Algorithm employed by gperf:\n");
	fprintf(stream,
           "  -k, --key-positions=KEYS\n"
           "                         Select the key positions used in the hash function.\n"
           "                         The allowable choices range between 1-%d, inclusive.\n"
           "                         The positions are separated by commas, ranges may be\n"
           "                         used, and key positions may occur in any order.\n"
           "                         Also, the meta-character '*' causes the generated\n"
           "                         hash function to consider ALL key positions, and $\n"
           "                         indicates the \"final character\" of a key, e.g.,\n"
           "                         $,1,2,4,6-10.\n",
           POS_MAX_KEY_POS);
	fprintf(stream,
           "  -D, --duplicates       Handle keywords that hash to duplicate values. This\n"
           "                         is useful for certain highly redundant keyword sets.\n");
	fprintf(stream,
           "  -m, --multiple-iterations=ITERATIONS\n"
           "                         Perform multiple choices of the -i and -j values,\n"
           "                         and choose the best results. This increases the\n"
           "                         running time by a factor of ITERATIONS but does a\n"
           "                         good job minimizing the generated table size.\n");
	fprintf(stream,
           "  -i, --initial-asso=N   Provide an initial value for the associate values\n"
           "                         array. Default is 0. Setting this value larger helps\n"
           "                         inflate the size of the final table.\n");
	fprintf(stream,
           "  -j, --jump=JUMP-VALUE  Affects the \"jump value\", i.e., how far to advance\n"
           "                         the associated character value upon collisions. Must\n"
           "                         be an odd number, default is %d.\n",
           OPTS_DEFAULT_JUMP_VALUE);
	fprintf(stream,
           "  -n, --no-strlen        Do not include the length of the keyword when\n"
           "                         computing the hash function.\n");
	fprintf(stream,
           "  -r, --random           Utilizes randomness to initialize the associated\n"
           "                         values table.\n");
	fprintf(stream,
           "  -s, --size-multiple=N  Affects the size of the generated hash table. The\n"
           "                         numeric argument N indicates \"how many times larger\n"
           "                         or smaller\" the associated value range should be,\n"
           "                         in relationship to the number of keys, e.g. a value\n"
           "                         of 3 means \"allow the maximum associated value to\n"
           "                         be about 3 times larger than the number of input\n"
           "                         keys\". Conversely, a value of 1/3 means \"make the\n"
           "                         maximum associated value about 3 times smaller than\n"
           "                         the number of input keys\". A larger table should\n"
           "                         decrease the time required for an unsuccessful\n"
           "                         search, at the expense of extra table space. Default\n"
           "                         value is 1.\n");
	fprintf(stream, "\n");
	fprintf(stream,
           "Informative output:\n"
           "  -h, --help             Print this message.\n"
           "  -v, --version          Print the gperf version number.\n"
           "  -d, --debug            Enables the debugging option (produces verbose\n"
           "                         output to the standard error).\n");
	fprintf(stream, "\n");
	fprintf(stream,
           "Report bugs to <bug-gperf@gnu.org>.\n");
}/*}}}*/
/*{{{ opts_set_language */
/* Sets the output language, if not already set */
void opts_set_language(struct Options *t, u8 *language)
{
	if (t->language != 0)
		return;
	t->language = language;
	t->option_word &= ~(OPTS_KRC | OPTS_C | OPTS_ANSIC | OPTS_CPLUSPLUS);
	if (strcmp(language, "KR-C") == 0)
		t->option_word |= OPTS_KRC;
	else if (strcmp (language, "C") == 0)
		t->option_word |= OPTS_C;
	else if (strcmp (language, "ANSI-C") == 0)
		t->option_word |= OPTS_ANSIC;
	else if (strcmp (language, "C++") == 0)
		t->option_word |= OPTS_CPLUSPLUS;
	else {
		fprintf(stderr, "unsupported language option %s, defaulting to ANSI-C\n", language);
		t->option_word |= OPTS_ANSIC;
	}
}/*}}}*/
/*{{{ opts_set_delimiters */
/* Sets the delimiters string, if not already set.  */
static void opts_set_delimiters(struct Options *t, u8 *delimiters)
{
	if (t->delimiters == DEFAULT_DELIMITERS)
		t->delimiters = delimiters;
}/*}}}*/
/*{{{ opts_set_slot_name */
/* sets the keyword key name, if not already set */
static void opts_set_slot_name(struct Options *t, u8 *name)
{
	if (t->slot_name == DEFAULT_SLOT_NAME)
		t->slot_name = name;
}/*}}}*/
/*{{{ opts_set_initializer_suffix */
/* sets the struct initializer suffix, if not already set */
static void opts_set_initializer_suffix(struct Options *t, u8 *initializers)
{
	if (t->initializer_suffix == DEFAULT_INITIALIZER_SUFFIX)
		t->initializer_suffix = initializers;
}/*}}}*/
/*{{{ opts_set_hash_name */
/* sets the hash function name, if not already set */
static void opts_set_hash_name(struct Options *t, u8 *name)
{
	if (t->hash_name == DEFAULT_HASH_NAME)
		t->hash_name = name;
}/*}}}*/
/*{{{ opts_set_function_name */
/* sets the generated function name, if not already set */
static void opts_set_function_name(struct Options *t, u8 *name)
{
	if (t->function_name == DEFAULT_FUNCTION_NAME)
		t->function_name = name;
}/*}}}*/
/*{{{ opts_set_class_name */
/* sets the generated class name, if not already set */
static void opts_set_class_name(struct Options *t, u8 *name)
{
	if (t->class_name == DEFAULT_CLASS_NAME)
		t->class_name = name;
}/*}}}*/
/*{{{ opts_set_stringpool_name */
/* sets the string pool name, if not already set */
static void opts_set_stringpool_name(struct Options *t, u8 *name)
{
	if (t->stringpool_name == DEFAULT_STRINGPOOL_NAME)
		t->stringpool_name = name;
}/*}}}*/
/*{{{ opts_set_constants_prefix */
/* sets the prefix for the constants, if not already set */
static void opts_set_constants_prefix(struct Options *t, u8 *prefix)
{
	if (t->constants_prefix == DEFAULT_CONSTANTS_PREFIX)
		t->constants_prefix = prefix;
}/*}}}*/
/*{{{ opts_set_wordlist_name */
/* sets the hash table array name, if not already set */
static void opts_set_wordlist_name(struct Options *t, u8 *name)
{
	if (t->wordlist_name == DEFAULT_WORDLIST_NAME)
		t->wordlist_name = name;
}/*}}}*/
/*{{{ opts_set_lengthtable_name */
/* sets the length table array name, if not already set */
static void opts_set_lengthtable_name(struct Options *t, u8 *name)
{
	if (t->lengthtable_name == DEFAULT_LENGTHTABLE_NAME)
		t->lengthtable_name = name;
}/*}}}*/
/*{{{ opts_set_total_switches */
/* sets the total number of switch statements, if not already set */
static void opts_set_total_switches(struct Options *t, s32 total_switches)
{
	if (!OPTS(SWITCH)) {
		t->option_word |= OPTS_SWITCH;
		t->total_switches = total_switches;
	}
}/*}}}*/
/*{{{ posstrp_new */
static struct PositionStringParser *posstrp_new(u8 *str, s32 low_bound,
	s32 high_bound, s32 end_word_marker, s32 error_value, s32 end_marker)
{
	struct PositionStringParser *t;

	t = calloc(1, sizeof(*t));
	t->str = str;
	t->low_bound = low_bound;
	t->high_bound = high_bound;
	t->end_word_marker = end_word_marker;
	t->error_value = error_value;
	t->end_marker = end_marker;
	t->in_range = false;
	return t;
}
/*}}}*/
/*{{{ posstrp_del */
static void posstrp_del(struct PositionStringParser *t)
{
	free(t);
}/*}}}*/
/*{{{ posstrp_nextPosition */
/* Returns the next key position from the given string */
static s32 posstrp_nextPosition(struct PositionStringParser *t)
{
	if (t->in_range) {
		/* We are inside a range. Return the next value from the range */
		if (++t->range_curr_value >= t->range_upper_bound)
			t->in_range = false;
		return t->range_curr_value;
	}
	/* we are not inside a range */
	/* Continue parsing the given string */
	loop {
		if (t->str[0] == 0)
			break;
		switch (t->str[0]) {
		case ',':
			/* Skip the comma */
			++(t->str);
			break;
		case '$':
			/* Valid key position */
			++(t->str);
			return t->end_word_marker;
		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9': {
			/* Valid key position */
			s32 curr_value;

			curr_value = 0;
			loop {
				if (!isdigit((int)(t->str[0])))
					break;
				curr_value = curr_value * 10 + (t->str[0] - '0');
				++(t->str);
			}
			if (t->str[0] == '-') {
				++(t->str);
				/* starting a range of key positions */
				t->in_range = true;

				t->range_upper_bound = 0;
				loop {
					if (!isdigit((int)(t->str[0])))
						break;
					t->range_upper_bound = t->range_upper_bound * 10
										+ (t->str[0] - '0');
					++(t->str);
				}
				/* Verify range's upper bound */
				if (!(t->range_upper_bound > curr_value && t->range_upper_bound
										<= t->high_bound))
					return t->error_value;
				t->range_curr_value = curr_value;
			}
			/* Verify range's lower bound */
			if (!(curr_value >= t->low_bound && curr_value <= t->high_bound))
				return t->error_value;
			return curr_value;
		}
		default:
			/* Invalid syntax.  */
			return t->error_value;
		}
	}
	return t->end_marker;
}/*}}}*/
/*{{{ opts_print */
static void opts_print(struct Options *t)
{
	s32 i;

	printf("/* Command-line: ");
	i = 0;
	loop  {
		u8 *arg;

		if (i >= t->argument_count)
			break;
		arg = t->argument_vector[i];
		/* escape arg if it contains shell metacharacters */
		if (*arg == '-') {
			putchar(*arg);
			++arg;
			if ((*arg >= 'A' && *arg <= 'Z') || (*arg >= 'a' && *arg <= 'z')) {
				putchar(*arg);
				++arg;
			} else if (*arg == '-') {
				loop {
					putchar(*arg);
					++arg;
					if (!((*arg >= 'A' && *arg <= 'Z') || (*arg >= 'a'
								&& *arg <= 'z') || *arg == '-'))
						break;
				}
				if (*arg == '=') {
					putchar(*arg);
					++arg;
				}
			}
		}
		if (strpbrk(arg, "\t\n !\"#$&'()*;<>?[\\]`{|}~") != 0) {
			if (strchr(arg, '\'') != 0) {
				putchar('"');
				loop {
					if (*arg == 0)
						break;
					if (*arg == '\"' || *arg == '\\' || *arg == '$'
										|| *arg == '`')
						putchar('\\');
					putchar(*arg);
					++arg;
				}
				putchar('"');
			} else {
				putchar('\'');
				loop {
					if (*arg == 0)
						break;
					if (*arg == '\\')
						putchar('\\');
					putchar(*arg);
					++arg;
				}
				putchar('\'');
			}
		} else
			printf("%s", arg);
		printf(" ");
		++i;
	}
	printf(" */");
}
/*}}}*/
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/globals.h"
#include "namespace/options.h"
#include "namespace/positions.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
