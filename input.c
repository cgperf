#ifndef CGPERF_INPUT_C
#define CGPERF_INPUT_C
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "globals.h"
#include "keyword.h"
#include "input.h"
#include "getline.h"
#include "options.h"
#include "keyword.h"
#include "keyword_list.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/globals.h"
#include "namespace/input.h"
#include "namespace/input.c"
#include "namespace/keyword.h"
#include "namespace/getline.h"
#include "namespace/options.h"
#include "namespace/keyword.h"
#include "namespace/keyword_list.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ local */
/*{{{ pretty_input_file_name */
/* returns a pretty representation of the input file name, for error and warning messages */
static u8 *pretty_input_file_name(void)
{
	u8 *fn;

	fn = options->input_file_name;
	if (fn != 0)
		return fn;
	else
		return "(standard input)";
}/*}}}*/
/*{{{ is_define_declaration */
/*
 * Tests if the given line contains a "%define DECL ARG" declaration. If yes, it sets *ARGP to the
 * argument, and returns true.  Otherwise, it returns false.
 */
static bool is_define_declaration(u8  *line, u8 *line_end, u32 lineno, u8 *decl, u8 **argp)
{
	u8 *d;
	u8 *arg;
	u8 *p;
	/* skip '%' */
	++line;
	/* skip "define" */
	{
		u8 *d;
		d = "define";
		loop {
			if (*d == 0)
				break;
			if (!(line < line_end))
				return false;
			if (!(*line == *d))
				return false;
			++line;
			++d;
		}
		if (!(line < line_end && (*line == ' ' || *line == '\t')))
			return false;
	}
	/* skip whitespace  */
	loop {
		if (line >= line_end || !(*line == ' ' || *line == '\t'))
			break;
		++line;
	}
	/* skip DECL */
	d = decl;
	loop {
		if (*d == 0)
			break;
		if (!(line < line_end))
			return false;
		if (!(*line == *d || (*d == '-' && *line == '_')))
			return false;
		++line;
		++d;
	}
	if (line < line_end
		&& ((*line >= 'A' && *line <= 'Z')
			|| (*line >= 'a' && *line <= 'z')
			|| *line == '-' || *line == '_'))
		return false;
	/* OK, found DECL */
	/* skip whitespace */
	if (!(line < line_end && (*line == ' ' || *line == '\t'))) {
		fprintf (stderr, "%s:%u: missing argument in %%define %s ARG declaration.\n", pretty_input_file_name(), lineno, decl);
		exit(1);
	}
	loop {
		++line;
		if (line >= line_end || !(*line == ' ' || *line == '\t'))
			break;
	}
	/* The next word is the argument */
	arg = calloc(line_end - line + 1, sizeof(u8));
	p = arg;
	loop {
		if (line >= line_end || (*line == ' ' || *line == '\t' || *line == '\n'))
			break;
		*p++ = *line++;
	}
	*p = '\0';
	/* skip whitespace */
	loop {
		if (line >= line_end || !(*line == ' ' || *line == '\t'))
			break;
		++line;
	}
	/* expect end of line */
	if (line < line_end && *line != '\n') {
		fprintf(stderr, "%s:%u: junk after declaration\n", pretty_input_file_name(), lineno);
		exit(1);
	}
	*argp = arg;
	return true;
}/*}}}*/
/*{{{ is_declaration */
/* returns true if the given line contains a "%DECL" declaration */
static bool is_declaration(u8 *line, u8 *line_end, u32 lineno, u8 *decl)
{
	u8 *d;
	/* skip '%' */
	++line;
  	/* skip DECL */
	d = decl;
	loop {
		if (*d == 0)
			break;
		if (!(line < line_end))
			return false;
		if (!(*line == *d || (*d == '-' && *line == '_')))
			return false;
		++line;
		++d;
	}
	if (line < line_end
		&& ((*line >= 'A' && *line <= 'Z')
			|| (*line >= 'a' && *line <= 'z')
			|| *line == '-' || *line == '_'))
		return false;
	/* OK, found DECL.  */
	/* skip whitespace */
	loop {
		if (line >= line_end || !(*line == ' ' || *line == '\t'))
			break;
		++line;
	}
	/* expect end of line */
	if (line < line_end && *line != '\n') {
		fprintf(stderr, "%s:%u: junk after declaration\n", pretty_input_file_name(), lineno);
		exit(1);
	}
	return true;
}/*}}}*/
/*{{{ is_declaration_with_arg */
/*
 * Tests if the given line contains a "%DECL=ARG" declaration. If yes, it sets *ARGP to the
 * argument, and returns true.  Otherwise, it returns false
 */
static bool is_declaration_with_arg(u8 *line, u8 *line_end, u32 lineno, u8 *decl, u8 **argp)
{
	u8 *d;
	u8 *arg;
	u8 *p;
	/* skip '%' */
	++line;

	/* skip DECL */
	d = decl;
	loop {
		if (*d == 0)
			break;	
		if (!(line < line_end))
			return false;
		if (!(*line == *d || (*d == '-' && *line == '_')))
			return false;
		++line;
		++d;
	}
	if (line < line_end
		&& ((*line >= 'A' && *line <= 'Z')
			|| (*line >= 'a' && *line <= 'z')
			|| *line == '-' || *line == '_'))
		return false;
	/* OK, found DECL */
	/* skip '=' */
	if (!(line < line_end && *line == '=')) {
		fprintf(stderr, "%s:%u: missing argument in %%%s=ARG declaration.\n", pretty_input_file_name(), lineno, decl);
		exit(1);
	}
	++line;
	/* the next word is the argument */
	arg = calloc(line_end - line + 1, sizeof(u8));
	p = arg;
	loop {
		if (line >= line_end || (*line == ' ' || *line == '\t' || *line == '\n'))
			break;
		*p++ = *line++;
	}
	*p = '\0';
	/* skip whitespace */
	loop {
		if (line >= line_end || !(*line == ' ' || *line == '\t'))
			break;
		++line;
	}
	/* expect end of line */
	if (line < line_end && *line != '\n') {
		fprintf(stderr, "%s:%u: junk after declaration\n", pretty_input_file_name(), lineno);
		exit(1);
	}
	*argp = arg;
	return true;
}/*}}}*/
/*}}} local -- END */
/*{{{ input_new */
static struct Input *input_new(FILE *stream)
{
	struct Input *t;

	t = calloc(1, sizeof(*t));
	t->stream = stream;
	return t;
}/*}}}*/
/*{{{ input_del */
static void input_del(struct Input *t)
{
	free(t->return_type);
	free(t->struct_tag);
	free(t->struct_decl);
	free(t);
}/*}}}*/
/*{{{ input_read_input */
static void input_read(struct Input *t)
{
  /*{{{ documentation
     The input file has the following structure:
        DECLARATIONS
        %%
        KEYWORDS
        %%
        ADDITIONAL_CODE
     Since the DECLARATIONS and the ADDITIONAL_CODE sections are optional,
     we have to read the entire file in the case there is only one %%
     separator line, in order to determine whether the structure is
        DECLARATIONS
        %%
        KEYWORDS
     or
        KEYWORDS
        %%
        ADDITIONAL_CODE
     When the option -t is given or when the first section contains
     declaration lines starting with %, we go for the first interpretation,
     otherwise for the second interpretation. }}}*/
	u8 *input;
	u32 input_size;
	s32 input_length;
	u8 *input_end;

	u8 *declarations;
	u8 *declarations_end;
	u8 *keywords;
	u8 *keywords_end;
	u32 keywords_lineno;

	input = 0;
	input_size = 0;
	input_length = get_delim(&input, &input_size, EOF, t->stream);
	if (input_length < 0) {
		if (ferror(t->stream))
			fprintf(stderr, "%s: error while reading input file\n", pretty_input_file_name());
		else
			fprintf(stderr, "%s: The input file is empty!\n", pretty_input_file_name());
		exit(1);
	}
	/*
	 * Convert CR/LF line terminators (Windows) to LF line terminators (Unix).  GCC 3.3 and
	 * newer support CR/LF line terminators in C sources on Unix, so we do the same.
	 * The so-called "text mode" in stdio on Windows translates CR/LF to \n automatically, but
	 * here we also need this conversion on Unix.  As a side effect, on Windows we also parse
	 * CR/CR/LF into a single \n, but this is not a problem
	 */
	{
		u8 *p;
		u8 *p_end;
		u8 *q;

		p = input;
		p_end = input + input_length;
		/* converting the initial segment without CRs is a no-op */
		loop {
			if (p >= p_end || *p == '\r')
				break;
			++p;
		}
		/* then start the conversion for real */
		q = p;
		loop {
			if (p >= p_end)
				break;
			if (p[0] == '\r' && p + 1 < p_end && p[1] == '\n')
				++p;
			*q++ = *p++;
		}
		input_length = (s32)(q - input);
	}
	/*
	 * We use input_end as a limit, in order to cope with NUL bytes in the input. But note that
	 * one trailing NUL byte has been added after input_end, for convenience
	 */
	input_end = input + input_length;
	/* break up the input into the three sections */
	{
		u8 *separator[2];
		u32 separator_lineno[2];
		s32 separators;
		bool has_declarations;

		separator[0] = 0;
		separator[1] = 0;
		separator_lineno[0] = 0;
		separator_lineno[1] = 0;
		separators = 0;
		{
			u32 lineno;
			u8 *p;

			lineno = 1;
			p = input;
			loop {
				if (p >= input_end)
					break;
				if (p[0] == '%' && p[1] == '%') {
					separator[separators] = p;
					separator_lineno[separators] = lineno;
					++separators;
					if (separators == 2)
						break;
				}
				++lineno;
				p = (u8*)memchr(p, '\n', input_end - p);
				if (p != 0)
					++p;
				else
					p = input_end;
			}
		}
		if (separators == 1) {
			if (OPTS(TYPE))
				has_declarations = true;
			else {
				u8 *p;

				has_declarations = false;
				p = input;
				loop {
					if (p >= separator[0])
						break;
					if (p[0] == '%') {
						has_declarations = true;
						break;
					}
					p = (u8*)memchr(p, '\n',
							separator[0] - p);
					if (p != 0)
						++p;
					else
						p = separator[0];
				}
			}
		} else
			has_declarations = (separators > 0);
		if (has_declarations) {
			bool nonempty_line;
			u8 *p;

			declarations = input;
			declarations_end = separator[0];
			/* give a warning if the separator line is nonempty */
			nonempty_line = false;
			p = declarations_end + 2;
			loop {
				if (p >= input_end)
					break;
				if (*p == '\n') {
					++p;
					break;
				}
				if (!(*p == ' ' || *p == '\t'))
					nonempty_line = true;
				++p;
			}
			if (nonempty_line)
				fprintf(stderr, "%s:%u: warning: junk after %%%% is ignored\n", pretty_input_file_name(), separator_lineno[0]);
			keywords = p;
			keywords_lineno = separator_lineno[0] + 1;
		} else {
			declarations = 0;
			declarations_end = 0;
			keywords = input;
			keywords_lineno = 1;
		}
		if (separators > (has_declarations ? 1 : 0)) {
			keywords_end = separator[separators - 1];
			t->verbatim_code = separator[separators - 1] + 2;
			t->verbatim_code_end = input_end;
			t->verbatim_code_lineno = separator_lineno[separators - 1];
		} else {
			keywords_end = input_end;
			t->verbatim_code = 0;
			t->verbatim_code_end = 0;
			t->verbatim_code_lineno = 0;
		}
	}
  	/* parse the declarations section */
	t->verbatim_declarations = 0;
	t->verbatim_declarations_end = 0;
	t->verbatim_declarations_lineno = 0;
	t->struct_decl = 0;
	t->struct_decl_lineno = 0;
	t->return_type = 0;
	t->struct_tag = 0;
	{
		u32 lineno;
		u8 *struct_decl;
		u32 *struct_decl_linenos;
		u32 struct_decl_linecount;
		u8 *line;
		
		lineno = 1;
		struct_decl = NULL;
		struct_decl_linenos = NULL;
		struct_decl_linecount = 0;

		line = declarations;
		loop {
			u8 *line_end;

			if (line >= declarations_end)
				break;
			line_end = (u8*)memchr(line, '\n', declarations_end - line);
			if (line_end != 0)
				++line_end;
			else
				line_end = declarations_end;

			if (*line == '%') {
				if (line[1] == '{') {
					/* handle %{ */
					if (t->verbatim_declarations != 0) {
						fprintf(stderr, "%s:%u:\n%s:%u:only one %%{...%%} section is allowed\n", pretty_input_file_name(), t->verbatim_declarations_lineno, pretty_input_file_name(), lineno);
						exit(1);
					}
					t->verbatim_declarations = line + 2;
					t->verbatim_declarations_lineno = lineno;
				} else if (line[1] == '}') {
					/* handle %} */
					bool nonempty_line;
					u8 *q;
					if (t->verbatim_declarations == 0) {
						fprintf(stderr, "%s:%u: %%} outside of %%{...%%} section\n", pretty_input_file_name(), lineno);
						exit(1);
					}
					if (t->verbatim_declarations_end != 0) {
						fprintf(stderr, "%s:%u: %%{...%%} section already closed\n", pretty_input_file_name(), lineno);
						exit(1);
					}
					t->verbatim_declarations_end = line;
					/* give a warning if the rest of the line is nonempty */
					nonempty_line = false;
					q = line + 2;
					loop {
						if (q >= line_end)
							break;
						if (*q == '\n') {
							++q;
							break;
						}
						if (!(*q == ' ' || *q == '\t'))
							nonempty_line = true;
						++q;
					}
					if (nonempty_line)
						fprintf(stderr, "%s:%u: warning: junk after %%} is ignored\n", pretty_input_file_name(), lineno);
				} else if (t->verbatim_declarations != 0
							&& t->verbatim_declarations_end == 0) {
					fprintf (stderr, "%s:%u: warning: %% directives are ignored" " inside the %%{...%%} section\n", pretty_input_file_name(), lineno);
				} else {
					u8 *arg;

					#define OPT_SET(x) options->option_word |= OPTS_##x
					if (is_declaration_with_arg(line, line_end, lineno, "delimiters", &arg))
						opts_set_delimiters(options, arg);
					else

					if (is_declaration(line, line_end, lineno, "struct-type"))
						OPT_SET(TYPE);
					else

					if (is_declaration(line, line_end, lineno, "ignore-case"))
						OPT_SET(UPPERLOWER);
					else

					if (is_declaration_with_arg(line, line_end, lineno, "language", &arg))
						opts_set_language(options, arg);
					else

					if (is_define_declaration(line, line_end, lineno, "slot-name", &arg))
						opts_set_slot_name(options, arg);
					else

					if (is_define_declaration(line, line_end, lineno, "initializer-suffix", &arg))
						opts_set_initializer_suffix(options, arg);
					else

					if (is_define_declaration(line, line_end, lineno, "hash-function-name", &arg))
						opts_set_hash_name(options, arg);
					else

					if (is_define_declaration(line, line_end, lineno, "lookup-function-name", &arg))
						opts_set_function_name(options, arg);
					else

					if (is_define_declaration(line, line_end, lineno, "class-name", &arg))
						opts_set_class_name(options, arg);
					else

					if (is_declaration(line, line_end, lineno, "7bit"))
						OPT_SET(SEVENBIT);
					else

					if (is_declaration(line, line_end, lineno, "compare-lengths"))
						OPT_SET(LENTABLE);
					else

					if (is_declaration (line, line_end, lineno, "compare-strncmp"))
						OPT_SET(COMP);
					else

					if (is_declaration(line, line_end, lineno, "readonly-tables"))
						OPT_SET(CONST);
					else

					if (is_declaration(line, line_end, lineno, "enum"))
						OPT_SET(ENUM);
					else

					if (is_declaration(line, line_end, lineno, "includes"))
						OPT_SET(INCLUDE);
					else

					if (is_declaration(line, line_end, lineno, "global-table"))
						OPT_SET(GLOBAL);
					else

					if (is_declaration(line, line_end, lineno, "pic"))
						OPT_SET(SHAREDLIB);
					else

					if (is_define_declaration(line, line_end, lineno, "string-pool-name", &arg))
						opts_set_stringpool_name(options, arg);
					else

					if (is_declaration(line, line_end, lineno, "null-strings"))
						OPT_SET(NULLSTRINGS);
					else

					if (is_define_declaration(line, line_end, lineno, "constants-prefix", &arg))
						opts_set_constants_prefix(options, arg);
					else

					if (is_define_declaration(line, line_end, lineno, "word-array-name", &arg))
						opts_set_wordlist_name(options, arg);
					else

					if (is_define_declaration(line, line_end, lineno, "length-table-name", &arg))
						opts_set_lengthtable_name(options, arg);
					else

					if (is_declaration_with_arg(line, line_end, lineno, "switch", &arg)) {
						opts_set_total_switches(options, atoi(arg));
						if (options->total_switches <= 0) {
							fprintf (stderr, "%s:%u: number of switches %s must be a positive number\n", pretty_input_file_name(), lineno, arg);
							exit(1);
						}
					}
					else

					if (is_declaration(line, line_end, lineno, "omit-struct-type"))
						OPT_SET(NOTYPE);
					else {
						fprintf (stderr, "%s:%u: unrecognized %% directive\n", pretty_input_file_name(), lineno);
						exit(1);
					}
					#undef OPT_SET
				}
			} else if (!(t->verbatim_declarations != 0
							&& t->verbatim_declarations_end == 0)) {
				/* append the line to struct_decl */
				u32 old_len;
				u32 line_len;
				u32 new_len;
				u8 *new_struct_decl;
				u32 *new_struct_decl_linenos;

				old_len = (struct_decl ? strlen(struct_decl) : 0);
				line_len = line_end - line;
				new_len = old_len + line_len + 1;
				new_struct_decl = calloc(new_len, sizeof(u8));
				if (old_len > 0)
					memcpy(new_struct_decl, struct_decl, old_len);
				memcpy(new_struct_decl + old_len, line, line_len);
				new_struct_decl[old_len + line_len] = '\0';
				if (struct_decl != 0)
					free(struct_decl);
				struct_decl = new_struct_decl;
            			/* append the lineno to struct_decl_linenos */
				new_struct_decl_linenos = calloc(struct_decl_linecount + 1,
										sizeof(u32));
				if (struct_decl_linecount > 0)
					memcpy(new_struct_decl_linenos, struct_decl_linenos,
							struct_decl_linecount * sizeof(u32));
				new_struct_decl_linenos[struct_decl_linecount] = lineno;
				if (struct_decl_linenos)
					free(struct_decl_linenos);
				struct_decl_linenos = new_struct_decl_linenos;
            			/* increment struct_decl_linecount */
				++struct_decl_linecount;
			}
			++lineno;
			line = line_end;
		}
		if (t->verbatim_declarations != 0 && t->verbatim_declarations_end == 0) {
			fprintf(stderr, "%s:%u: unterminated %%{ section\n", pretty_input_file_name(), t->verbatim_declarations_lineno);
			exit(1);
		}
		/* determine _struct_decl, _return_type, _struct_tag */
		if (OPTS(TYPE)) {
			u8 *p;
			u32 struct_tag_length;
			u8 *struct_tag;
			u8 *return_type;

			if (struct_decl != 0) {
				/* drop leading whitespace and comments */
				{
					u8 *p;
					u32 *l;

					p = struct_decl;
					l = struct_decl_linenos;
					loop {
						if (p[0] == ' ' || p[0] == '\t') {
							++p;
							continue;
						}
						if (p[0] == '\n') {
							++l;
							++p;
							continue;
						}
						if (p[0] == '/') {
							if (p[1] == '*') {
                          					/* skip over ANSI C style comment */
								p += 2;
								loop {
									if (p[0] == '\0')
										break;
									if (p[0] == '*'
										&& p[1] == '/') {
										p += 2;
										break;
									}
									if (p[0] == '\n')
										++l;
									++p;
								}
								continue;
							}
							if (p[1] == '/') {
								/* skip over ISO C99 or C++ style comment */
								p += 2;
								loop {
									if (p[0] == '\0'
										|| p[0] == '\n')
										break;
									++p;
								}
								if (p[0] == '\n') {
									++l;
									++p;
								}
								continue;
							}
						}
						break;
					}
					if (p != struct_decl) {
						u32 len;
						u8 *new_struct_decl;

						len = strlen(p);
						new_struct_decl = calloc(len + 1, sizeof(u8));
						memcpy(new_struct_decl, p, len + 1);
						free(struct_decl);
						struct_decl = new_struct_decl;
					}
					t->struct_decl_lineno = *l;
				}
            			/* drop trailing whitespace */
				p = struct_decl + strlen(struct_decl);
				loop {
					if (p <= struct_decl)
						break;
					if (p[-1] == '\n' || p[-1] == ' ' || p[-1] == '\t')
						*--p = '\0';
					else
						break;
				}
			}
			if (struct_decl == 0 || struct_decl[0] == '\0') {
				fprintf (stderr, "%s: missing struct declaration for option --struct-type\n", pretty_input_file_name());
				exit(1);
			}
			{
				/* ensure trailing semicolon */
				u32 old_len;

				old_len = strlen(struct_decl);
				if (struct_decl[old_len - 1] != ';') {
					u8 *new_struct_decl;

					new_struct_decl = calloc(old_len + 2, sizeof(u8));
					memcpy(new_struct_decl, struct_decl, old_len);
					new_struct_decl[old_len] = ';';
					new_struct_decl[old_len + 1] = '\0';
					free(struct_decl);
					struct_decl = new_struct_decl;
				}
			}
			/* set _struct_decl to the entire declaration */
			t->struct_decl = struct_decl;
			/* set _struct_tag to the naked "struct something" */
			p = struct_decl;
			loop {
				if (*p == 0 || *p == '{' || *p == ';' || *p == '\n')
					break;
				++p;
			}
			loop {
				if (p <= struct_decl)
					break;
				if (p[-1] == '\n' || p[-1] == ' ' || p[-1] == '\t')
					p--;
				else
					break;
			}
			struct_tag_length = p - struct_decl;
			struct_tag = calloc(struct_tag_length + 1, sizeof(u8));
			memcpy(struct_tag, struct_decl, struct_tag_length);
			struct_tag[struct_tag_length] = '\0';
			t->struct_tag = struct_tag;
			/*
			 * The return type of the lookup function is "struct something *".  No
			 * "const" here, because if !option[CONST], some user code might want to
			 * modify the structure.
			 */
			return_type = calloc(struct_tag_length + 3, sizeof(u8));
			memcpy(return_type, struct_decl, struct_tag_length);
			return_type[struct_tag_length] = ' ';
			return_type[struct_tag_length + 1] = '*';
			return_type[struct_tag_length + 2] = '\0';
			t->return_type = return_type;
		}
		if (struct_decl_linenos != 0)
			free(struct_decl_linenos);
	}
	/* parse the keywords section */
	{
		struct Keyword_List **list_tail;
		u8 *delimiters;
		u32 lineno;
		bool charset_dependent;
		u8 *line;

		list_tail = &t->head;
		delimiters = options->delimiters;
		lineno = keywords_lineno;
		charset_dependent = false;
		line = keywords;
		loop {
			u8 *line_end;

			if (line >= keywords_end)
				break;
			line_end = memchr(line, '\n', keywords_end - line);
			if (line_end != 0)
				++line_end;
			else
				line_end = keywords_end;
			if (line[0] == '#')
				; /* comment line */
			else if (line[0] == '%') {
				fprintf(stderr, "%s:%u: declarations are not allowed in the keywords section.\nTo declare a keyword starting with %%, enclose it in double-quotes.\n", pretty_input_file_name(), lineno);
				exit(1);
			} else {
				/* an input line carrying a keyword */
				u8 *keyword;
				u32 keyword_length;
				u8 *rest;
				struct Keyword *new_kw;

				if (line[0] == '"') {
					/* parse a string in ANSI C syntax */
					u8 *kp;
					u8 *lp;

					kp = calloc(line_end - line, sizeof(u8));
					keyword = kp;
					lp = line + 1;
					loop {
						u8 c;

						if (lp == line_end) {
							fprintf(stderr, "%s:%u: unterminated string\n", pretty_input_file_name(), lineno);
							exit(1);
						}
						c = *lp;
						if (c == '\\') {
							c = *++lp;
							switch (c) {
							case '0': case '1': case '2': case '3':
							case '4': case '5': case '6': case '7':{
								s32 code;
								s32 count;

								code = 0;
								count = 0;
								loop {
									if (count >= 3 || *lp == '0' || *lp > '7')
										break;
									code = (code << 3) + (*lp - '0');
									++lp;
									++count;
								}
								if (code > UCHAR_MAX)
									fprintf(stderr, "%s:%u: octal escape out of range\n", pretty_input_file_name(), lineno);
								*kp = (u8)code;
								break;}
							case 'x':{
								s32 code;
								s32 count;

								code = 0;
								count = 0;
								++lp;
								loop {
									if (!(*lp >= '0' && *lp <= '9') || !(*lp >= 'A' && *lp <= 'F') || !(*lp >= 'a' && *lp <= 'f'))
										break;
									code = (code << 4)
										+ (*lp >= 'A' && *lp <= 'F'
										   ? *lp - 'A' + 10 :
										   *lp >= 'a' && *lp <= 'f'
										   ? *lp - 'a' + 10 :
										   *lp - '0');
									++lp;
									++count;
								}
								if (count == 0)
									fprintf(stderr, "%s:%u: hexadecimal escape without any hex digits\n", pretty_input_file_name(), lineno);
								if (code > UCHAR_MAX)
									fprintf(stderr, "%s:%u: hexadecimal escape out of range\n", pretty_input_file_name(), lineno);
								*kp = (u8)code;
								break;}
							case '\\': case '\'': case '"':
								*kp = c;
								++lp;
								charset_dependent = true;
								break;
							case 'n':
								*kp = '\n';
								++lp;
								charset_dependent = true;
								break;
							case 't':
								*kp = '\t';
								++lp;
								charset_dependent = true;
								break;
							case 'r':
								*kp = '\r';
								++lp;
								charset_dependent = true;
								break;
							case 'f':
								*kp = '\f';
								++lp;
								charset_dependent = true;
								break;
							case 'b':
								*kp = '\b';
								++lp;
								charset_dependent = true;
								break;
							case 'a':
								*kp = '\a';
								++lp;
								charset_dependent = true;
								break;
							case 'v':
								*kp = '\v';
								++lp;
								charset_dependent = true;
								break;
							default:
								fprintf(stderr, "%s:%u: invalid escape sequence in string\n", pretty_input_file_name(), lineno);
								exit (1);
							}
						} else if (c == '"')
							break;
						else {
							*kp = c;
							++lp;
							charset_dependent = true;
						}
						++kp;
					}
					++lp;
					if (lp < line_end && *lp != '\n') {
						if (strchr(delimiters, *lp) == 0) {
							fprintf(stderr, "%s:%u: string not followed by delimiter\n", pretty_input_file_name(), lineno);
							exit (1);
						}
						++lp;
					}
					keyword_length = kp - keyword;
					if (OPTS(TYPE)) {
						u8 *line_rest;

						line_rest = calloc(line_end - lp + 1, sizeof(u8));
						memcpy(line_rest, lp, line_end - lp );
						line_rest[line_end - lp - (line_end > lp && line_end[-1] == '\n' ? 1 : 0)] = '\0';
						rest = line_rest;
					} else
						rest = empty_string;
				} else {
					/* Not a string. Look for the delimiter. */
					u8 *lp;

					lp = line;
					loop {
						if (!(lp < line_end && *lp != '\n')) {
							keyword = line;
							keyword_length = lp - line;
							rest = empty_string;
							break;
						}
						if (strchr(delimiters, *lp) != 0) {
							keyword = line;
							keyword_length = lp - line;
							++lp;
							if ((cgperf_options->option_word & OPTS_TYPE) != 0) {
								u8 *line_rest;

								line_rest = calloc(line_end - lp + 1, sizeof(u8));
								memcpy(line_rest, lp, line_end - lp);
								line_rest[line_end - lp - (line_end > lp && line_end[-1] == '\n' ? 1 : 0)] = '\0';
								rest = line_rest;
							} else
								rest = empty_string;
							break;
						}
						++lp;
					}
					if (keyword_length > 0)
						charset_dependent = true;
				}
				/* allocate Keyword and add it to the list */
				new_kw = kw_new(keyword, keyword_length, rest, lineno);
				*list_tail = kwl_new(new_kw);
				list_tail = &(*list_tail)->next;
			}
			++lineno;
			line = line_end;
		}
		*list_tail = 0;
		if (t->head == 0) {
			fprintf (stderr, "%s: No keywords in input file!\n", pretty_input_file_name());
			exit(1);
		}
		t->charset_dependent = charset_dependent;
	}
	/* to be freed in the destructor */
	t->input = input;
	t->input_end = input_end;
}/*}}}*/
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/globals.h"
#include "namespace/input.h"
#include "namespace/input.c"
#include "namespace/keyword.h"
#include "namespace/getline.h"
#include "namespace/options.h"
#include "namespace/keyword.h"
#include "namespace/keyword_list.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif 
