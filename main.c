#ifndef CGPERF_MAIN_C
#define CGPERF_MAIN_C
#include <stdlib.h>
#include <string.h>
#include "globals.h"
#include "options.h"
#include "keyword.h"
#include "keyword_list.h"
#include "input.h"
#include "search.h"
#include "output.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/globals.h"
#include "namespace/options.h"
#include "namespace/keyword.h"
#include "namespace/keyword_list.h"
#include "namespace/input.h"
#include "namespace/search.h"
#include "namespace/output.h"
/*------------------------------------------------------------------------------------------------*/
static void cgperf_init_once(void)
{
	options = opts_new();
}
static void cgperf_main_deinit_once(void)
{
	opts_del(options);
}
int main(int argc, char **argv)
{
	int exitcode;

	cgperf_init_once();
	/* Set the Options. Open the input file and assign stdin to it. */
	opts_parse_options(options, (u32)argc, (u8**)argv);

	/* open the input file  */
	if (options->input_file_name != 0)
		if (!freopen(options->input_file_name, "r", stdin)) {
			fprintf(stderr, "Cannot open input file '%s'\n", options->input_file_name);
			exit(1);
		}
	{
		/* initialize the keyword list */
		struct Input *inputter;
		struct Keyword_List *list;

		inputter = input_new(stdin);
		input_read(inputter);
		/*
		 * we can cast the keyword list to KeywordExt_List* because its list elements were
		 * created by KeywordExt_Factory
		 */
		list = inputter->head;
		{
			struct Search *searcher;

			searcher = schr_new(list);
			schr_optimize(searcher);
			list = searcher->head;
			/* open the output file */
			if (options->output_file_name != 0)
				if (strcmp(options->output_file_name, "-") != 0)
					if (freopen(options->output_file_name, "w", stdout) == 0) {
						fprintf(stderr, "Cannot open output file '%s'\n", options->output_file_name);
						exit(1);
					}
			{
				/* output the hash function code */
				struct Output *outputter;
				outputter = output_new(
						searcher->head,
						inputter->struct_decl,
						inputter->struct_decl_lineno,
						inputter->return_type,
						inputter->struct_tag,
						inputter->verbatim_declarations,
						inputter->verbatim_declarations_end,
						inputter->verbatim_declarations_lineno,
						inputter->verbatim_code,
						inputter->verbatim_code_end,
						inputter->verbatim_code_lineno,
						inputter->charset_dependent,
						searcher->total_keys,
						searcher->max_key_len,
						searcher->min_key_len,
						searcher->hash_includes_len,
						searcher->key_positions,
						searcher->alpha_inc,
						searcher->total_duplicates,
						searcher->alpha_size,
						searcher->asso_values);
				output_do(outputter);
				exitcode = 0;
				if (fflush (stdout) || ferror (stdout)) {
					fprintf(stderr, "error while writing output file\n");
					exitcode = 1;
				}
				/* here we run the Output destructor */
				output_del(outputter);
			}
			schr_del(searcher);
		}
		//TODO
		input_del(inputter);
	}
	/* don't use exit() here, it skips the destructors */
	cgperf_main_deinit_once();
	return exitcode;
}
/*----------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/globals.h"
#include "namespace/options.h"
#include "namespace/keyword.h"
#include "namespace/keyword_list.h"
#include "namespace/input.h"
#include "namespace/search.h"
#undef EPILOG
/*----------------------------------------------------------------------------*/
#endif
