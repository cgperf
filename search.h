#ifndef CGPERF_SEARCH_H
#define CGPERF_SEARCH_H
#include <stdbool.h>
#include "c_fixing.h"
#include "keyword_list.h"
#include "positions.h"
#include "bool-array.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/search.h"
#include "namespace/keyword.h"
#include "namespace/keyword_list.h"
#include "namespace/positions.h"
#include "namespace/bool-array.h"
/* for the Search local types */
#include "namespace/search.c"
/*---------------------------i--------------------------------------------------------------------*/
/*{{{ types */
struct Search {
/*{{{ public */
	struct Keyword_List *head;
	/* total number of keywords, counting duplicates */
	s32 total_keys;
	/* maximum length of the longest keyword */
	s32 max_key_len;
	/* minimum length of the shortest keyword */
	s32 min_key_len;
	/* whether the hash function includes the length */
	bool hash_includes_len;
	/* user-specified or computed key positions */
	struct Positions *key_positions;
	/* adjustments to add to bytes add specific key positions */
	u32 *alpha_inc;
	/* size of alphabet */
	u32 alpha_size;
	/*
	 * alphabet character unification, either the identity or a mapping from upper case
	 * characters to lower case characters (and maybe more)
	 */
	u32 *alpha_unify;
	/* maximum _selchars_length over all keywords */
	u32 max_selchars_length;
	/*
	 * total number of duplicates that have been moved to _duplicate_link lists (not counting
	 * their representatives which stay on the main list)
	 */
	s32 total_duplicates;
	/*
	 * Counts occurrences of each key set character.  occurrences[c] is the number of times that
	 * c occurs among the _selchars of a keyword.
	 */
	s32 *occurrences;
	/* value associated with each character */
	s32 *asso_values;
/*}}} public -- END */
/*{{{ private */
	/* maximal possible hash value */
	s32 max_hash_value;
	/* exclusive upper bound for every _asso_values[c]. A power of 2. */
	u32 asso_value_max;
	/* Initial value for asso_values table. -1 means random. */
	s32 initial_asso_value;
	/* Jump length when trying alternative values. 0 means random. */
	s32 jump;
	/* Length of _head list. Number of keywords, not counting duplicates. */
	s32 list_len;
	/* sparse bit vector for collision detection */
	struct Bool_Array *collision_detector;
/*}}} private -- END */
};
/*}}} types -- END */
/*{{{ public static methods */
static struct Search *schr_new(struct Keyword_List *list);
static void schr_del(struct Search *t);
static void schr_optimize(struct Search *t);
/*}}} public static methods -- END */
/*{{{ private static methods */
static void schr_prepare(struct Search *t);
static void schr_find_good_asso_values(struct Search *t);
static u32 schr_count_duplicates_tuple(struct Search *t);
static u32 schr_count_duplicates_tuple_do(struct Search *t, struct Positions *positions,
										u32 *alpha_unify);
static void schr_find_positions(struct Search *t);
static void schr_find_alpha_inc(struct Search *t);
static u32 *schr_compute_alpha_unify(struct Search *t);
static u32 *schr_compute_alpha_unify_with_inc(struct Search *t, struct Positions *positions,
										u32 *alpha_inc);
static u32 schr_compute_alpha_size(void);
static u32 schr_compute_alpha_size_with_inc(struct Search *t, u32 *alpha_inc);
static void schr_init_selchars_tuple(struct Search *t, struct Positions *positions,
										u32 *alpha_unify);
static void schr_delete_selchars(struct Search *t);
static u32 schr_count_duplicates_multiset(struct Search *t, u32 *alpha_inc);
static void schr_init_selchars_multiset(struct Search *t, struct Positions *positions,
								u32 *alpha_unify, u32 *alpha_inc);
static void schr_prepare_asso_values(struct Search *t);
static void schr_find_asso_values(struct Search *t);
struct EquivalenceClass;
static struct EquivalenceClass *schr_compute_partition(struct Search *t, bool *undetermined);
static u32 schr_count_possible_collisions(struct Search *t, struct EquivalenceClass *partition,
											u32 c);
static bool schr_unchanged_partition(struct Search *t, struct EquivalenceClass *partition, u32 c);
static s32 schr_compute_hash(struct Search *t, struct Keyword *kw);
static void schr_sort(struct Search *t);
/*}}} private static methods -- END */
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/search.h"
#include "namespace/keyword.h"
#include "namespace/keyword_list.h"
#include "namespace/positions.h"
#include "namespace/bool-array.h"
/* for the Search local types */
#include "namespace/search.c"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
