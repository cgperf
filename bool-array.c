#ifndef CGPERF_BOOL_ARRAY_C
#define CGPERF_BOOL_ARRAY_C
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "c_fixing.h"
#include "globals.h"
#include "options.h"
#include "bool-array.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/globals.h"
#include "namespace/options.h"
#include "namespace/bool-array.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ ba_new */
static struct Bool_Array *ba_new(u32 size)
{
	struct Bool_Array *t;

	t = calloc(1, sizeof(*t));
	t->size = size;
	t->iteration_number = 1;
	t->storage_array = calloc(size, sizeof(*(t->storage_array)));
	if (OPTS(DEBUG))
		fprintf (stderr, "\nbool array size = %d, total bytes = %d\n", t->size, (u32)(t->size * sizeof(t->storage_array[0])));
	return t;
}/*}}}*/
/*{{{ ba_del */
static void ba_del(struct Bool_Array *t)
{
	if (OPTS(DEBUG))
    		fprintf(stderr, "\ndumping boolean array information\nsize = %d\niteration number = %d\nend of array dump\n", t->size, t->iteration_number);
	free(t->storage_array);
	free(t);
}/*}}}*/
/*{{{ ba_clear */
/* resets all bits to zero */
static void ba_clear(struct Bool_Array *t)
{
	/*
	 * If we wrap around it's time to zero things out again!  However, this only occurs once
	 * about every 2^32 iterations, so it will not happen more frequently than once per second.
	 */
	++(t->iteration_number);
	if (t->iteration_number == 0) {
		t->iteration_number = 1;
		memset(t->storage_array, 0, t->size * sizeof(*(t->storage_array)));
		if (OPTS(DEBUG)) {
			fprintf(stderr, "(re-initialized bool_array)\n");
			fflush(stderr);
		}
	}
}/*}}}*/
/*{{{ ba_set_bit */
/*
 * Sets the specified bit to true.
 * Returns its previous value (false or true).
 */
static bool ba_set_bit(struct Bool_Array *t, u32 index)
{
	if (t->storage_array[index] == t->iteration_number)
		/* the bit was set since the last clear() call */
		return true;
	else {
		/* the last operation on this bit was clear(). Set it now. */
		t->storage_array[index] = t->iteration_number;
		return false;
	}
}/*}}}*/
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/globals.h"
#include "namespace/options.h"
#include "namespace/bool-array.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
