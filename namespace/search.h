#ifndef EPILOG
#define schr_compute_alpha_size 		cgperf_Search_compute_alpha_size
#define schr_compute_alpha_size_with_inc 	cgperf_Search_compute_alpha_size_with_inc
#define schr_compute_alpha_unify 		cgperf_Search_compute_alpha_unify
#define schr_compute_alpha_unify_with_inc 	cgperf_Search_compute_alpha_unify_with_inc
#define schr_compute_hash			cgperf_Search_compute_hash
#define schr_compute_partition			cgperf_Search_compute_partition
#define schr_count_duplicates_multiset		cgperf_Search_count_duplicates_multiset
#define schr_count_duplicates_tuple		cgperf_Search_count_duplicates_tuple
#define schr_count_duplicates_tuple_do		cgperf_Search_count_duplicates_tuple_do
#define schr_count_possible_collisions		cgperf_Search_count_possible_collisions
#define schr_del				cgperf_Search_del
#define schr_delete_partition			cgperf_Search_delete_partition
#define schr_delete_selchars			cgperf_Search_delete_selchars
#define schr_find_alpha_inc			cgperf_Search_find_alpha_inc
#define schr_find_asso_values			cgperf_Serach_find_asso_values
#define schr_find_good_asso_values		cgperf_Search_find_good_asso_values
#define schr_find_positions			cgperf_Search_find_positions
#define schr_init_selchars_multiset		cgperf_Search_init_selchars_multiset
#define schr_init_selchars_tuple		cgperf_Search_init_selchars_tuple
#define schr_new				cgperf_Search_new
#define schr_optimize				cgperf_Search_optimize
#define schr_prepare				cgperf_Search_prepare
#define schr_prepare_asso_values		cgperf_Search_prepare_asso_values
#define schr_sort				cgperf_Search_sort
#define schr_unchanged_partition		cgperf_Search_unchanged_partition
#define Search				cgperf_Search
/*############################################################################*/
#else /* EPILOG */
#undef schr_compute_alpha_size
#undef schr_compute_alpha_size_with_inc
#undef schr_compute_alpha_unify
#undef schr_compute_alpha_unify_with_inc
#undef schr_compute_hash
#undef schr_compute_partition
#undef schr_count_duplicates_multiset
#undef schr_count_duplicates_tuple
#undef schr_count_duplicates_tuple_do
#undef schr_count_possible_collisions
#undef schr_del
#undef schr_delete_partition
#undef schr_delete_selchars
#undef schr_find_alpha_inc
#undef schr_find_asso_values
#undef schr_find_good_asso_values
#undef schr_find_positions
#undef schr_init_selchars_multiset
#undef schr_init_selchars_tuple
#undef schr_new
#undef schr_optimize
#undef schr_prepare
#undef schr_prepare_asso_values
#undef schr_sort
#undef schr_unchanged_partition
#undef Search
#endif
