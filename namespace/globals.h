#ifndef EPILOG
#define OPTS(x) ((cgperf_options->option_word & cgperf_OPTIONS_##x) != 0)
/*----------------------------------------------------------------------------*/
#define options cgperf_options
/*############################################################################*/
#else /* EPILOG */
#undef OPTS
/*----------------------------------------------------------------------------*/
#undef options
#endif
