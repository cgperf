#ifndef EPILOG
#define DEFAULT_CLASS_NAME			cgperf_OPTIONS_DEFAULT_CLASS_NAME
#define DEFAULT_CONSTANTS_PREFIX		cgperf_OPTIONS_DEFAULT_CONSTANTS_PREFIX
#define DEFAULT_DELIMITERS			cgperf_OPTIONS_DEFAULT_DELIMITERS
#define DEFAULT_FUNCTION_NAME			cgperf_OPTIONS_DEFAULT_FUNCTION_NAME
#define DEFAULT_HASH_NAME			cgperf_OPTIONS_DEFAULT_HASH_NAME
#define DEFAULT_INITIALIZER_SUFFIX		cgperf_OPTIONS_DEFAULT_INITIALIZER_SUFFIX
#define DEFAULT_LENGTHTABLE_NAME		cgperf_OPTIONS_DEFAULT_LENGTHTABLE_NAME
#define DEFAULT_SLOT_NAME			cgperf_OPTIONS_DEFAULT_SLOT_NAME
#define DEFAULT_STRINGPOOL_NAME			cgperf_OPTIONS_DEFAULT_STRINGPOOL_NAME
#define DEFAULT_WORDLIST_NAME			cgperf_OPTIONS_DEFAULT_WORDLIST_NAME
#define Options					cgperf_Options
#define opts_del				cgperf_Options_del
#define opts_long_options			cgperf_Options_long_options
#define opts_long_usage				cgperf_Options_long_usage
#define opts_new				cgperf_Options_new
#define opts_parse_options			cgperf_Options_parse_options
#define opts_print				cgperf_Options_print
#define opts_program_name			cgperf_Options_program_name
#define opts_set_class_name			cgperf_Options_set_class_name
#define opts_set_constants_prefix		cgperf_Options_set_constants_prefix
#define opts_set_delimiters			cgperf_Options_set_delimiters
#define opts_set_function_name			cgperf_Options_set_function_name
#define opts_set_hash_name			cgperf_Options_set_hash_name
#define opts_set_initializer_suffix		cgperf_Options_set_initializer_suffix
#define opts_set_language			cgperf_Options_set_language
#define opts_set_lengthtable_name		cgperf_Options_set_lengthtable_name
#define opts_set_slot_name			cgperf_Options_set_slot_name
#define opts_set_stringpool_name		cgperf_Options_set_stringpool_name
#define opts_set_total_switches			cgperf_Options_set_total_switches
#define opts_set_wordlist_name			cgperf_Options_set_wordlist_name
#define opts_short_usage			cgperf_Options_short_usage
#define OPTS_ANSIC				cgperf_OPTIONS_ANSIC
#define OPTS_C					cgperf_OPTIONS_C
#define OPTS_COMP				cgperf_OPTIONS_COMP
#define OPTS_CONST				cgperf_OPTIONS_CONST
#define OPTS_CPLUSPLUS				cgperf_OPTIONS_CPLUSPLUS
#define OPTS_DEBUG				cgperf_OPTIONS_DEBUG
#define OPTS_DEFAULT_JUMP_VALUE			cgperf_OPTIONS_DEFAULT_JUMP_VALUE
#define OPTS_DUP				cgperf_OPTIONS_DUP
#define OPTS_ENUM				cgperf_OPTIONS_ENUM
#define OPTS_GLOBAL				cgperf_OPTIONS_GLOBAL
#define OPTS_INCLUDE				cgperf_OPTIONS_INCLUDE
#define OPTS_KRC				cgperf_OPTIONS_KRC
#define OPTS_LENTABLE				cgperf_OPTIONS_LENTABLE
#define OPTS_NOLENGTH				cgperf_OPTIONS_NOLENGTH
#define OPTS_NOTYPE				cgperf_OPTIONS_NOTYPE
#define OPTS_NULLSTRINGS			cgperf_OPTIONS_NULLSTRINGS
#define OPTS_POSITIONS				cgperf_OPTIONS_POSITIONS
#define OPTS_RANDOM				cgperf_OPTIONS_RANDOM
#define OPTS_SEVENBIT				cgperf_OPTIONS_SEVENBIT
#define OPTS_SHAREDLIB				cgperf_OPTIONS_SHAREDLIB
#define OPTS_SWITCH				cgperf_OPTIONS_SWITCH
#define OPTS_TYPE				cgperf_OPTIONS_TYPE
#define OPTS_UPPERLOWER				cgperf_OPTIONS_UPPERLOWER
#define posstrp_del				cgperf_PositionsStringParser_del
#define posstrp_new				cgperf_PositionsStringParser_new
#define posstrp_nextPosition			cgperf_PositionsStringParser_nextPosition
/*############################################################################*/
#else /* EPILOG */
#undef DEFAULT_CLASS_NAME
#undef DEFAULT_CONSTANTS_PREFIX
#undef DEFAULT_DELIMITERS
#undef DEFAULT_FUNCTION_NAME
#undef DEFAULT_HASH_NAME
#undef DEFAULT_INITIALIZER_SUFFIX
#undef DEFAULT_LENGTHTABLE_NAME
#undef DEFAULT_SLOT_NAME
#undef DEFAULT_STRINGPOOL_NAME
#undef DEFAULT_WORDLIST_NAME
#undef Options
#undef opts_del
#undef opts_long_options
#undef opts_long_usage
#undef opts_new
#undef opts_parse_options
#undef opts_print
#undef opts_program_name
#undef opts_set_class_name
#undef opts_set_constants_prefix
#undef opts_set_delimiters
#undef opts_set_function_name
#undef opts_set_hash_name
#undef opts_set_initializer_suffix
#undef opts_set_language
#undef opts_set_lengthtable_name
#undef opts_set_slot_name
#undef opts_set_stringpool_name
#undef opts_set_total_switches
#undef opts_set_wordlist_name
#undef opts_short_usage
#undef OPTIONS_ANSIC
#undef OPTIONS_C
#undef OPTIONS_COMP
#undef OPTIONS_CONST
#undef OPTIONS_CPLUSPLUS
#undef OPTIONS_DEBUG
#undef OPTIONS_DEFAULT_JUMP_VALUE
#undef OPTIONS_DUP
#undef OPTIONS_ENUM
#undef OPTIONS_GLOBAL
#undef OPTIONS_INCLUDE
#undef OPTIONS_KRC
#undef OPTIONS_LENTABLE
#undef OPTIONS_NOLENGTH
#undef OPTIONS_NOTYPE
#undef OPTIONS_NULLSTRINGS
#undef OPTIONS_POSITIONS
#undef OPTIONS_RANDOM
#undef OPTIONS_SEVENBIT
#undef OPTIONS_SHAREDLIB
#undef OPTIONS_SWITCH
#undef OPTIONS_TYPE
#undef OPTS_UPPERLOWER
#undef posstrp_del
#undef posstrp_new
#undef posstrp_nextPosition
#endif
