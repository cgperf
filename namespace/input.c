#ifndef EPILOG
#define is_declaration		cgperf_is_declaration
#define is_declaration_with_arg	cgperf_is_declaration_with_arg
#define is_define_declaration	cgperf_is_define_declaration
#define pretty_input_file_name	cgperf_pretty_input_file_name
#else /* EPILOG */
#undef is_declaration
#undef is_declaration_with_arg
#undef is_define_declaration
#undef pretty_input_file_name
#endif

