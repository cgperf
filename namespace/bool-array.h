#ifndef EPILOG
#define ba_clear	cgperf_Bool_Array_clean
#define ba_del		cgperf_Bool_Array_del
#define ba_new		cgperf_Bool_Array_new
#define ba_set_bit	cgperf_Bool_Array_set_bit
#define Bool_Array	cgperf_Bool_Array
/*############################################################################*/
#else /* EPILOG */
#undef ba_clear
#undef ba_del
#undef ba_new
#undef ba_set_bit
#undef Bool_Array
#endif
