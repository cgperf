#ifndef EPILOG
#define empty_string				cgperf_empty_string
#define Keyword					cgperf_Keyword
#define kw_delete_selchars			cgperf_Keyword_delete_selchars
#define kw_init_selchars_low			cgperf_Keyword_init_selchars_low
#define kw_init_selchars_multiset		cgperf_Keyword_init_selchars_multiset
#define kw_init_selchars_tuple			cgperf_Keyword_init_selchars_tuple
#define kw_new					cgperf_Keyword_new
#define sort_char_set				cgperf_sort_char_set
/*############################################################################*/
#else /* EPILOG */
#undef empty_string
#undef Keyword
#undef kw_delete_selchars
#undef kw_init_selchars_low
#undef kw_init_selchars_multiset
#undef kw_init_selchars_tuple
#undef kw_new
#undef sort_char_set
#endif
