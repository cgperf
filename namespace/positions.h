#ifndef EPILOG
#define pos_add				cgperf_Positions_add
#define pos_contains			cgperf_Positions_contains
#define pos_cpy				cgperf_Positions_cpy
#define pos_del				cgperf_Positions_del
#define pos_iterator			cgperf_Positions_iterator
#define pos_iterator_all		cgperf_Positions_iterator_all
#define POS_LASTCHAR			cgperf_POSITIONS_LASTCHAR
#define POS_MAX_KEY_POS			cgperf_POSITIONS_MAX_KEY_POS
#define POS_MAX_SIZE			cgpeff_POSITIONS_MAX_SIZE
#define pos_new				cgperf_Positions_new
#define pos_new_cpy			cgperf_Positions_new_cpy
#define pos_print			cgperf_Positions_print
#define pos_remove			cgperf_Positions_remove
#define pos_reviterator			cgperf_Positions_reviterator
#define pos_set_useall			cgperf_Positions_set_useall
#define pos_sort			cgperf_Positions_sort
#define positer_del			cgperf_PositionsIterator_del
#define POSITER_EOS			cgperf_POSITIONITERATOR_EOS
#define positer_new			cgperf_PositionIterator_new
#define positer_new_all			cgperf_PositionIterator_new_all
#define positer_next			cgperf_PositionIterator_next
#define positer_remaining		cgperf_PositionIterator_remaining
#define POSREVIT_EOS			cgperf_POSITIONREVERSEITERATOR_EOS
#define Positions			cgperf_Positions
#define PositionIterator		cgperf_PositionIterator
#define PositionReverseIterator		cgperf_PositionReverseIterator
#define posrevit_del			cgperf_PositionReverseIterator_del
#define POSREVIT_EOS			cgperf_POSITIONREVERSEITERATOR_EOS
#define posrevit_new			cgperf_PositionReverseIterator_new
#define posrevit_next			cgperf_PositionReverseIterator_next
/*############################################################################*/
#else /* EPILOG */
#undef pos_add
#undef pos_contains
#undef pos_cpy
#undef pos_del
#undef pos_iterator
#undef pos_iterator_all
#undef POS_LASTCHAR
#undef POS_MAX_KEY_POS
#undef POS_MAX_SIZE
#undef pos_new
#undef pos_new_cpy
#undef pos_print
#undef pos_remove
#undef pos_reviterator
#undef pos_set_useall
#undef pos_sort
#undef positer_del
#undef POSITER_EOS
#undef positer_new
#undef positer_new_all
#undef positer_next
#undef positer_remaining
#undef Positions
#undef PositionIterator
#undef posrevit_del
#undef POSREVIT_EOS
#undef posrevit_new
#undef posrevit_next
#endif
