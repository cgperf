#ifndef EPILOG
#define input_del		cgperf_Input_del
#define input_new		cgperf_Input_new
#define input_read		cgperf_Input_read
#define Input   		cgperf_Input
#else /* EPILOG */
#undef input_del
#undef input_new
#undef input_read
#undef Input
#endif
