#ifndef EPILOG
/* global util */
#define copy_list		cgperf_Keyword_List_copy_list
#define delete_list		cgperf_Keyword_List_delete_list
#define merge			cgperf_Keyword_List_merge
#define mergesort_list		cgperf_Keyword_List_mergesort_list
/* global util -- END */
#define Keyword_List		cgperf_Keyword_List
#define kwl_del			cgperf_Keyword_List_del
#define kwl_new			cgperf_Keyword_List_new
/*############################################################################*/
#else /* EPILOG */
/* global util */
#undef copy_list
#undef delete_list
#undef merge
#undef mergesort_list
/* global util -- END */
#undef Keyword_List
#undef kwl_del
#undef kwl_new
#endif
