#ifndef EPILOG
#define Hash_Table	cgperf_Hash_Table
#define ht_del		cgperf_Hash_Table_del
#define ht_dump		cgperf_Hash_Table_dump
#define ht_equal	cgperf_Hash_Table_equal
#define ht_insert	cgperf_Hash_Table_insert
#define ht_new		cgperf_Hash_Table_new
#define ht_size_factor	cgperf_Hash_Table_size_factor
/*############################################################################*/
#else /* EPILOG */
#undef Hash_Table
#undef ht_new
#undef ht_del
#undef ht_dump
#undef ht_equal
#undef ht_insert
#undef ht_size_factor
#endif
