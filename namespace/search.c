#ifndef EPILOG
#define delete_partition	cgperf_Search_delete_partition
#define equals			cgperf_Search_equals
#define EquivalenceClass	cgperf_Search_EquivalenceClass
#define less_by_hash_value	cgperf_Search_less_by_hash_value
#define Step			cgperf_Search_Step
/*############################################################################*/
#else /* EPILOG */
#undef delete_partition
#undef equals
#undef EquivalenceClass
#undef less_by_hash_value
#undef Step
#endif
