#ifndef GPERF_KEYWORD_C
#define GPERF_KEYWORD_C
#include "c_fixing.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/keyword.h"
#include "namespace/positions.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ global */
static u8 *empty_string = "";
/*}}} globals -- END */
/*------------------------------------------------------------------------------------------------*/
/*{{{ local */
static void sort_char_set(u32 *base, s32 len);
/*}}} local -- END */
/*------------------------------------------------------------------------------------------------*/
/*{{{ Keyword class */
/*{{{ constants and types */
struct Keyword {
	/*----------------------------------------------------------------------------------------*/
	/* data members defined immediately by the input file */
	/* the keyword as a string, possibly containing NUL bytes */
	u8 *allchars;
	s32 allchars_length;
	/* additional stuff seen on the same line of the input file */
	u8 *rest;
	/* line number of this keyword in the input file */
	u32 lineno;
	/*----------------------------------------------------------------------------------------*/
	/* Ext: data members depending on the keyposition list */
	/*
	 * the selected characters that participate for the hash function,
	 * selected according to the keyposition list, as a canonically
	 * reordered multiset
	 */
	u32 *selchars;
	s32 selchars_length;
	/*
	 * Chained list of keywords having the same _selchars and
	 *  - if !option[NOLENGTH] - also the same _allchars_length.
	 * Note that these duplicates are not members of the main keyword list
	 */
	struct Keyword *duplicate_link;
	/* data members used by the algorithm */
	s32 hash_value; /* hash value for the keyword */
	/* data members used by the output routines */
	s32 final_index;
};
/*}}} constants and types -- END */
/*{{{ private static methods */
static u32 *kw_init_selchars_low(struct Keyword *t, struct Positions *positions, u32 *alpha_unify,
										u32 *alpha_inc);
/*}}} private static methods */
/*{{{ public static methods */
static struct Keyword *kw_new(u8 *allchars, s32 allchars_length, u8 *rest, u32 lineno);
/* methods depending on the keyposition list */
static void kw_init_selchars_tuple(struct Keyword *t, struct Positions *positions,
										u32 *alpha_unify);
static void kw_init_selchars_multiset(struct Keyword *t, struct Positions *positions,
								u32 *alpha_unify, u32 *alpha_inc);
static void kw_delete_selchars(struct Keyword *t);
/*}}} public static methods -- END */
/*}}} Keyword class -- END */
#define EPILOG
#include "namespace/keyword.h"
#include "namespace/positions.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
