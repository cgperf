#ifndef CGPERF_HASH_TABLE_H
#define CGPERF_HASH_TABLE_H
#include <stdbool.h>
#include "c_fixing.h"
#include "keyword.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/hash-table.h"
#include "namespace/keyword.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ constants and types */
/* to make double hashing efficient, there need to be enough spare entries */
enum {
	ht_size_factor = 10
};
struct Hash_Table {
/*{{{ private */
	/* a detail of the comparison function */
	bool ignore_length;
	/* Statistics: Number of collisions so far. */
	u32 collisions;
	/* log2(_size).  */
	u32 log_size;
	/* size of the vector */
	u32 size;
	/* vector of entries */
	struct Keyword **table;
/*}}} private -- END */
};
/*}}} constants and types -- END */
/*{{{ public static methods */
static struct Hash_Table *ht_new(u32 size, bool ignore_length);
static void ht_del(struct Hash_Table *t);
static struct Keyword *ht_insert(struct Hash_Table *t, struct Keyword *item);
static void ht_dump(struct Hash_Table *t);
/*}}} public static methods -- END */
/*{{{ private static methods */
static bool ht_equal(struct Hash_Table *t, struct Keyword *item1, struct Keyword *item2);
/*}}} private static methods -- END */
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/hash-table.h"
#include "namespace/keyword.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
