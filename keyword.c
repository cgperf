#ifndef CGPERF_KEYWORD_C
#define CGPERF_KEYWORD_C
#include <stdlib.h>
#include "c_fixing.h"
#include "keyword.h"
#include "positions.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/keyword.h"
#include "namespace/positions.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ sort_char_set */
/* sort a small set of 'unsigned int', base[0..len-1], in place */
static void sort_char_set(u32 *base, s32 len)
{
	s32 i;

	/* bubble sort is sufficient here */
	i = 1;
	loop {
		s32 j;
		u32 tmp;

		if (i >= len)
			break;
		j = i;
		tmp = base[j];
		loop {
			if (j <= 0 || tmp  >= base[j - 1])
				break;
			base[j] = base[j - 1];
			j--;
		}
		base[j] = tmp;
		++i;
	}
}/*}}}*/
/*{{{ kw_new */
static struct Keyword *kw_new(u8 *allchars, s32 allchars_length, u8 *rest, u32 lineno)
{
	struct Keyword *t;

	t = calloc(1, sizeof(*t));
	t->allchars = allchars;
	t->allchars_length = allchars_length;
	t->rest = rest;
	t->lineno = lineno;
	t->final_index = -1;
	return t;
}/*}}}*/
/*{{{ kw_init_selchars_low */
/* Initializes selchars and selchars_length.

   General idea:
     The hash function will be computed as
         asso_values[allchars[key_pos[0]]] +
         asso_values[allchars[key_pos[1]]] + ...
     We compute selchars as the multiset
         { allchars[key_pos[0]], allchars[key_pos[1]], ... }
     so that the hash function becomes
         asso_values[selchars[0]] + asso_values[selchars[1]] + ...
   Furthermore we sort the selchars array, to ease detection of duplicates
   later.

   More in detail: The arguments alpha_unify (used for case-insensitive
   hash functions) and alpha_inc (used to disambiguate permutations)
   apply slight modifications. The hash function will be computed as
       sum (j=0,1,...: k = key_pos[j]:
            asso_values[alpha_unify[allchars[k]+alpha_inc[k]]])
       + (allchars_length if !option[NOLENGTH], 0 otherwise).
   We compute selchars as the multiset
       { alpha_unify[allchars[k]+alpha_inc[k]] : j=0,1,..., k = key_pos[j] }
   so that the hash function becomes
       asso_values[selchars[0]] + asso_values[selchars[1]] + ...
       + (allchars_length if !option[NOLENGTH], 0 otherwise).
 */
static u32 *kw_init_selchars_low(struct Keyword *t, struct Positions *positions, u32 *alpha_unify,
										u32 *alpha_inc)
{
	/* iterate through the list of positions, initializing selchars(via ptr) */
	struct PositionIterator *iter;
	u32 *key_set;
	u32 *ptr;

	iter = pos_iterator(positions, t->allchars_length);
	key_set = calloc(positer_remaining(iter), sizeof(*key_set));
	ptr = key_set;

	loop {
		s32 i;
		u32 c;

		i = positer_next(iter);
		if (i == POSITER_EOS)
			break;

		if (i == POS_LASTCHAR)
        		/* special notation for last KEY position, i.e. '$' */
			c = (u8)(t->allchars[t->allchars_length - 1]);
		else if (i < t->allchars_length) {
			/* within range of KEY length, so we'll keep it */
			c = (u8)(t->allchars[i]);
			if (alpha_inc != 0)
				c += alpha_inc[i];
		} else
        		/*
			 * out of range of KEY length, the iterator should not
			 * have produced this
			 */
			abort();
		if (alpha_unify != 0)
			c = alpha_unify[c];
		*ptr = c;
		++ptr;
	}
	t->selchars = key_set;
	t->selchars_length = ptr - key_set;
	positer_del(iter);
	return key_set;
}/*}}}*/
/*{{{ kw_init_selchars_tuple */
static void kw_init_selchars_tuple(struct Keyword *t, struct Positions *positions,
										u32 *alpha_unify)
{
	kw_init_selchars_low(t, positions, alpha_unify, 0);
}/*}}}*/
/*{{{ kw_delete_selchars */
static void kw_delete_selchars(struct Keyword *t)
{
	free(t->selchars);
}/*}}}*/
/*{{{ kw_init_selchars_multiset */
/* initializes selchars and selchars_length, with reordering */
static void kw_init_selchars_multiset(struct Keyword *t, struct Positions *positions,
								u32 *alpha_unify, u32 *alpha_inc)
{
	u32 *selchars;

	selchars = kw_init_selchars_low(t, positions, alpha_unify, alpha_inc);
	/* sort the selchars elements alphabetically */
	sort_char_set(selchars, t->selchars_length);
}/*}}}*/
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/keyword.h"
#include "namespace/positions.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
