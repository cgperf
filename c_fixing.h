#ifndef C_FIXING_H
#define C_FIXING_H
#include <stdint.h>
#include <limits.h>
#define u8 uint8_t
#define u32 uint32_t
#define U32_MIN UINT_MIN
#define U32_MAX UINT_MAX
#define u64 uint64_t
#define s32 int32_t
#define S32_MIN INT_MIN
#define S32_MAX INT_MAX
#define f32 float
#define f64 double
#define loop for(;;)
#endif
