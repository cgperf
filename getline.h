#ifndef CGPERF_GETLINE_H
#define CGPERF_GETLINE_H
#include <stdio.h>
#include "c_fixing.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/getline.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ local */
static s32 getstr(u8 **lineptr, u32 *n, FILE *stream, u8 terminator,
								u32 offset);
/*}}}*/
/*{{{ public */
static s32 get_delim(u8 **lineptr, u32 *n, s32 delimiter, FILE *stream);
/*}}}*/
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/getline.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
