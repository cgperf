#ifndef CGPERF_OPTIONS_H
#define CGPERF_OPTIONS_H
#include <stdbool.h>
#include <stdio.h>
#include "c_fixing.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/positions.h"
#include "namespace/options.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ Options */
/*{{{ globals */
/* records the program name */
static u8 *opts_program_name;
/*}}} globals -- END */
/*{{{ constants */
enum {
	/* size to jump on a collision */
	OPTS_DEFAULT_JUMP_VALUE = 5,
	/* enumeration of the possible boolean options  */
	/* --- input file interpretation --- */
	/* handle user-defined type structured keyword input */
	OPTS_TYPE = 1 << 0,
	/* ignore case of ASCII characters */
	OPTS_UPPERLOWER = 1 << 1,
	/* --- language for the output code --- */
	/* generate K&R C code: no prototypes, no const */
	OPTS_KRC = 1 << 2,
	/* generate C code: no prototypes, but const (user can #define it away) */
	OPTS_C = 1 << 3,
	/* generate ISO/ANSI C code: prototypes and const, but no class */
	OPTS_ANSIC = 1 << 4,
	/* generate C++ code: prototypes, const, class, inline, enum */
	OPTS_CPLUSPLUS = 1 << 5,
	/* --- details in the output code --- */
	/* assume 7-bit, not 8-bit, characters */
	OPTS_SEVENBIT = 1 << 6,
	/* generate a length table for string comparison */
	OPTS_LENTABLE = 1 << 7,
	/* generate strncmp rather than strcmp */
	OPTS_COMP = 1 << 8,
	/* make the generated tables readonly (const) */
	OPTS_CONST = 1 << 9,
	/* use enum for constants */
	OPTS_ENUM = 1 << 10,
	/* generate #include statements */
	OPTS_INCLUDE = 1 << 11,
	/* make the keyword table a global variable */
	OPTS_GLOBAL = 1 << 12,
	/* use NULL strings instead of empty strings for empty table entries */
	OPTS_NULLSTRINGS = 1 << 13,
	/* optimize for position-independent code */
	OPTS_SHAREDLIB = 1 << 14,
	/* generate switch output to save space */
	OPTS_SWITCH = 1 << 15,
	/* don't include user-defined type definition in output -- it's already defined elsewhere */
	OPTS_NOTYPE = 1 << 16,
	/* --- algorithm employed by gperf --- */
	/* use the given key positions  */
	OPTS_POSITIONS = 1 << 17,
	/* handle duplicate hash values for keywords */
	OPTS_DUP = 1 << 18,
	/* don't include keyword length in hash computations */
	OPTS_NOLENGTH = 1 << 19,
	/* randomly initialize the associated values table */
	OPTS_RANDOM = 1 << 20,
	/* --- informative output --- */
	/* enable debugging (prints diagnostics to stderr) */
	OPTS_DEBUG = 1 << 21,
};
/*}}} constants -- END */
/*{{{ types */
struct Options {
	/* records count of command-line arguments */
	u32 argument_count;
	/* stores a pointer to command-line argument vector */
	u8 **argument_vector;
	/* holds the boolean options */
	u32 option_word;
	/* separates keywords from other attributes */
	u8 *delimiters;
	/* suffix for empty struct initializers */
	u8 *initializer_suffix;
	/* name used for generated hash function */
	u8 *hash_name;
	/* initial value for asso_values table */
	s32 initial_asso_value;
	/* jump length when trying alternative values */
	s32 jump;
	/* contains user-specified key choices */
	struct Positions *key_positions;
	/* Name used for keyword key */
	u8 *slot_name;
	/* the output language */
	u8 *language;
  	/* number of attempts at finding good asso_values */
	s32 asso_iterations;
	/* names used for generated lookup function */
	u8 *function_name;
	/* name used for the string pool */
	u8 *stringpool_name;
	/* factor by which to multiply the generated table's size */
	f32 size_multiple;
	/* number of switch statements to generate */
	s32 total_switches;
	/* name used for hash table array */
	u8 *wordlist_name;
	/* name used for generated C++ class */
	u8 *class_name;
	/* name of output file */
	u8 *output_file_name;
	/* name used for length table array */
	u8 *lengthtable_name;
	/* prefix for the constants */
	u8 *constants_prefix;
	/* name of input file */
	u8 *input_file_name;
};
/*}}} types -- END */
/*{{{ public static methods */
static struct Options *opts_new(void);
static void opts_del(struct Options *t);
static void opts_parse_options(struct Options *t, u32 argc, u8 **argv);
static void opts_long_usage(FILE *stream);
static void opts_short_usage(FILE *stream);
static void opts_print(struct Options *t);
/*{{{ accessors */
static void opts_set_delimiters(struct Options *t, u8 *delimiters);
static void opts_set_language(struct Options *t, u8 *language);
static void opts_set_slot_name(struct Options *t, u8 *name);
static void opts_set_initializer_suffix(struct Options *t, u8 *initializers);
static void opts_set_hash_name(struct Options *t, u8 *name);
static void opts_set_function_name(struct Options *t, u8 *name);
static void opts_set_class_name(struct Options *t, u8 *name);
static void opts_set_stringpool_name(struct Options *t, u8 *name);
static void opts_set_constants_prefix(struct Options *t, u8 *prefix);
static void opts_set_wordlist_name(struct Options *t, u8 *name);
static void opts_set_lengthtable_name(struct Options *t, u8 *name);
static void opts_set_total_switches(struct Options *t, s32 total_switches);
/*}}}*/
/*}}} public static methods -- END */
/*}}} Options -- END */
/*{{{ PositionStringParser */
/*{{{ constants and types */
struct PositionStringParser {
/*{{{ private */
	/* A pointer to the string provided by the user */
	u8 *str;
	/* Smallest possible value, inclusive */
	s32 low_bound;
	/* Greatest possible value, inclusive */
	s32 high_bound;
	/* A value marking the abstract "end of word" ( usually '$') */
	s32 end_word_marker;
	/* Error value returned when input is syntactically erroneous */
	s32 error_value;
	/* Value returned after last key is processed */
	s32 end_marker;
	/* Intermediate state for producing a range of positions */
	bool in_range; /* True while producing a range of positions */
	s32 range_upper_bound; /* Upper bound (inclusive) of the range */
	s32 range_curr_value; /* Last value returned */
/*}}} private -- END */
};
/*}}} constants and types -- END */
/*{{{ public static methods */
static struct PositionStringParser *posstrp_new(u8 *str, s32 low_bound, s32 high_bound,
					s32 end_word_marker, s32 error_value, s32 end_marker);
static void posstrp_del(struct PositionStringParser *t);
static s32 posstrp_nextPosition(struct PositionStringParser *t);
/*}}} public static methods -- END */
/*}}} PositionStringParser -- END */
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/positions.h"
#include "namespace/options.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
