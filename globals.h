#ifndef CGPERF_GLOBALS_H
#define CGPERF_GLOBALS_H
#include "options.h"
/*----------------------------------------------------------------------------*/
#include "namespace/globals.h"
#include "namespace/options.h"
/*----------------------------------------------------------------------------*/
static struct Options *options;
/*----------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/globals.h"
#include "namespace/options.h"
#undef EPILOG
/*----------------------------------------------------------------------------*/
#endif
