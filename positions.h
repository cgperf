#ifndef CGPERF_POSITIONS_H
#define CGPERF_POSITIONS_H
#include <stdbool.h>
#include "c_fixing.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/positions.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ Positions */
struct PositionIterator;
struct PositionReverseIterator;
/*{{{ constants */
enum {
	/*
	 * Maximum key position specifiable by the user, 1-based. Note that max_key_pos-1 must fit
	 * into the element type of positions[], below.
	 */
	POS_MAX_KEY_POS = 255,
	/* Denotes the last char of a keyword, depending on the keyword's length */
	POS_LASTCHAR = -1,
	/*
	 * Maximum possible size. Since duplicates are eliminated and the possible 0-based positions
	 * are -1..max_key_pos-1, this is:
	 */
	POS_MAX_SIZE = POS_MAX_KEY_POS + 1,
};
/*}}} constants -- END */
/*{{{ types */
struct Positions {
/*{{{ public */
	/*
	 * array of positions. 0 for the first char, 1 for the second char etc., lastchar for the
	 * last char
	 */
	s32 positions[POS_MAX_SIZE];
	/* number of positions */
	u32 size;
/*}}} public -- END */
/*{{{ private */
	/* The special case denoted by '*' */
	bool useall;
/*}}} private -- END */
};
/*}}} types -- END */
/*{{{ public static methods */
static struct Positions *pos_new(void);
static struct Positions *pos_new_cpy(struct Positions *src);
static void pos_print(struct Positions *t);
static void pos_del(struct Positions *t);
static bool pos_contains(struct Positions *t, s32 pos);
static void pos_add(struct Positions *t, s32 pos);
static void pos_remove(struct Positions *t, s32 pos);
/* Write access */
static void pos_set_useall(struct Positions *t, bool useall);
static bool pos_sort(struct Positions *t);
static struct PositionIterator *pos_iterator(struct Positions *t, s32 maxlen);
static struct PositionIterator *pos_iterator_all(struct Positions *t);
static struct PositionReverseIterator *pos_reviterator(struct Positions *t);
static void pos_cpy(struct Positions *d, struct Positions *s);
/*}}} public static methods -- END */
/*}}} Position -- END */
/*{{{ PositionIterator */
/*{{{ constants */
enum {
	/* end of iteration marker */
	POSITER_EOS = -2,
};
/*}}} constants -- END */
/*{{{ types */
struct PositionIterator {
/*{{{ private */
	struct Positions *set;
	u32 index;
/*}}}*/
};
/*}}} types -- END */
/*{{{ public static methods */
static struct PositionIterator *positer_new(struct Positions *positions, s32 maxlen);
static struct PositionIterator *positer_new_all(struct Positions *positions);
static void positer_del(struct PositionIterator *t);
static u32 positer_remaining(struct PositionIterator *t);
static s32 positer_next(struct PositionIterator *t);
/*}}} public static methods -- END */
/*}}} PositionIterator -- END */
/*{{{ PositionReverseIterator */
/*{{{ constants and types */
enum {
	/* end of iteration marker */
	POSREVIT_EOS = -2,
};
struct PositionReverseIterator {
/*{{{ private */
	struct Positions *set;
	u32 index;
	u32 minindex;
/*}}} private -- END */
};
/*}}} constants and types -- END */
/*{{{ public static methods */
static struct PositionReverseIterator *posrevit_new(struct Positions *positions);
static void posrevit_del(struct PositionReverseIterator *t);
static s32 posrevit_next(struct PositionReverseIterator *t);
/*}}} public static methods -- END */
/*}}} PositionReverseIterator -- END */
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/positions.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
