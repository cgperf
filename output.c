#ifndef CGPERF_OUTPUT_C
#define CGPERF_OUTPUT_C
#include <stdbool.h>
#include "c_fixing.h"
#include "globals.h"
#include "options.h"
#include "output.h"
#include "keyword.h"
#include "keyword_list.h"
#include "positions.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/globals.h"
#include "namespace/options.h"
#include "namespace/output.h"
#include "namespace/output.c"
#include "namespace/keyword.h"
#include "namespace/keyword_list.h"
#include "namespace/positions.h"
/*------------------------------------------------------------------------------------------------*/
/* We use a downcase table because when called repeatedly, the code gperf_downcase[c]
   is faster than
       if (c >= 'A' && c <= 'Z')
         c += 'a' - 'A';
 */
#define USE_DOWNCASE_TABLE 1
/*------------------------------------------------------------------------------------------------*/
/*{{{ local */
/*{{{ types */
/*
 * because of the way output_keyword_table works, every duplicate set is
 * stored contiguously in the wordlist array
 */
struct Duplicate_Entry
{
	s32 hash_value; /* hash value for this particular duplicate set */
	s32 index;      /* index into the main keyword storage array */
	s32 count;      /* number of consecutive duplicates at this index */
};
/*}}}*/
/*{{{ variables */
/* the "register " storage-class specifier */
static u8 *register_scs;
/* the "const " qualifier */
static u8 *const_always;
/* the "const " qualifier, for read-only arrays */
static u8 *const_readonly_array;
/* the "const " qualifier, for the array type */
static u8 *const_for_struct;
/*}}} variables -- END */
/*{{{ code */
/*{{{ output_string */
/*
 * Outputs a keyword, as a string: enclosed in double quotes, escaping backslashes, double quote and
 * unprintable characters
 */
static void output_string(u8 *key, s32 len)
{
	putchar('"');
	loop {
		u8 c;
	
		if (len <= 0)
			break;
		c = (u8)(*key++);
		if (isprint(c)) {
			if (c == '"' || c == '\\')
				putchar('\\');
			putchar(c);
		} else {
			/*
			 * Use octal escapes, not hexadecimal escapes, because some old C compilers
			 * didn't understand hexadecimal escapes, and because hexadecimal escapes
			 * are not limited to 2 digits, thus needing special care if the following
			 * character happens to be a digit.
			 */
			putchar('\\');
			putchar('0' + ((c >> 6) & 7));
			putchar('0' + ((c >> 3) & 7));
			putchar('0' + (c & 7));
		}
		len--;
	}
	putchar('"');
}/*}}}*/
/*{{{ output_line_directive */
/* outputs a #line directive, referring to the given line number */
static void output_line_directive(u32 lineno)
{
	u8 *file_name;

	file_name = options->input_file_name;
	if (file_name != 0) {
		printf("#line %u ", lineno);
		output_string(file_name, (s32)strlen(file_name));
		printf("\n");
	}
}/*}}}*/
/*{{{ output_constant_define */
static void output_constant_define(u8 *name, s32 value)
{
	u8 *prefix;
	u8 *combined_name;

	prefix = options->constants_prefix;
	combined_name = calloc(strlen(prefix) + strlen(name) + 1, sizeof(u8));
	strcpy(combined_name, prefix);
	strcpy(combined_name + strlen(prefix), name);
	printf("#define %s %d\n", combined_name, value);
	free(combined_name);
}/*}}}*/
/*{{{ output_constant_enum */
static void output_constant_enum(u8 *name, s32 value, u8 *indentation, bool *pending_comma)
{
	u8 *prefix;
	u8 *combined_name;

	prefix = options->constants_prefix;
	combined_name = calloc(strlen(prefix) + strlen(name) + 1, sizeof(u8));
	strcpy(combined_name, prefix);
	strcpy(combined_name + strlen(prefix), name);
	if (*pending_comma)
		printf (",\n");
	printf("%s    %s = %d", indentation, combined_name, value);
	*pending_comma = true;
	free(combined_name);
}/*}}}*/
/*{{{ ouput_upperlower_table */
#if USE_DOWNCASE_TABLE
static void output_upperlower_table(void)
{
	u32 c;

	printf(
"#ifndef GPERF_DOWNCASE\n"
"#define GPERF_DOWNCASE 1\n"
"static %sunsigned char gperf_downcase[256] =\n"
"  {",
		const_readonly_array);
	c = 0;
	loop {
		if (c >= 256)
			break;
		if ((c % 15) == 0)
			printf("\n   ");
		printf(" %3d", c >= 'A' && c <= 'Z' ? c + 'a' - 'A' : c);
		if (c < 255)
			printf (",");
		++c;
	}
	printf("\n"
"  };\n"
"#endif\n\n");
}
#endif
/*}}}*/
/*{{{ output_upperlower_memcmp */
/* output gperf's ASCII-case insensitive memcmp replacement */
static void output_upperlower_memcmp(void)
{
	printf(
"#ifndef GPERF_CASE_MEMCMP\n"
"#define GPERF_CASE_MEMCMP 1\n"
"static int\n"
"gperf_case_memcmp ");
	printf(OPTS(KRC) ? "(s1, s2, n)\n"
"     %schar *s1;\n"
"     %schar *s2;\n"
"     %ssize_t n;\n" :
		OPTS(C) ? "(s1, s2, n)\n"
"     %sconst char *s1;\n"
"     %sconst char *s2;\n"
"     %ssize_t n;\n" :
		OPTS(ANSIC) || OPTS(CPLUSPLUS) ? "(%sconst char *s1, %sconst char *s2, %ssize_t n)\n" :
"", register_scs, register_scs, register_scs);
#if USE_DOWNCASE_TABLE
	printf(
"{\n"
"  for (; n > 0;)\n"
"    {\n"
"      unsiGNED char c1 = gperf_downcase[(unsigned char)*s1++];\n"
"      unsigned char c2 = gperf_downcase[(unsigned char)*s2++];\n"
"      if (c1 == c2)\n"
"        {\n"
"          n--;\n"
"          continue;\n"
"        }\n"
"      return (int)c1 - (int)c2;\n"
"    }\n"
"  return 0;\n"
"}\n");
#else
	printf(
"{\n"
"  for (; n > 0;)\n"
"    {\n"
"      unsigned char c1 = *s1++;\n"
"      unsigned char c2 = *s2++;\n"
"      if (c1 >= 'A' && c1 <= 'Z')\n"
"        c1 += 'a' - 'A';\n"
"      if (c2 >= 'A' && c2 <= 'Z')\n"
"        c2 += 'a' - 'A';\n"
"      if (c1 == c2)\n"
"        {\n"
"          n--;\n"
"          continue;\n"
"        }\n"
"      return (int)c1 - (int)c2;\n"
"    }\n"
"  return 0;\n"
"}\n");
#endif
	printf(
"#endif\n\n");
}/*}}}*/
/*{{{ output_upperlower_strncmp */
/* output gperf's ASCII-case insensitive strncmp replacement */
static void output_upperlower_strncmp(void)
{
	printf(
"#ifndef GPERF_CASE_STRNCMP\n"
"#define GPERF_CASE_STRNCMP 1\n"
"static int\n"
"gperf_case_strncmp ");
	printf(OPTS(KRC) ? "(s1, s2, n)\n"
"     %schar *s1;\n"
"     %schar *s2;\n"
"     %ssize_t n;\n" :
		OPTS(C) ? "(s1, s2, n)\n"
"     %sconst char *s1;\n"
"     %sconst char *s2;\n"
"     %ssize_t n;\n" :
		OPTS(ANSIC) || OPTS(CPLUSPLUS) ? "(%sconst char *s1, %sconst char *s2, %ssize_t n)\n" :
"", register_scs, register_scs, register_scs);
#if USE_DOWNCASE_TABLE
	printf(
"{\n"
"  for (; n > 0;)\n"
"    {\n"
"      unsigned char c1 = gperf_downcase[(unsigned char)*s1++];\n"
"      unsigned char c2 = gperf_downcase[(unsigned char)*s2++];\n"
"      if (c1 != 0 && c1 == c2)\n"
"        {\n"
"          n--;\n"
"          continue;\n"
"        }\n"
"      return (int)c1 - (int)c2;\n"
"    }\n"
"  return 0;\n"
"}\n");
#else
	printf(
"{\n"
"  for (; n > 0;)\n"
"    {\n"
"      unsigned char c1 = *s1++;\n"
"      unsigned char c2 = *s2++;\n"
"      if (c1 >= 'A' && c1 <= 'Z')\n"
"        c1 += 'a' - 'A';\n"
"      if (c2 >= 'A' && c2 <= 'Z')\n"
"        c2 += 'a' - 'A';\n"
"      if (c1 != 0 && c1 == c2)\n"
"        {\n"
"          n--;\n"
"          continue;\n"
"        }\n"
"      return (int)c1 - (int)c2;\n"
"    }\n"
"  return 0;\n"
"}\n");
#endif
	printf(
"#endif\n\n");
}/*}}}*/
/*{{{ output_upperlower_strcmp */
/* output gperf's ASCII-case insensitive strcmp replacement */
static void output_upperlower_strcmp(void)
{
	printf(
"#ifndef GPERF_CASE_STRCMP\n"
"#define GPERF_CASE_STRCMP 1\n"
"static int\n"
"gperf_case_strcmp ");
	printf(OPTS(KRC) ?  "(s1, s2)\n"
"     %schar *s1;\n"
"     %schar *s2;\n" :
		OPTS(C) ?  "(s1, s2)\n"
"     %sconst char *s1;\n"
"     %sconst char *s2;\n" :
		OPTS(ANSIC) || OPTS(CPLUSPLUS) ? "(%sconst char *s1, %sconst char *s2)\n" :
"", register_scs, register_scs);
#if USE_DOWNCASE_TABLE
	printf(
"{\n"
"  for (;;)\n"
"    {\n"
"      unsigned char c1 = gperf_downcase[(unsigned char)*s1++];\n"
"      unsigned char c2 = gperf_downcase[(unsigned char)*s2++];\n"
"      if (c1 != 0 && c1 == c2)\n"
"        continue;\n"
"      return (int)c1 - (int)c2;\n"
"    }\n"
"}\n");
#else
	printf(
"{\n"
"  for (;;)\n"
"    {\n"
"      unsigned char c1 = *s1++;\n"
"      unsigned char c2 = *s2++;\n"
"      if (c1 >= 'A' && c1 <= 'Z')\n"
"        c1 += 'a' - 'A';\n"
"      if (c2 >= 'A' && c2 <= 'Z')\n"
"        c2 += 'a' - 'A';\n"
"      if (c1 != 0 && c1 == c2)\n"
"        continue;\n"
"      return (int)c1 - (int)c2;\n"
"    }\n"
"}\n");
#endif
	printf
("#endif\n\n");
}/*}}}*/
/*{{{ smallest_integral_type */
/* returns the smallest unsigned C type capable of holding integers up to N */
static u8 *smallest_integral_type(s32 n)
{
	if (n <= UCHAR_MAX) return "unsigned char";
	if (n <= USHRT_MAX) return "unsigned short";
	return "unsigned int";
}/*}}}*/
/*{{{ smallest_integral_type_2 */
/* returns the smallest signed C type capable of holding integers from MIN to MAX */
static u8 *smallest_integral_type_2(s32 min, s32 max)
{
	if (OPTS(ANSIC) || OPTS(CPLUSPLUS))
		if (min >= SCHAR_MIN && max <= SCHAR_MAX) return "signed char";
	if (min >= SHRT_MIN && max <= SHRT_MAX) return "short";
	return "int";
}/*}}}*/
/*{{{ output_const_type */
/*
 * Outputs a type and a const specifier (i.e. "const " or "").
 * The output is terminated with a space.
 */
static void output_const_type(u8 *const_string, u8 *type_string)
{
	if (type_string[strlen(type_string) - 1] == '*')
		/* for pointer types, put the 'const' after the type */
		printf(
"%s %s",		type_string, const_string);
	else
		/* for scalar or struct types, put the 'const' before the type */
		printf(
"%s%s ",		const_string, type_string);
}/*}}}*/
/*{{{ output_keyword_blank_entries */
static void output_keyword_blank_entries(s32 count, u8 *indent)
{
	s32 columns;
	s32 column;
	s32 i;

	if (OPTS(TYPE)) {
		columns = 58 / (4 + (OPTS(SHAREDLIB) ? 2 : OPTS(NULLSTRINGS) ? 8 : 2)
							+ strlen(options->initializer_suffix));
		if (columns == 0)
			columns = 1;
	} else
		columns = (OPTS(SHAREDLIB) ? 9 : OPTS(NULLSTRINGS) ? 4 : 9);
	column = 0;
	i = 0;
	loop {
		if (i >= count)
			break;
		if ((column % columns) == 0) {
			if (i > 0)
				printf(
",\n");
			printf(
"%s    ",			indent);
		} else if (i > 0)
				printf(", ");
		if (OPTS(TYPE))
			printf("{");
		if (OPTS(SHAREDLIB))
			printf("-1");
		else {
			if (OPTS(NULLSTRINGS))
				printf("(char*)0");
			else
				printf("\"\"");
		}
		if (OPTS(TYPE))
			printf(
"%s}",				options->initializer_suffix);
		++column;
		++i;
	}
}/*}}}*/
/*{{{ output_keyword_entry */
static void output_keyword_entry(struct Keyword *tmp, s32 stringpool_index, u8 *indent,
										bool is_duplicate)
{
	if (OPTS(TYPE))
		output_line_directive(tmp->lineno);
	printf(
"%s    ",	indent);
	if (OPTS(TYPE))
		printf("{");
	if (OPTS(SHAREDLIB))
		/*
		 * How to determine a certain offset in stringpool at compile time?
		 *  - The standard way would be to use the 'offsetof' macro.  But it is only
		 *    defined in <stddef.h>, and <stddef.h> is not among the prerequisite
		 *    header files that the user must #include.
		 *  - The next best way would be to take the address and cast to 'intptr_t'
		 *    or 'uintptr_t'.  But these types are only defined in <stdint.h>, and
		 *    <stdint.h> is not among the prerequisite header files that the user
		 *    must #include.
		 *  - The next best approximation of 'uintptr_t' is 'size_t'.  It is defined
		 *    in the prerequisite header <string.h>.
		 *  - The types 'long' and 'unsigned long' do work as well, but on 64-bit
		 *    native Windows platforms, they don't have the same size as pointers
		 *    and therefore generate warnings.
		 */
		printf("(int)(size_t)&((struct %s_t *)0)->%s_str%d",
			options->stringpool_name, options->stringpool_name, stringpool_index);
	else
		output_string(tmp->allchars, tmp->allchars_length);
	if (OPTS(TYPE)) {
		if (strlen(tmp->rest) > 0)
			printf(",%s", tmp->rest);
		printf("}");
	}
	if (OPTS(DEBUG)) {
		printf(" /* ");
		if (is_duplicate)
			printf("hash value duplicate, ");
		else
			printf("hash value = %d, ", tmp->hash_value);
		printf("index = %d */", tmp->final_index);
	}

}/*}}}*/
/*{{{ output_switch_case */
/* Output a single switch case (including duplicates).  Advance list.  */
static struct Keyword_List *output_switch_case(struct Keyword_List *list, s32 indent,
										s32 *jumps_away)
{
	if (OPTS(DEBUG))
		printf(
"%*s/* hash value = %4d, keyword = \"%.*s\" */\n", indent, "", list->kw->hash_value,
						list->kw->allchars_length, list->kw->allchars);
	if (OPTS(DUP) && list->kw->duplicate_link) {
		s32 count;
		struct Keyword *links;

		if (OPTS(LENTABLE))
			printf(
"%*slengthptr = &%s[%d];\n", indent, "", options->lengthtable_name, list->kw->final_index);
		printf(
"%*swordptr = &%s[%d];\n", indent, "", options->wordlist_name, list->kw->final_index);
		count = 0;
		links = list->kw;
		loop {
			if (links == 0)
				break;
			++count;
			links = links->duplicate_link;
		}
		printf(
"%*swordendptr = wordptr + %d;\n"
"%*sgoto multicompare;\n", indent, "", count, indent, "");
		*jumps_away = 1;
	} else {
		if (OPTS(LENTABLE)) {
			printf(
"%*sif (len == %d)\n"
"%*s  {\n",			indent, "", list->kw->allchars_length, indent, "");
			indent += 4;
		}
		printf("%*sresword = ",	indent, "");
		if (OPTS(TYPE))
			printf("&%s[%d]", options->wordlist_name, list->kw->final_index);
		else
			output_string(list->kw->allchars, list->kw->allchars_length);
		printf(";\n");
		printf(
"%*sgoto compare;\n", indent, "");
		if (OPTS(LENTABLE)) {
			indent -= 4;
			printf(
"%*s  }\n",			indent, "");
		} else
			*jumps_away = 1;
	}
	return list->next;
}/*}}}*/
/*{{{ output_switches */
/*
 * output a total of size cases, grouped into num_switches switch statements, where 0 <
 * num_switches <= size
 */
static void output_switches(struct Keyword_List *list, s32 num_switches, s32 size,
						s32 min_hash_value, s32 max_hash_value, s32 indent)
{
	if (OPTS(DEBUG))
		printf(
"%*s/* know %d <= key <= %d, contains %d cases */\n", indent, "", min_hash_value, max_hash_value,
											size);
	if (num_switches > 1) {
		s32 part1;
		s32 part2;
		s32 size1;
		s32 size2;
		struct Keyword_List *tmp;
		s32 count;

		part1 = num_switches / 2;
		part2 = num_switches - part1;
		size1 = (s32)((f64)(size) / (f64)(num_switches) * (f64)(part1) + 0.5);
		size2 = size - size1;

		tmp = list;
		count = size1;
		loop {
			if (count <= 0)
				break;
			tmp = tmp->next;
			count--;
		}
		printf(
"%*sif (key < %d)\n"
"%*s  {\n",		indent, "", tmp->kw->hash_value, indent, "");
		output_switches(list, part1, size1, min_hash_value, tmp->kw->hash_value - 1,
											indent + 4);
		printf(
"%*s  }\n"
"%*selse\n"
"%*s  {\n",		indent, "", indent, "", indent, "");
		output_switches(tmp, part2, size2, tmp->kw->hash_value, max_hash_value, indent + 4);
		printf(
"%*s  }\n",		indent, "");
	} else {
		s32 lowest_case_value;

		lowest_case_value = list->kw->hash_value;
		if (size == 1) {
			s32 jumps_away;

			jumps_away = 0;
			if (min_hash_value == max_hash_value)
				output_switch_case(list, indent, &jumps_away);
			else {
				printf(
"%*sif (key == %d)\n"
"%*s  {\n",				indent, "", lowest_case_value, indent, "");
				output_switch_case(list, indent + 4, &jumps_away);
				printf(
"%*s  }\n",				indent, "");
			}
		} else {
			if (lowest_case_value == 0)
				printf(
"%*sswitch (key)\n",			indent, "");
			else
				printf(
"%*sswitch (key - %d)\n",		indent, "", lowest_case_value);
			printf(
"%*s  {\n",			indent, "");
			loop {
				s32 jumps_away;

				if (size <= 0)
					break;
				jumps_away = 0;
				printf(
"%*s    case %d:\n",			indent, "", list->kw->hash_value - lowest_case_value);
				list = output_switch_case(list, indent + 6, &jumps_away);
				if (!jumps_away)
					printf(
"%*s      break;\n",				indent, "");
				size--;
			}
			printf(
"%*s  }\n",			indent, "");
		}
	}
}/*}}}*/
/*{{{ output_firstchar_comparison */
/*
 * Outputs the comparison expression for the first byte. Returns true if the this comparison is
 * complete.
 */
static bool output_firstchar_comparison(u8 *expr1, u8 *expr2)
{
	/*
	 * First, we emit a comparison of the first byte of the two strings.  This catches most
	 * cases where the string being looked up is not in the hash table but happens to have the
	 * same hash code as an element of the hash table.
	 */
	if (OPTS(UPPERLOWER)) {
		/* incomplete comparison, just for speedup */
		printf("(((unsigned char)*");
		printf("%s", expr1);
		printf(" ^ (unsigned char)*");
		printf("%s", expr2);
		printf(") & ~32) == 0");
		return false;
	}
	/* Complete comparison.  */
	printf("*");
	printf("%s", expr1);
	printf(" == *");
	printf("%s", expr2);
	return true;
}/*}}}*/
/*------------------------------------------------------------------------------------------------*/
/*{{{ output_comparison_X */
/*
 * Outputs the comparison expression.
 * expr1 outputs a simple expression of type 'const char *' referring to the string being looked up.
 * expr2 outputs a simple expression of type 'const char *' referring to the constant string stored
 * in the gperf generated hash table.
 */
/*{{{ output_comparison_memcmp */
static void output_comparison_memcmp(u8 *expr1, u8 *expr2)
{
	bool firstchar_done;

	firstchar_done = output_firstchar_comparison(expr1, expr2);
	printf(" && !");
	if (OPTS(UPPERLOWER))
		printf("gperf_case_");
	printf("memcmp (");
	if (firstchar_done) {
		printf("%s", expr1);
		printf(" + 1, ");
		printf("%s", expr2);
		printf(" + 1, len - 1");
	} else {
		printf("%s", expr1);
		printf(", ");
		printf("%s", expr2);
		printf(", len");
	}
	printf(")");
}/*}}}*/
/*{{{ output_comparison_strncmp */
static void output_comparison_strncmp(u8 *expr1, u8 *expr2)
{
	bool firstchar_done;

	firstchar_done = output_firstchar_comparison(expr1, expr2);
	printf(" && !");
	if (OPTS(UPPERLOWER))
		printf("gperf_case_");
	printf("strncmp (");
	if (firstchar_done) {
		printf("%s", expr1);
		printf(" + 1, ");
		printf("%s", expr2);
		printf(" + 1, len - 1");
	} else {
		printf("%s", expr1);
		printf(", ");
		printf("%s", expr2);
		printf(", len");
	}
	printf(") && ");
	printf("%s", expr2);
	printf("[len] == '\\0'");
}/*}}}*/
/*{{{ output_comparison_strcmp */
static void output_comparison_strcmp(u8 *expr1, u8 *expr2)
{
	bool firstchar_done;

	firstchar_done = output_firstchar_comparison(expr1, expr2);
	printf(" && !");
	if (OPTS(UPPERLOWER))
		printf("gperf_case_");
	printf("strcmp (");
	if (firstchar_done) {
		printf("%s", expr1);
		printf(" + 1, ");
		printf("%s", expr2);
		printf(" + 1");
	} else {
		printf("%s", expr1);
		printf(", ");
		printf("%s", expr2);
	}
	printf(")");
}/*}}}*/
/*}}} output_comparison_X -- END */
/*------------------------------------------------------------------------------------------------*/
/*}}} code -- END */
/*}}} local -- END */
/*------------------------------------------------------------------------------------------------*/
/*{{{ output_new */
/* Constructor.
   Note about the keyword list starting at head:
   - The list is ordered by increasing _hash_value.  This has been achieved
     by Search::sort().
   - Duplicates, i.e. keywords with the same _selchars set, are chained
     through the _duplicate_link pointer.  Only one representative per
     duplicate equivalence class remains on the linear keyword list.
   - Accidental duplicates, i.e. keywords for which the _asso_values[] search
     couldn't achieve different hash values, cannot occur on the linear
     keyword list.  Search::optimize would catch this mistake.
*/
static struct Output *output_new(struct Keyword_List *head, u8 *struct_decl,
				u32 struct_decl_lineno, u8 *return_type,
				u8 *struct_tag, u8 *verbatim_declarations,
				u8 *verbatim_declarations_end,
				u32 verbatim_declarations_lineno,
				u8 *verbatim_code, u8 *verbatim_code_end,
				u32 verbatim_code_lineno, bool charset_dependent,
				s32 total_keys, s32 max_key_len, s32 min_key_len,
				bool hash_includes_len, struct Positions *positions,
				u32 *alpha_inc, s32 total_duplicates,
				u32 alpha_size, s32 *asso_values)
{
	struct Output *t;

	t = calloc(1, sizeof(*t));
	t->head = head;
	t->struct_decl = struct_decl;
	t->struct_decl_lineno = struct_decl_lineno;
	t->return_type = return_type;
	t->struct_tag = struct_tag;
	t->verbatim_declarations = verbatim_declarations;
	t->verbatim_declarations_end = verbatim_declarations_end;
	t->verbatim_declarations_lineno = verbatim_declarations_lineno;
	t->verbatim_code = verbatim_code;
	t->verbatim_code_end = verbatim_code_end;
	t->verbatim_code_lineno = verbatim_code_lineno;
	t->charset_dependent = charset_dependent;	
	t->total_keys = total_keys;
	t->max_key_len = max_key_len;
	t->min_key_len = min_key_len;
	t->hash_includes_len = hash_includes_len;
	t->key_positions = pos_new_cpy(positions);
	t->alpha_inc = alpha_inc;
	t->total_duplicates = total_duplicates;
	t->alpha_size = alpha_size;
	t->asso_values = asso_values;
	return t;
}/*}}}*/
/*{{{ output_del */
static void output_del(struct Output *t)
{
	pos_del(t->key_positions);
	free(t);
}
/*}}}*/
/*{{{ output_do */
/* generates the hash function and the key word recognizer function based upon the user's Options */
static void output_do(struct Output *t)
{
	output_compute_min_max(t);
	if (OPTS(CPLUSPLUS)) /* yeah, we know nowadays that c++ is never a good idea anyway */
		/*
		 * The 'register' keyword is removed from C++17. See
		 * http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2015/n4340
		 */
		register_scs = "";
	else
		register_scs = "register ";
	if (OPTS(C) || OPTS(ANSIC) || OPTS(CPLUSPLUS)) {
		const_always = "const ";
		const_readonly_array = (OPTS(CONST) ? "const " : "");
		const_for_struct = ((OPTS(CONST) && OPTS(TYPE)) ? "const " : "" );
	} else {
		const_always = "";
		const_readonly_array = "";
		const_for_struct = "";
	}
	if (!OPTS(TYPE)) {
		t->return_type = (const_always[0] != 0 ? "const char *" : "char *");
		t->struct_tag = (const_always[0] != 0 ? "const char *" : "char *");
	}
	t->wordlist_eltype = (OPTS(SHAREDLIB) && !OPTS(TYPE) ? (u8*)"int" : t->struct_tag);
	printf ("/* ");
	if (OPTS(KRC))
		printf("KR-C");
	else if (OPTS(C))
		printf("C");
	else if (OPTS(ANSIC))
		printf("ANSI-C");
	else if (OPTS(CPLUSPLUS))
		printf("C++");
	printf(" code produced by gperf version %s */\n", cgperf_version_string);
	opts_print(options);
	printf("\n");
	if (!OPTS(POSITIONS)) {
		printf ("/* Computed positions: -k'");
		pos_print(t->key_positions);
		printf("' */\n");
	}
	printf("\n");
	if (t->charset_dependent && (t->key_positions->size > 0 || OPTS(UPPERLOWER))) {
		printf("#if !((' ' == 32) && ('!' == 33) && ('\"' == 34) && ('#' == 35) \\\n"
		       "      && ('%%' == 37) && ('&' == 38) && ('\\'' == 39) && ('(' == 40) \\\n"
		       "      && (')' == 41) && ('*' == 42) && ('+' == 43) && (',' == 44) \\\n"
		       "      && ('-' == 45) && ('.' == 46) && ('/' == 47) && ('0' == 48) \\\n"
		       "      && ('1' == 49) && ('2' == 50) && ('3' == 51) && ('4' == 52) \\\n"
		       "      && ('5' == 53) && ('6' == 54) && ('7' == 55) && ('8' == 56) \\\n"
		       "      && ('9' == 57) && (':' == 58) && (';' == 59) && ('<' == 60) \\\n"
		       "      && ('=' == 61) && ('>' == 62) && ('?' == 63) && ('A' == 65) \\\n"
		       "      && ('B' == 66) && ('C' == 67) && ('D' == 68) && ('E' == 69) \\\n"
		       "      && ('F' == 70) && ('G' == 71) && ('H' == 72) && ('I' == 73) \\\n"
		       "      && ('J' == 74) && ('K' == 75) && ('L' == 76) && ('M' == 77) \\\n"
		       "      && ('N' == 78) && ('O' == 79) && ('P' == 80) && ('Q' == 81) \\\n"
		       "      && ('R' == 82) && ('S' == 83) && ('T' == 84) && ('U' == 85) \\\n"
		       "      && ('V' == 86) && ('W' == 87) && ('X' == 88) && ('Y' == 89) \\\n"
		       "      && ('Z' == 90) && ('[' == 91) && ('\\\\' == 92) && (']' == 93) \\\n"
		       "      && ('^' == 94) && ('_' == 95) && ('a' == 97) && ('b' == 98) \\\n"
		       "      && ('c' == 99) && ('d' == 100) && ('e' == 101) && ('f' == 102) \\\n"
		       "      && ('g' == 103) && ('h' == 104) && ('i' == 105) && ('j' == 106) \\\n"
		       "      && ('k' == 107) && ('l' == 108) && ('m' == 109) && ('n' == 110) \\\n"
		       "      && ('o' == 111) && ('p' == 112) && ('q' == 113) && ('r' == 114) \\\n"
		       "      && ('s' == 115) && ('t' == 116) && ('u' == 117) && ('v' == 118) \\\n"
		       "      && ('w' == 119) && ('x' == 120) && ('y' == 121) && ('z' == 122) \\\n"
		       "      && ('{' == 123) && ('|' == 124) && ('}' == 125) && ('~' == 126))\n"
		       "/* The character set is not based on ISO-646.  */\n");
		printf("%s \"gperf generated tables don't work with this execution character set. Please report a bug to <bug-gperf@gnu.org>.\"\n", OPTS(KRC) || OPTS(C) ? "error" : "#error");
		printf ("#endif\n\n");
	}
	if (t->verbatim_declarations < t->verbatim_declarations_end) {
		output_line_directive(t->verbatim_declarations_lineno);
		fwrite(t->verbatim_declarations, 1, t->verbatim_declarations_end -
								t->verbatim_declarations, stdout);
	}
	if (OPTS(TYPE) && !OPTS(NOTYPE)) {
		/* output type declaration now, reference it later on.... */
		output_line_directive(t->struct_decl_lineno);
		printf("%s\n", t->struct_decl);
	}
	if (OPTS(INCLUDE))
		printf("#include <string.h>\n"); /* declare strlen(), strcmp(), strncmp() */
	if (!OPTS(ENUM)) /* refactored: overzealous code factorization */
		output_constants_defines(t);
	else if (OPTS(GLOBAL))
		output_constants_enum(t, "");
	printf("/* maximum key range = %d, duplicates = %d */\n\n", t->max_hash_value - t->min_hash_value + 1, t->total_duplicates);
	if (OPTS(UPPERLOWER)) {
		#if USE_DOWNCASE_TABLE
		output_upperlower_table();
		#endif
		if (OPTS(LENTABLE))
			output_upperlower_memcmp();
		else {
			if (OPTS(COMP))
				output_upperlower_strncmp();
			else
				output_upperlower_strcmp();
		}
	}
	if (OPTS(CPLUSPLUS))
		printf(
"class %s\n"
"{\n"
"private:\n"
"  static inline unsigned int %s (const char *str, size_t len);\n"
"public:\n"
"  static %s%s%s (const char *str, size_t len);\n"
"};\n"
"\n",			options->class_name, options->hash_name, const_for_struct, t->return_type,
			options->function_name);
	output_hash_function(t);
	if (OPTS(SHAREDLIB) && (OPTS(GLOBAL) || OPTS(TYPE)))
		output_lookup_pools(t);
	if (OPTS(GLOBAL))
		output_lookup_tables(t);
	output_lookup_function(t);
	if (t->verbatim_code < t->verbatim_code_end) {
		output_line_directive(t->verbatim_code_lineno);
		fwrite(t->verbatim_code, 1, t->verbatim_code_end - t->verbatim_code, stdout);
	}
	fflush(stdout);
}/*}}}*/
/*{{{ output_compute_min_max */
static void output_compute_min_max(struct Output *t)
{
	struct Keyword_List *tmp;
	/*
	 * since the list is already sorted by hash value all we need to do is to look at the first
	 * and the last element of the list
	 */
	t->min_hash_value = t->head->kw->hash_value;
	tmp = t->head;
	loop {
		if (tmp->next == 0)
			break;
		tmp = tmp->next;
	}
	t->max_hash_value = tmp->kw->hash_value;
}/*}}}*/
/*{{{ output_constants_defines */
static void output_constants_defines(struct Output *t)
{
	printf("\n");
	output_constant_define("TOTAL_KEYWORDS", t->total_keys);
	output_constant_define("MIN_WORD_LENGTH", t->min_key_len);
	output_constant_define("MAX_WORD_LENGTH", t->max_key_len);
	output_constant_define("MIN_HASH_VALUE", t->min_hash_value);
	output_constant_define("MAX_HASH_VALUE", t->max_hash_value);
}/*}}}*/
/*{{{ output_constants_enum */
static void output_constants_enum(struct Output *t, u8 *indentation)
{
	bool pending_comma;

	printf("%senum\n"
          "%s  {\n", indentation, indentation);
	pending_comma = false;
	output_constant_enum("TOTAL_KEYWORDS", t->total_keys, indentation, &pending_comma);
	output_constant_enum("MIN_WORD_LENGTH", t->min_key_len, indentation, &pending_comma);
	output_constant_enum("MAX_WORD_LENGTH", t->max_key_len, indentation, &pending_comma);
	output_constant_enum("MIN_HASH_VALUE", t->min_hash_value, indentation, &pending_comma);
	output_constant_enum("MAX_HASH_VALUE", t->max_hash_value, indentation, &pending_comma);
	if (pending_comma)
		printf("\n");
	printf("%s  };\n\n", indentation);
}/*}}}*/
/*{{{ output_hash_function */
/* Generates C code for the hash function that returns the
   proper encoding for each keyword.
   The hash function has the signature
     unsigned int <hash> (const char *str, size_t len).  */
static void output_hash_function(struct Output *t)
{
	/* output the function's head */
	if (OPTS(CPLUSPLUS))
		printf("inline ");
	else if (OPTS(KRC) || OPTS(C) || OPTS(ANSIC))
		printf(
"#ifdef __GNUC__\n"
"__inline\n"
"#else\n"
"#ifdef __cplusplus\n"
"inline\n"
"#endif\n"
"#endif\n");
	if (/* the function does not use the 'str' argument? */
		(t->key_positions->size == 0)
		|| /* the function uses 'str', but not the 'len' argument? */
			(!t->hash_includes_len
			 && t->key_positions->positions[0] < t->min_key_len)
			 && t->key_positions->positions[t->key_positions->size - 1] != POS_LASTCHAR)
		/* pacify lint */
		printf("/*ARGSUSED*/\n");
	if (OPTS(KRC) || OPTS(C) || OPTS(ANSIC))
		printf("static ");
	printf("unsigned int\n");
	if (OPTS(CPLUSPLUS))
		printf("%s::", options->class_name);
	printf("%s ", options->hash_name);
 	printf(OPTS(KRC) ?
"(str, len)\n"
"     %schar *str;\n"
"     %ssize_t len;\n" :
		OPTS(C) ?
"(str, len)\n"
"     %sconst char *str;\n"
"     %ssize_t len;\n" :
		OPTS(ANSIC) || OPTS(CPLUSPLUS) ?
"(%sconst char *str, %ssize_t len)\n" :
"", 		register_scs, register_scs);

	/*
	 * note that when the hash function is called, it has already been verified that
	 * min_key_len <= len <= max_key_len
	 */
	/* output the function's body */
	printf(
"{\n");
	/* first the asso_values array */
	if (t->key_positions->size > 0) {
		s32 columns;
		s32 field_width;
		s32 trunc;
		u32 count;
		/*
		 * the values in the asso_values array are all unsigned integers <= MAX_HASH_VALUE +
		 * 1
		 */
		printf(
"  static %s%s asso_values[] =\n"
"    {",	const_readonly_array, smallest_integral_type(t->max_hash_value + 1));
		columns = 10;
		/* calculate maximum number of digits required for MAX_HASH_VALUE + 1 */
		field_width = 2;
		trunc = t->max_hash_value + 1;
		loop {
			trunc /= 10;
			if (trunc <= 0)
				break;
			++field_width;
		}
		count = 0;
		loop {
			if (count >= t->alpha_size)
				break;
			if (count > 0)
				printf(",");
			if ((count % columns) == 0)
				printf("\n     ");
			printf("%*d", field_width, t->asso_values[count]);
			++count;
		}
		printf(
"\n"
"    };\n");
	}
	if (t->key_positions->size == 0) {
		/* trivial case: No key positions at all */
		printf(
"  return %s;\n", t->hash_includes_len ? "len" : "0");
	} else {
		struct PositionIterator *iter;
		s32 key_pos;
		/*
		 * Iterate through the key positions. Remember that Positions::sort() has sorted
		 * them in decreasing order, with Positions::LASTCHAR coming last.
		 */
		iter = pos_iterator(t->key_positions, t->max_key_len);
		/* get the highest key position */
		key_pos = positer_next(iter);
		if (key_pos == POS_LASTCHAR || key_pos < t->min_key_len) {
			/*
			 * We can perform additional optimizations here: Write it out as a single
			 * expression. Note that the values are added as 'int's even though the
			 * asso_values array may contain 'unsigned char's or 'unsigned short's.
			 */
			printf(
"  return %s",			t->hash_includes_len ? "len + " : "");
			if (t->key_positions->size == 2
				&& t->key_positions->positions[0] == 0
				&& t->key_positions->positions[1] == POS_LASTCHAR) {
				/* optimize special case of "-k 1,$" */
				output_asso_values_ref(t, POS_LASTCHAR);
				printf(" + ");
				output_asso_values_ref(t, 0);
			} else {
				loop {
					if (key_pos == POS_LASTCHAR)
						break;
					output_asso_values_ref(t, key_pos);
					key_pos = positer_next(iter);
					if (key_pos != POSITER_EOS)
						printf(" + ");
					else
						break;
				}
				if (key_pos == POS_LASTCHAR)
					output_asso_values_ref(t, POS_LASTCHAR);
			}
			printf(";\n");
		} else {
			u8 *fallthrough_marker;
			/* we've got to use the correct, but brute force, technique */
			/*
			 * pseudo-statement or comment that avoids a compiler warning or lint
			 * warning
			 */
			fallthrough_marker =
"#if defined __cplusplus && (__cplusplus >= 201703L || (__cplusplus >= 201103L && defined __clang_major__ && defined __clang_minor__ && __clang_major__ + (__clang_minor__ >= 9) > 3))\n"
"      [[fallthrough]];\n"
"#elif defined __GNUC__ && __GNUC__ >= 7\n"
"      __attribute__ ((__fallthrough__));\n"
"#endif\n"
"      /*FALLTHROUGH*/\n";
			/*
			 * it doesn't really matter whether hval is an 'int' or 'unsigned int', but
			 * 'unsigned int' gives fewer warnings
			 */
			printf(
"  %sunsigned int hval = %s;\n\n"
"  switch (%s)\n"
"    {\n"
"      default:\n",				register_scs, t->hash_includes_len ? "len" : "0",
							t->hash_includes_len ? "hval" : "len");
			loop {
				if (key_pos == POS_LASTCHAR || key_pos < t->max_key_len)
					break;
				key_pos = positer_next(iter);
				if (key_pos == POSITER_EOS)
					break;
			}
			if (key_pos != POSITER_EOS && key_pos != POS_LASTCHAR) {
				s32 i;

				i = key_pos;
				loop {
					if (i > key_pos)
						printf("%s", fallthrough_marker);
					loop {
						if (i <= key_pos)
							break;
						printf("      case %d:\n", i);
						i--;
					}
					printf("        hval += ");
					output_asso_values_ref(t, key_pos);
					printf(";\n");
					key_pos = positer_next(iter);
					if (key_pos == POSITER_EOS || key_pos == POS_LASTCHAR)
						break;
				}
				if (i >= t->min_key_len)
					printf("%s", fallthrough_marker);
				loop {
					if (i < t->min_key_len)
						break;
					printf("      case %d:\n", i);
					i--;
				}
			}
			printf(
"        break;\n"
"    }\n"
"  return hval");
			if (key_pos == POS_LASTCHAR) {
				printf(" + ");
				output_asso_values_ref(t, POS_LASTCHAR);
			}
			printf (";\n");
		}
		positer_del(iter);
	}
	printf ("}\n\n");
}/*}}}*/
/*{{{ output_asso_values_ref */
/* Generates a C expression for an asso_values[] reference.  */
static void output_asso_values_ref(struct Output *t, s32 pos)
{
	printf("asso_values[");
	/*
	 * Always cast to unsigned char. This is necessary when the alpha_inc is nonzero, and also
	 * avoids a gcc warning "subscript has type 'char'".
	 */
	if (OPTS(CPLUSPLUS)) {
		/*
		 * In C++, a C style cast may lead to a 'warning: use of old-style cast'.
		 * Therefore prefer the C++ style cast syntax.
		 */
		printf("static_cast<unsigned char>(");
		output_asso_values_index(t, pos);
		printf(")");
	} else {
		printf("(unsigned char)");
		output_asso_values_index(t, pos);
	}
	printf("]");
}/*}}}*/
/*{{{ output_asso_values_index */
/* generates a C expression for an asso_values[] index */
static void output_asso_values_index(struct Output *t, s32 pos)
{
	if (pos == POS_LASTCHAR)
		printf("str[len - 1]");
	else {
		printf("str[%d]", pos);
		if (t->alpha_inc[pos])
			printf("+%u", t->alpha_inc[pos]);
	}
}/*}}}*/
/*{{{ output_lookup_pools */
/* generate all pools needed for the lookup function */
static void output_lookup_pools(struct Output *t)
{
	if (OPTS(SWITCH)) {
		if (OPTS(TYPE) || (OPTS(DUP) && t->total_duplicates > 0))
			output_string_pool(t);
	} else
		output_string_pool(t);
}/*}}}*/
/*{{{ output_string_pool */
/*
 * Prints out the string pool, containing the strings of the keyword table.
 * Only called if option[SHAREDLIB]
 */
static void output_string_pool(struct Output *t)
{
	u8 *indent;
	s32 index;
	struct Keyword_List *tmp;

	indent = OPTS(TYPE) || OPTS(GLOBAL) ? "" : "  ";

	printf(
"%sstruct %s_t\n"
"%s  {\n",	indent, options->stringpool_name, indent);
	tmp = t->head;
	index = 0;
	loop {
		struct Keyword *kw;

		if (tmp == 0)
			break;
		kw = tmp->kw;		
		/*
		 * If generating a switch statement, and there is no user defined type, we generate
		 * non-duplicates directly in the code. Only duplicates go into the table.
		 */
		if (OPTS(SWITCH) && !OPTS(TYPE) && kw->duplicate_link == 0)
			continue;
		if (!OPTS(SWITCH) && !OPTS(DUP))
			index = kw->hash_value;
		printf("%s    char %s_str%d[sizeof(", indent, options->stringpool_name, index);
		output_string(kw->allchars, kw->allchars_length);
		printf(")];\n");
		/* deal with duplicates specially */
		if (kw->duplicate_link) {/* implies option[DUP] */
			struct Keyword *links;

			links = kw->duplicate_link;
			loop {
				if (links == 0)
					break;
				if (!(links->allchars_length == kw->allchars_length
							&& memcmp(links->allchars, kw->allchars,
								kw->allchars_length) == 0)) {
					++index;
					printf("%s    char %s_str%d[sizeof(", indent,
								options->stringpool_name, index);
					output_string(links->allchars, links->allchars_length);
					printf(")];\n");
				}	
				links = links->duplicate_link;
			}
		}
		++index;
		tmp = tmp->next; 
	}
	printf(
"%s  };\n",	indent);
	printf(
"%sstatic %sstruct %s_t %s_contents =\n"
"%s  {\n", indent, const_readonly_array, options->stringpool_name, options->stringpool_name,
											indent);
	tmp = t->head;
	index = 0;
	loop {
		struct Keyword *kw;

		if (tmp == 0)
			break;
		kw = tmp->kw;
		/*
		 * If generating a switch statement, and there is no user defined type, we generate
		 * non-duplicates directly in the code. Only duplicates go into the table.
		 */
		if (OPTS(SWITCH) && !OPTS(TYPE) && kw->duplicate_link == 0)
			continue;
		if (index > 0)
			printf(",\n");

		if (!OPTS(SWITCH) && !OPTS(DUP))
			index = kw->hash_value;
		printf(
"%s    ",		indent);
		output_string(kw->allchars, kw->allchars_length);
		/* deal with duplicates specially */
		if (kw->duplicate_link != 0) {/* implies option[DUP] */
			struct Keyword *links;

			links = kw->duplicate_link;
			loop {
				if (links == 0)
					break;
				if (!(links->allchars_length == kw->allchars_length
							&& memcmp(links->allchars, kw->allchars,
								kw->allchars_length) == 0)) {
					++index;
					printf(",\n");
					printf(
"%s    ",					indent);
					output_string(links->allchars, links->allchars_length);
				}
				links = links->duplicate_link;
			}
		}
		++index;
		tmp = tmp->next; 
	}
	if (index > 0)
		printf("\n");
	printf(
"%s  };\n",	indent);
	printf(
"%s#define %s ((%schar *) &%s_contents)\n", indent, options->stringpool_name, const_always,
									options->stringpool_name);
	if (OPTS(GLOBAL))
		printf(
"\n");
}/*}}}*/
/*{{{ output_lookup_tables */
/* generate all the tables needed for the lookup function */
static void output_lookup_tables(struct Output *t)
{
	if (OPTS(SWITCH)) {
		/* use the switch in place of lookup table */
		if (OPTS(LENTABLE) && (OPTS(DUP) && t->total_duplicates > 0))
			output_keylength_table(t);
		if (OPTS(TYPE) || (OPTS(DUP) && t->total_duplicates > 0))
			output_keyword_table(t);
	} else {
		/* use the lookup table, in place of switch */
		if (OPTS(LENTABLE))
			output_keylength_table(t);
		output_keyword_table(t);
		output_lookup_array(t);
	}
}/*}}}*/
/*{{{ output_keylength_table */
/*
 * Prints out a table of keyword lengths, for use with the comparison code in generated function
 * 'in_word_set'.  Only called if option[LENTABLE].
 */
static void output_keylength_table(struct Output *t)
{
	s32 columns;
	u8 *indent;
	s32 index;
	s32 column;
	struct Keyword_List *tmp;

	columns = 14;
	indent = OPTS(GLOBAL) ? "" : "  ";

	printf(
"%sstatic %s%s %s[] =\n"
"%s  {",	indent, const_readonly_array, smallest_integral_type(t->max_key_len),
								options->lengthtable_name, indent);
	column = 0;
	tmp = t->head;
	index = 0;
	loop {
		struct Keyword *kw;

		if (tmp == 0)
			break;
		kw = tmp->kw;
		/*
		 * If generating a switch statement, and there is no user defined type, we generate
		 * non-duplicates directly in the code. Only duplicates go into the table.
		 */
		if (OPTS(SWITCH) && !OPTS(TYPE) && kw->duplicate_link == 0)
			continue;
		if (index < kw->hash_value && !OPTS(SWITCH) && !OPTS(DUP)) {
			/* some blank entries */
			loop {
				if (index >= kw->hash_value)
					break;
				if (index > 0)
					printf(",");
				if ((column % columns) == 0)
					printf(
"\n%s   ",					indent);
				++column;
				printf("%3d", 0);
				++index;
			}
		}
		if (index > 0)
			printf(",");
		if ((column % columns) == 0)
			printf(
"\n%s   ",			indent);
		++column;
		printf("%3d", kw->allchars_length);
		++index;
		/* deal with duplicates specially */
		if (kw->duplicate_link != 0) {
			struct Keyword *links;

			links = kw->duplicate_link;
			loop {
				if (links == 0)
					break;
				printf(",");
				if ((column % columns) == 0)
					printf(
"\n%s   ",						indent);
				++column;
				printf("%3d", links->allchars_length);
				++index;
				links = links->duplicate_link;	
			}
		}
		tmp = tmp->next;
	}
	printf(
"\n%s  };\n",	indent);
	if (OPTS(GLOBAL))
		printf(
"\n");
}/*}}}*/
/*{{{ output_keyword_table */
/* prints out the array containing the keywords for the hash function */
static void output_keyword_table(struct Output *t)
{
	u8 *indent;
	s32 index;
	struct Keyword_List *tmp;

	indent  = OPTS(GLOBAL) ? "" : "  ";
	printf(
"%sstatic ",	indent);
	output_const_type(const_readonly_array, t->wordlist_eltype);
	printf("%s[] =\n"
"%s  {\n",	options->wordlist_name, indent);
	/* generate an array of reserved words at appropriate locations */
	tmp = t->head;
	index = 0;
	loop {
		struct Keyword *kw;

		if (tmp == 0)
			break;
		kw = tmp->kw;
		/*
		 * If generating a switch statement, and there is no user defined type, we generate
		 * non-duplicates directly in the code. Only duplicates go into the table.
		 */
		if (OPTS(SWITCH) && !OPTS(TYPE) && kw->duplicate_link == 0)
			continue;
		if (index > 0)
			printf(",\n");
		if (index < kw->hash_value && !OPTS(SWITCH) && !OPTS(DUP)) {
			/* some blank entries */
			output_keyword_blank_entries(kw->hash_value - index, indent);
			printf(",\n");
			index = kw->hash_value;
		}
		kw->final_index = index;
		output_keyword_entry(kw, index, indent, false);
		/* deal with duplicates specially */
		if (kw->duplicate_link != 0) { /* implies option[DUP] */
			struct Keyword *links;

			links = kw->duplicate_link;
			loop {
				s32 stringpool_index;

				if (links == 0)
					break;
				++index;
				links->final_index = index;
				printf(",\n");
				stringpool_index =
					(links->allchars_length == kw->allchars_length
						&& memcmp(links->allchars, kw->allchars,
									kw->allchars_length) == 0
					? kw->final_index : links->final_index);
				output_keyword_entry(links, stringpool_index, indent, true);
				links = links->duplicate_link;
			}
		}
		++index;
		tmp = tmp->next;
	}
	if (index > 0)
		printf("\n");
	printf(
"%s  };\n\n",	indent);
}/*}}}*/
/*{{{ output_lookup_array */
/*
 * generates the large, sparse table that maps hash values into the smaller, contiguous range of the
 * keyword table
 */
static void output_lookup_array(struct Output *t)
{
	s32 DEFAULT_VALUE;
	struct Duplicate_Entry *duplicates;
	s32 *lookup_array;
	s32 lookup_array_size;
	struct Duplicate_Entry *dup_ptr;
	s32 *lookup_ptr;
	struct Keyword_List *tmp;
	s32 min;
	s32 max;
	u8 *indent;
	s32 field_width;
	s32 columns;
	s32 column;
	s32 i;
	if (!OPTS(DUP))
		return;

	DEFAULT_VALUE = -1;

	duplicates = calloc(t->total_duplicates, sizeof(*duplicates)); 
	lookup_array = calloc(t->max_hash_value + 1 + 2 * t->total_duplicates,
								sizeof(*lookup_array));
	lookup_array_size = t->max_hash_value + 1;
	dup_ptr = &duplicates[0];
	lookup_ptr = &lookup_array[t->max_hash_value + 1 + 2 * t->total_duplicates];

	loop {
		if (lookup_ptr <= lookup_array)
			break;
		*--lookup_ptr = DEFAULT_VALUE;
	}
	/* now dup_ptr = &duplicates[0] and lookup_ptr = &lookup_array[0] */
	tmp = t->head;
	loop {
		s32 hash_value;

		if (tmp == 0)
			break;
		hash_value = tmp->kw->hash_value;
		lookup_array[hash_value] = tmp->kw->final_index;
		if (OPTS(DEBUG))
			fprintf(stderr, "keyword = %.*s, index = %d\n", tmp->kw->allchars_length, tmp->kw->allchars, tmp->kw->final_index);
		if (tmp->kw->duplicate_link != 0) {
			struct Keyword *ptr;

			/* start a duplicate entry */
			dup_ptr->hash_value = hash_value;
			dup_ptr->index = tmp->kw->final_index;
			dup_ptr->count = 1;

			ptr = tmp->kw->duplicate_link;
			loop {
				if (ptr != 0)
					break;
				++(dup_ptr->count);
				if (OPTS(DEBUG))
					fprintf(stderr, "static linked keyword = %.*s, index = %d\n", ptr->allchars_length, ptr->allchars, ptr->final_index);
				ptr = ptr->duplicate_link;
			}
			++dup_ptr;
		}
		tmp = tmp->next;
	}
	loop {
		s32 i;

		if (dup_ptr <= duplicates)
			break;
		dup_ptr--;
		if (OPTS(DEBUG))
			fprintf(stderr, "dup_ptr[%lu]: hash_value = %d, index = %d, count = %d\n", (unsigned long)(dup_ptr - duplicates), dup_ptr->hash_value, dup_ptr->index, dup_ptr->count);
		/*
		 * start searching for available space towards the right part of the lookup
		 * array
		 */
		i = dup_ptr->hash_value;
		loop {
			if (i >= lookup_array_size - 1)
				break;
			if (lookup_array[i] == DEFAULT_VALUE && lookup_array[i + 1]
										== DEFAULT_VALUE)
				goto found_i;
			++i;
		}
		/* if we didn't find it to the right look to the left instead... */
		i = dup_ptr->hash_value - 1;
		loop {
			if (i < 0)
				break;
			if (lookup_array[i] == DEFAULT_VALUE && lookup_array[i + 1]
									== DEFAULT_VALUE)
				goto found_i;
			i--;
		}
		/* append to the end of lookup_array */
		i = lookup_array_size;
		lookup_array_size += 2;
found_i:
		/*
		 * Put in an indirection from dup_ptr->_hash_value to i.
		 * At i and i+1 store dup_ptr->_final_index and dup_ptr->count.
		 */
		lookup_array[dup_ptr->hash_value] = - 1 - t->total_keys - i;
		lookup_array[i] = - t->total_keys + dup_ptr->index;
		lookup_array[i + 1] = - dup_ptr->count;
		/* All these three values are <= -2, distinct from DEFAULT_VALUE */
	}
	/* the values of the lookup array are now known */
	min = S32_MAX;
	max = S32_MIN;
	lookup_ptr = lookup_array + lookup_array_size;
	loop {
		s32 val;

		if (lookup_ptr <= lookup_array)
			break;
		val = *--lookup_ptr;
		if (min > val)
			min = val;
		if (max < val)
			max = val;
	}
	indent = OPTS(GLOBAL) ? "" : "  ";
	printf(
"%sstatic %s%s lookup[] =\n"
"%s  {",		indent, const_readonly_array, smallest_integral_type_2(min, max), indent);
	/* calculate maximum number of digits required for MIN..MAX */
	{
		s32 trunc;

		field_width = 2;
		trunc = max;
		loop {			
			trunc /= 10;
			if (trunc <= 0)
				break;
			++field_width;
		}
	}
	if (min < 0) {
		s32 neg_field_width;
		s32 trunc;

		neg_field_width = 2;
		trunc = -min;
		loop {
			trunc /= 10;
			if (trunc <= 0)
				break;
			++neg_field_width;
		}
		++neg_field_width; /* account for the minus sign */
		if (field_width < neg_field_width)
			field_width = neg_field_width;
	}
	columns = 42 / field_width;
	column = 0;
	i = 0;
	loop {
		if (i >= lookup_array_size)
			break;
		if (i > 0)
			printf(",");
		if ((column % columns) == 0)
			printf("\n%s   ", indent);
		++column;
		printf("%*d", field_width, lookup_array[i]);
		++i;
	}
	printf(
"\n%s  };\n\n",		indent);
	free(duplicates);
	free(lookup_array);
}/*}}}*/
/*{{{ output_lookup_function */
/* generates C code for the lookup function */
static void output_lookup_function(struct Output *t)
{
	/* output the function's head */
	/*
	 * We don't declare the lookup function 'static' because we cannot make assumptions about
	 * the compilation units of the user.
	 * Since we don't make it 'static', it makes no sense to declare it 'inline', because
	 * non-static inline functions must not reference static functions or variables, see ISO C
	 * 99 section 6.7.4.(3).
	 */
	printf(
"%s%s\n",	const_for_struct, t->return_type);
	if (OPTS(CPLUSPLUS))
		printf(
"%s::",			options->class_name);
	printf("%s ", options->function_name);
	printf(
		OPTS(KRC) ? "(str, len)\n"
"     %schar *str;\n"
"     %ssize_t len;\n" :
		OPTS(C) ? "(str, len)\n"
"     %sconst char *str;\n"
"     %ssize_t len;\n" :
		OPTS(ANSIC) || OPTS(CPLUSPLUS) ?  "(%sconst char *str, %ssize_t len)\n" :
"",		register_scs, register_scs);

	/* output the function's body */
	printf(
"{\n");
	if (OPTS(ENUM) && !OPTS(GLOBAL))
		output_constants_enum(t, "  ");
	if (OPTS(SHAREDLIB) && !(OPTS(GLOBAL) || OPTS(TYPE)))
		output_lookup_pools(t);
	if (!OPTS(GLOBAL))
		output_lookup_tables(t);
	if (OPTS(LENTABLE))
		output_lookup_function_body(t, output_comparison_memcmp);
	else {
		if (OPTS(COMP))
			output_lookup_function_body(t, output_comparison_strncmp);
		else
			output_lookup_function_body(t, output_comparison_strcmp);
	}
	printf(
"}\n");
}/*}}}*/
/*{{{  output_lookup_function_body */
static void output_lookup_function_body(struct Output *t,
						void (*output_comparison)(u8 *expr1, u8 *expr2))
{
	printf(
"  if (len <= %sMAX_WORD_LENGTH && len >= %sMIN_WORD_LENGTH)\n"
"    {\n"
"      %sunsigned int key = %s (str, len);\n\n", options->constants_prefix, 
				options->constants_prefix, register_scs, options->hash_name);
	if (OPTS(SWITCH)) {
		s32 switch_size;
		s32 num_switches;

		switch_size = output_num_hash_values(t);
		num_switches = options->total_switches;
		if (num_switches > switch_size)
			num_switches = switch_size;
		printf(
"      if (key <= %sMAX_HASH_VALUE", options->constants_prefix);
		if (t->min_hash_value > 0)
			printf(
" && key >= %sMIN_HASH_VALUE", options->constants_prefix);
		printf (
")\n"
"        {\n");
		if (OPTS(DUP) && t->total_duplicates > 0) {
			if (OPTS(LENTABLE))
				printf(
"          %s%s%s *lengthptr;\n", register_scs, const_always, smallest_integral_type(
										t->max_key_len));
			printf(
"          %s",			register_scs);
			output_const_type(const_readonly_array, t->wordlist_eltype);
			printf("*wordptr;\n");
			printf(
"          %s",			register_scs);
			output_const_type(const_readonly_array, t->wordlist_eltype);
			printf("*wordendptr;\n");
		}
		if (OPTS(TYPE)) {
			printf(
"          %s",			register_scs);
			output_const_type(const_readonly_array, t->struct_tag);
			printf("*resword;\n\n");
		} else 
			printf(
"          %s%sresword;\n\n", register_scs, t->struct_tag);
		output_switches(t->head, num_switches, switch_size, t->min_hash_value,
									t->max_hash_value, 10);
		printf(
"          return 0;\n");
		if (OPTS(DUP) && t->total_duplicates > 0) {
			s32 indent;

			indent = 8;
			printf(
"%*smulticompare:\n"
"%*s  while (wordptr < wordendptr)\n"
"%*s    {\n",			indent, "", indent, "", indent, "");
			if (OPTS(LENTABLE)) {
				printf(
"%*s      if (len == *lengthptr)\n"
"%*s        {\n",			indent, "", indent, "");
				indent += 4;
			}
			printf(
"%*s      %s%schar *s = ", indent, "", register_scs, const_always);
			if (OPTS(TYPE))
				printf("wordptr->%s", options->slot_name);
			else
				printf("*wordptr");
			if (OPTS(SHAREDLIB))
				printf(" + %s", options->stringpool_name);
			printf(";\n\n"
"%*s      if (",		indent, "");
			output_comparison("str", "s");
			printf(")\n"
"%*s        return %s;\n", indent, "", OPTS(TYPE) ? "wordptr" : "s");
			if (OPTS(LENTABLE)) {
				indent -= 4;
				printf(
"%*s        }\n",			indent, "");
			}
			if (OPTS(LENTABLE))
				printf(
"%*s      lengthptr++;\n",		indent, "");
			printf(
"%*s      wordptr++;\n"
"%*s    }\n"
"%*s  return 0;\n",		indent, "", indent, "", indent, "");
		}
      		printf(
"        compare:\n");
		if (OPTS(TYPE)) {
			printf(
"          {\n"
"            %s%schar *s = resword->%s", register_scs, const_always, options->slot_name);
			if (OPTS(SHAREDLIB))
				printf(" + %s", options->stringpool_name);
				printf(";\n\n"
"            if (");
			output_comparison("str", "s");
			printf(
")\n"
"              return resword;\n"
"          }\n");
		} else {
			output_comparison("str", "resword");
			printf(
")\n"
"            return resword;\n");
		}
		printf(
"        }\n");
	} else {
		printf(
"      if (key <= %sMAX_HASH_VALUE)\n", options->constants_prefix);
		if (OPTS(DUP)) {
			s32 indent;

			indent = 8;
			printf(
"%*s{\n"
"%*s  %sint index = lookup[key];\n\n"
"%*s  if (index >= 0)\n",	indent, "", indent, "", register_scs, indent, "");
			if (OPTS(LENTABLE)) {
				printf(
"%*s    {\n"
"%*s      if (len == %s[index])\n",	indent, "", indent, "", options->lengthtable_name);
				indent += 4;
			}
			printf(
"%*s    {\n"
"%*s      %s%schar *s = %s[index]",	indent, "", indent, "", register_scs, const_always,
								options->wordlist_name);
			if (OPTS(TYPE))
				printf(".%s", options->slot_name);
			if (OPTS(SHAREDLIB))
				printf (" + %s", options->stringpool_name);
			printf(";\n\n"
"%*s      if (",		indent, "");
			output_comparison("str", "s");
			printf (")\n"
"%*s        return ",		indent, "");
			if (OPTS(TYPE))
				printf("&%s[index]", options->wordlist_name);
			else
				printf("s");
			printf(";\n"
"%*s    }\n",			indent, "");
			if (OPTS(LENTABLE)) {
				indent -= 4;
				printf(
"%*s    }\n",				indent, "");
			}
			if (t->total_duplicates > 0) {
				printf(
"%*s  else if (index < -%sTOTAL_KEYWORDS)\n"
"%*s    {\n"
"%*s      %sint offset = - 1 - %sTOTAL_KEYWORDS - index;\n", indent, "", options->constants_prefix,
							indent, "", indent, "", register_scs,
									options->constants_prefix);
				if (OPTS(LENTABLE))
					printf(
"%*s      %s%s%s *lengthptr = &%s[%sTOTAL_KEYWORDS + lookup[offset]];\n", indent, "",
							register_scs, const_always,
							smallest_integral_type(t->max_key_len),
									options->lengthtable_name,
									options->constants_prefix);
				printf(
"%*s      %s",				indent, "", register_scs);
				output_const_type(const_readonly_array, t->wordlist_eltype);
				printf("*wordptr = &%s[%sTOTAL_KEYWORDS + lookup[offset]];\n",
					options->wordlist_name, options->constants_prefix);
				printf(
"%*s      %s",				indent, "", register_scs);
				output_const_type(const_readonly_array, t->wordlist_eltype);
				printf("*wordendptr = wordptr + -lookup[offset + 1];\n\n");
				printf(
"%*s      while (wordptr < wordendptr)\n"
"%*s        {\n",			indent, "", indent, "");
				if (OPTS(LENTABLE)) {
					printf(
"%*s          if (len == *lengthptr)\n"
"%*s            {\n",			indent, "", indent, "");
					indent += 4;
				}
				printf(
"%*s          %s%schar *s = ",		indent, "", register_scs, const_always);
				if (OPTS(TYPE))
					printf("wordptr->%s", options->slot_name);
				else
					printf("*wordptr");
				if (OPTS(SHAREDLIB))
					printf(" + %s", options->stringpool_name);
				printf (";\n\n"
"%*s          if (",			indent, "");
				output_comparison("str", "s");
				printf (")\n"
"%*s            return %s;\n",		indent, "", OPTS(TYPE) ? "wordptr" : "s");
				if (OPTS(LENTABLE)) {
					indent -= 4;
					printf(
"%*s            }\n",				indent, "");
				}
				if (OPTS(LENTABLE))
					printf(
"%*s          lengthptr++;\n",			indent, "");
				printf(
"%*s          wordptr++;\n"
"%*s        }\n"
"%*s    }\n",				indent, "", indent, "", indent, "");
			}
			printf(
"%*s}\n",			indent, "");
		} else {
			s32 indent;

			indent = 8;
			if (OPTS(LENTABLE)) {
				printf(
"%*sif (len == %s[key])\n",		indent, "", options->lengthtable_name);
				indent += 2;
			}
			if (OPTS(SHAREDLIB)) {
				if (!OPTS(LENTABLE)) {
					printf(
"%*s{\n"
"%*s  %sint o = %s[key]",				indent, "", indent, "", register_scs,
									options->wordlist_name);
					if (OPTS(TYPE))
						printf(".%s", options->slot_name);
					printf (";\n"
"%*s  if (o >= 0)\n"
"%*s    {\n",					indent, "", indent, "");
					indent += 4;
					printf(
"%*s  %s%schar *s = o",				indent, "", register_scs, const_always);
				} else {
					/*
					 * no need for the (o >= 0) test, because the
					 * (len == lengthtable[key]) test already guarantees that
					 * key points to nonempty table entry
					 */
					printf (
"%*s{\n"
"%*s  %s%schar *s = %s[key]",			indent, "", indent, "", register_scs, const_always,
									options->wordlist_name);
					if (OPTS(TYPE))
						printf(".%s", options->slot_name);
				}
				printf (" + %s", options->stringpool_name);
			} else {
				printf(
"%*s{\n"
"%*s  %s%schar *s = %s[key]",		indent, "", indent, "", register_scs, const_always,
									options->wordlist_name);
				if (OPTS(TYPE))
					printf(".%s", options->slot_name);
			}
			printf (";\n\n"
"%*s  if (",			indent, "");
			if (!OPTS(SHAREDLIB) && OPTS(NULLSTRINGS))
				printf ("s && ");
			output_comparison("str", "s");
			printf (")\n"
"%*s    return ",		indent, "");
			if (OPTS(TYPE))
				printf("&%s[key]", options->wordlist_name);
			else
				printf("s");
			printf(";\n");
			if (OPTS(SHAREDLIB) && !OPTS(LENTABLE)) {
				indent -= 4;
				printf(
"%*s    }\n",				indent, "");
			}
			printf(
"%*s}\n",			indent, "");
		}
	}
	printf(
"    }\n"
"  return 0;\n");
}/*}}}*/
/*{{{ output_num_hash_values */
/* Returns the number of different hash values.  */
static s32 output_num_hash_values(struct Output *t)
{
	s32 count;
	struct Keyword_List *tmp;
	/*
	 * since the list is already sorted by hash value and doesn't contain duplicates, we can
	 * simply count the number of keywords on the list
	 */
	count = 0;
	tmp = t->head;
	loop {
		if (tmp == 0)
			break;
		++count;
		tmp = tmp->next;
	}
	return count;
}/*}}}*/
/*------------------------------------------------------------------------------------------------*/
#undef USE_DOWNCASE_TABLE
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/globals.h"
#include "namespace/options.h"
#include "namespace/output.h"
#include "namespace/output.c"
#include "namespace/keyword.h"
#include "namespace/keyword_list.h"
#include "namespace/positions.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
