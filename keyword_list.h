#ifndef CGPERF_KEYWORD_LIST_H
#define CGPERF_KEYWORD_LIST_H
#include <stdbool.h>

#include "keyword.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/keyword.h"
#include "namespace/keyword_list.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ Keyword_List */
/*{{{ constants and types */
struct Keyword_List {
	struct Keyword *kw;
	struct Keyword_List *next;
};
/*}}} constants and types -- END */
/*{{{ public static methods */
static struct Keyword_List *kwl_new(struct Keyword *kw);
static void kwl_del(struct Keyword_List *t);
/*}}} public static methods -- END */
/*{{{ global utils */
static void delete_list(struct Keyword_List *list);
static struct Keyword_List *copy_list(struct Keyword_List *list);
static struct Keyword_List *mergesort_list(struct Keyword_List *list, bool (*less)(
						struct Keyword *kw1, struct Keyword *kw2));
static struct Keyword_List *merge(struct Keyword_List *list1, struct Keyword_List *list2, bool
					(*less)(struct Keyword *kw1, struct Keyword *kw2));
/*}}} global utils -- END */
/*}}} Keyword_List -- END */
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/keyword.h"
#include "namespace/keyword_list.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
