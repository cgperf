#ifndef CGPERF_POSITIONS_C
#define CGPERF_POSITIONS_C
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "c_fixing.h"
#include "positions.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/positions.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ pos_new */
static struct Positions *pos_new(void)
{
	struct Positions *t;

	t = calloc(1, sizeof(*t));
	t->useall = false;
	t->size = 0;
	return t;
}/*}}}*/
/*{{{ pos_new_cpy */
/* the copy constructor */
static struct Positions *pos_new_cpy(struct Positions *src)
{
	struct Positions *t;

	t = malloc(sizeof(*t));
	memcpy(t, src, sizeof(struct Positions));
	return t;
}/*}}}*/
/*{{{ pos_del */
static void pos_del(struct Positions *t)
{
	free(t);
}/*}}}*/
/*{{{ pos_set_useall */
static void pos_set_useall(struct Positions *t, bool useall)
{
	t->useall = useall;
	if (useall) {
		s32 *ptr;
		s32 i;
		/* The positions are 0, 1, ..., Positions_max_key_pos-1, in descending order */
		t->size = POS_MAX_KEY_POS;
		ptr = t->positions;
		i = POS_MAX_KEY_POS - 1;
		loop {
			if (i < 0)
				break;
			*ptr++ = i;
			i--;
		}
	}
}/*}}}*/
/*{{{ pos_sort */
static bool pos_sort(struct Positions *t)
{
	/*
	 * Sorts the array in reverse order. Returns true if there are no duplicates, false
	 * otherwise
	 */
	bool duplicate_free;
	s32 *base;
	u32 len;
	u32 i;

	if (t->useall)
		return true;
	/* bubble sort */
	duplicate_free = true;
	base = t->positions;
	len = t->size;

	i = 1;
	loop {
      		u32 j;
      		s32 tmp;

		if (i >= len)
			break;
		j = i;
		tmp = base[j];
		loop {
			if ((j == 0) || (tmp < base[j - 1]))
				break;
			base[j] = base[j - 1];
			if (base[j] == tmp) /* oh no, a duplicate!!! */
				duplicate_free = false;
			j--;
		}
		base[j] = tmp;
		++i;
	}
	return duplicate_free;
}/*}}}*/
/*{{{ pos_contains */
/* assumes the array is in reverse order */
static bool pos_contains(struct Positions *t, s32 pos)
{
	u32 count;
	s32 *p;

	count = t->size;
	p = t->positions + t->size - 1;
	loop {
		if (count == 0)
			break;
		if (*p == pos)
			return true;
		if (*p > pos)
			break;
		p--;
		count--;
	}
	return false;
}/*}}}*/
/*{{{ pos_remove */
static void pos_remove(struct Positions *t, s32 pos)
{
	u32 count;

	pos_set_useall(t, false);
	count = t->size;
	if (count > 0) {
		s32 *p;

		p = t->positions + t->size - 1;
		if (*p == pos) {
			(t->size)--;
			return;
		}
		if (*p < pos) {
			s32 prev;

			prev = *p;
			loop {
				s32 curr;

				p--;
				count--;
				if (count == 0)
					break;
				if (*p == pos) {
					*p = prev;
					(t->size)--;
					return;
				}
				if (*p > pos)
					break;
				curr = *p;
				*p = prev;
				prev = curr;
			}
		}
	}
	fprintf(stderr, "Positions::remove internal error: not found\n");
	exit(1);
}/*}}}*/
/*{{{ pos_add */
/* assumes the array is in reverse order */
static void pos_add(struct Positions *t, s32 pos)
{
	u32 count;
	s32 *p;

	pos_set_useall(t, false);

	count = t->size;
	if (count == POS_MAX_SIZE) {
		fprintf(stderr, "Positions_add internal error: overflow\n");
		exit(1);
	}
	p = t->positions + t->size - 1;
	loop {
		if (count == 0)
			break;
		if (*p == pos) {
          		fprintf(stderr, "Positions_add internal error: duplicate\n");
          		exit(1);
		}
		if (*p > pos)
			break;
		p[1] = p[0];
		p--;
		count--;
	}
	p[1] = pos;
	++(t->size);
}/*}}}*/
/*{{{ pos_iterator */
/*
 * creates an iterator, returning the positions in descending order, that apply to strings of length
 * <= maxlen.
 */
static struct PositionIterator *pos_iterator(struct Positions *t, s32 maxlen)
{
	return positer_new(t, maxlen);
}/*}}}*/
/*{{{ pos_iterator_all */
/* creates an iterator, returning the positions in descending order */
static struct PositionIterator *pos_iterator_all(struct Positions *t)
{
	return positer_new_all(t);
}/*}}}*/
/*{{{ pos_reviterator */
/* creates an iterator, returning the positions in ascending order */
static struct PositionReverseIterator *pos_reviterator(struct Positions *t)
{
	return posrevit_new(t);
}/*}}}*/
/*{{{ positer_new */
/* initializes an iterator through POSITIONS, ignoring positions >= maxlen */
static struct PositionIterator *positer_new(struct Positions *positions, s32 maxlen)
{
	struct PositionIterator *t;

	t = calloc(1, sizeof(*t));
	t->set = positions;

	if (positions->useall) {
		t->index = (maxlen <= (s32)POS_MAX_KEY_POS ? (s32)POS_MAX_KEY_POS - maxlen : 0);
	} else {
		u32 index;

		index = 0;
		loop {
			if (index >= positions->size || positions->positions[index] < maxlen)
				break;
			++index;
		}
		t->index = index;
	}
	return t;
}/*}}}*/
/*{{{ positer_new_all */
/* initializes an iterator through POSITIONS */
static struct PositionIterator *positer_new_all(struct Positions *positions)
{
	struct PositionIterator *t;

	t = calloc(1, sizeof(*t));
	t->set = positions;
	return t;
}/*}}}*/
/*{{{ positer_remaining */
/* returns the number of remaining positions, i.e. how often next() will return a value != EOS */
static u32 positer_remaining(struct PositionIterator *t)
{
	return t->set->size - t->index;
}/*}}}*/
/*{{{ positer_next */
/* retrieves the next position, or EOS past the end */
static s32 positer_next(struct PositionIterator *t)
{
	s32 r;

	r = t->index < t->set->size ? t->set->positions[t->index] : POSITER_EOS;
	++(t->index);
	return r;
}/*}}}*/
/*{{{ positer_del */
static void positer_del(struct PositionIterator *t)
{
	free(t);
}/*}}}*/
/*{{{ posrevit_new */
static struct PositionReverseIterator *posrevit_new(struct Positions *positions)
{
	struct PositionReverseIterator *t;

	t = calloc(1, sizeof(*t));
	t->set = positions;
	t->index = t->set->size;
	return t;
}/*}}}*/
/*{{{ posrevit_del */
static void posrevit_del(struct PositionReverseIterator *t)
{
	free(t);
}/*}}}*/
/*{{{ posrevit_next */
/* retrieves the next position, or EOS past the end  */
static s32 posrevit_next(struct PositionReverseIterator *t)
{
	s32 r;

	(t->index)--;
	r = (t->index > t->minindex ?  t->set->positions[t->index] : POSREVIT_EOS);
	return r;
}
/*}}}*/
/*{{{ pos_cpy */
/* _NOT_ the copy constructor */
static void pos_cpy(struct Positions *d, struct Positions *s)
{
	memcpy(d, s, sizeof(struct Positions));
}/*}}}*/
/*{{{ pos_print */
static void pos_print(struct Positions *t)
{
	bool first;
	bool seen_LASTCHAR;
	u32 count;
	s32 *p;

	if (t->useall) {
		printf ("*");
		return;
	}
	first = true;
	seen_LASTCHAR = false;
	count = t->size;
	p = t->positions + t->size - 1;
	loop {
		if (count == 0)
			break;
		count--;
		if (*p == POS_LASTCHAR)
			seen_LASTCHAR = true;
		else {
			if (!first)
				printf(",");
			printf("%d", *p + 1);
			if (count > 0 && p[-1] == *p + 1) {
				printf("-");
				loop {
					p--;
					count--;
					if (!(count > 0 && p[-1] == *p + 1))
						break;
				}
				printf("%d", *p + 1);
			}
			first = false;
		}
		p--;
	}
	if (seen_LASTCHAR) {
		if (!first)
			printf(",");
		printf("$");
	}
}/*}}}*/
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/positions.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
