#ifndef CGPERF_OUTPUT_H
#define CGPERF_OUTPUT_H
#include <stdbool.h>
#include "c_fixing.h"
#include "keyword_list.h"
#include "positions.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/globals.h"
#include "namespace/options.h"
#include "namespace/output.h"
#include "namespace/keyword_list.h"
#include "namespace/positions.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ types */
struct Output {
/*{{{ private */
	/* linked list of keywords */
	struct Keyword_List *head;
	/* declaration of struct type for a keyword and its attributes */
	u8 *struct_decl;
	u32 struct_decl_lineno;
	/* pointer to return type for lookup function */
	u8 *return_type;
	/* shorthand for user-defined struct tag type */
	u8 *struct_tag;
	/* the C code from the declarations section */
	u8 *verbatim_declarations;
	u8 *verbatim_declarations_end;
	u32 verbatim_declarations_lineno;
	/* the C code from the end of the file */
	u8 *verbatim_code;
	u8 *verbatim_code_end;
	u32 verbatim_code_lineno;
	/* whether the keyword chars would have different values in a different character set */
	bool charset_dependent;
	/* total number of keys, counting duplicates */
	s32 total_keys;
	/* maximum length of the longest keyword */
	s32 max_key_len;
	/* minimum length of the shortest keyword */
	s32 min_key_len;
	/* whether the hash function includes the length */
	bool hash_includes_len;
	/* key positions */
	struct Positions *key_positions;
	/* adjustments to add to bytes add specific key positions */
	u32 *alpha_inc;
	/* total number of duplicate hash values */
	s32 total_duplicates;
	/* size of alphabet */
	u32 alpha_size;
	/* value associated with each character */
	s32 *asso_values;
	/* minimum hash value for all keywords */
	s32 min_hash_value;
	/* maximum hash value for all keywords */
	s32 max_hash_value;
	/* element type of keyword array */
	u8 *wordlist_eltype;
/*}}} private -- END */
};
/*}}} types -- END */
/*{{{ public static methods */
static struct Output *output_new(struct Keyword_List *head, u8 *struct_decl,
				u32 struct_decl_lineno, u8 *return_type,
				u8 *struct_tag, u8 *verbatim_declarations,
				u8 *verbatim_declarations_end,
				u32 verbatim_declarations_lineno,
				u8 *verbatim_code, u8 *verbatim_code_end,
				u32 verbatim_code_lineno, bool charset_dependent,
				s32 total_keys, s32 max_key_len, s32 min_key_len,
				bool hash_includes_len, struct Positions *positions,
				u32 *alpha_inc, s32 total_duplicates,
				u32 alpha_size, s32 *asso_values);
static void output_do(struct Output *t);
static void output_compute_min_max(struct Output *t);
/*}}} public static methods -- END */
/*{{{ private static methods */
static void output_constants_defines(struct Output *t);
static void output_constants_enum(struct Output *t, u8 *indentation);
static void output_hash_function(struct Output *t);
static void output_asso_values_ref(struct Output *t, s32 pos);
static void output_asso_values_index(struct Output *t, s32 pos);
static void output_lookup_pools(struct Output *t);
static void output_string_pool(struct Output *t);
static void output_lookup_tables(struct Output *t);
static void output_keylength_table(struct Output *t);
static void output_keyword_table(struct Output *t);
static void output_lookup_array(struct Output *t);
static void output_lookup_function(struct Output *t);
static void output_lookup_function_body(struct Output *t,
						void (*output_comparison)(u8 *expr1, u8 *expr2));
static s32 output_num_hash_values(struct Output *t);
/*}}} private static methods -- END */
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/globals.h"
#include "namespace/options.h"
#include "namespace/output.h"
#include "namespace/keyword_list.h"
#include "namespace/positions.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif

