#ifndef CGPERF_INPUT_H
#define CGPERF_INPUT_H
#include <stdbool.h>
#include <stdio.h>
#include "c_fixing.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/input.h"
#include "namespace/keyword.h"
#include "namespace/keyword_list.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ types */
struct Input {
/*{{{ public */
	/* memory block containing the entire input */
	u8 *input;
	u8 *input_end;
	/* the C code from the declarations section */
	u8 *verbatim_declarations;
	u8 *verbatim_declarations_end;
	u32 verbatim_declarations_lineno;
	/* the C code from the end of the file */
	u8 *verbatim_code;
	u8 *verbatim_code_end;
	u32 verbatim_code_lineno;
	/* declaration of struct type for a keyword and its attributes */
	u8 *struct_decl;
	u32 struct_decl_lineno;
	/* return type of the lookup function */
	u8 *return_type;
	/* shorthand for user-defined struct tag type */
	u8 *struct_tag;
	/* list of all keywords */
	struct Keyword_List *head;
	/* whether the keyword chars would have different values in a different character set */
	bool charset_dependent;
/*}}} public -- END */
/*{{{ private */
	FILE *stream;
/*}}} prived -- END */
};
/*}}} types -- END */
/*{{{ public static methods */
static struct Input *input_new(FILE *stream);
static void input_read(struct Input *t);
/*}}} public static methos -- END */
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/input.h"
#include "namespace/keyword.h"
#include "namespace/keyword_list.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif
