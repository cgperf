#ifndef CGPERF_GETLINE_C
#define CGPERF_GETLINE_C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "c_fixing.h"
#include "getline.h"
/*------------------------------------------------------------------------------------------------*/
#include "namespace/getline.h"
/*------------------------------------------------------------------------------------------------*/
/*{{{ get_delim */
static s32 get_delim(u8 **lineptr, u32 *n, s32 delimiter, FILE *stream)
{
	return getstr(lineptr, n, stream, delimiter, 0);
}/*}}}*/
/*{{{ getstr */
/* always add at least this many bytes when extending the buffer */
#define MIN_CHUNK 64
/* Reads up to (and including) a TERMINATOR from STREAM into *LINEPTR + OFFSET
   (and null-terminate it). *LINEPTR is a pointer returned from new [] (or
   NULL), pointing to *N characters of space.  It is realloc'd as
   necessary.  Returns the number of characters read (not including the
   null terminator), or -1 on error or immediate EOF.
   NOTE: There is another getstr() function declared in <curses.h>.  */
static s32 getstr(u8 **lineptr, u32 *n, FILE *stream, u8 terminator,
								u32 offset)
{
	u32 nchars_avail;	/* allocated but unused chars in *LINEPTR */
	u8 *read_pos;		/* Where we're reading into *LINEPTR. */

	if (!lineptr || !n || !stream)
		return -1;
	if (!*lineptr) {
		*n = MIN_CHUNK;
		*lineptr = calloc(*n, sizeof(u8));
	}
	nchars_avail = *n - offset;
	read_pos = *lineptr + offset;
	loop {
		s32 c;

		c = getc(stream);
		/*
		 * we always want at least one char left in the buffer, since we always (unless we
		 * get an error while reading the first char) NUL-terminate the line buffer
		 */
		assert(*n - nchars_avail == (u32)(read_pos - *lineptr));
		if (nchars_avail < 2) {
			u8 *new_line;

			if (*n > MIN_CHUNK)
				*n *= 2;
			else
				*n += MIN_CHUNK;

			nchars_avail = *n + *lineptr - read_pos;
			new_line = calloc(*n, sizeof(u8));
			if (*lineptr != 0) {
				memcpy(new_line, *lineptr, read_pos - *lineptr);
				free(*lineptr);
			}
			*lineptr = new_line;
			read_pos = *n - nchars_avail + *lineptr;
			assert(*n - nchars_avail == (u32)(read_pos - *lineptr));
		}
		if (c == EOF || ferror(stream)) {
			/* return partial line, if any */
			if (read_pos == *lineptr)
				return -1;
			else
				break;
		}
		*read_pos++ = c;
		nchars_avail--;

		if (c == terminator)
			/* return the line */
			break;
	}
	/* done - NUL terminate and return the number of chars read */
	*read_pos = '\0';
	return read_pos - (*lineptr + offset);
}
#undef MIN_CHUNK
/*}}}*/
/*------------------------------------------------------------------------------------------------*/
#define EPILOG
#include "namespace/getline.h"
#undef EPILOG
/*------------------------------------------------------------------------------------------------*/
#endif

